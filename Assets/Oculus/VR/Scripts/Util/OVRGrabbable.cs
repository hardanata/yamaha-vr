/************************************************************************************
Copyright : Copyright (c) Facebook Technologies, LLC and its affiliates. All rights reserved.

Licensed under the Oculus Utilities SDK License Version 1.31 (the "License"); you may not use
the Utilities SDK except in compliance with the License, which is provided at the time of installation
or download, or which otherwise accompanies this software in either electronic or hard copy form.

You may obtain a copy of the License at
https://developer.oculus.com/licenses/utilities-1.31

Unless required by applicable law or agreed to in writing, the Utilities SDK distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF
ANY KIND, either express or implied. See the License for the specific language governing
permissions and limitations under the License.
************************************************************************************/

using System;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// An object that can be grabbed and thrown by OVRGrabber.
/// </summary>
public class OVRGrabbable : MonoBehaviour
{
	public enum AxisAlign
	{
		X = 0,
		Y = 1,
		Z = 2,
		None = 3
	}

	public Transform overrideTransform { get; set; }
	public AxisAlign overrideAxis { get; set; }
	
    [SerializeField]
    protected bool m_allowOffhandGrab = true;
    [SerializeField]
    protected bool m_snapPosition = false;
    [SerializeField]
    protected bool m_snapOrientation = false;
    [SerializeField]
    protected Transform m_snapOffset;
    [SerializeField]
    protected Collider[] m_grabPoints = null;

    protected bool m_grabbedKinematic = false;
    protected Collider m_grabbedCollider = null;
    protected OVRGrabber m_grabbedBy = null;

    public UnityEvent onGrabBegin;
    public UnityEvent onGrabRelease;
    public UnityAction onTriggerBegin;
    public UnityAction onTriggerRelease;
    public UnityAction onDownB;

    private const float TriggerBegin = 0.55f;
    private const float TriggerEnd = 0.35f;

    private float m_prevFlex;
    
	/// <summary>
	/// If true, the object can currently be grabbed.
	/// </summary>
    public bool allowOffhandGrab
    {
        get { return m_allowOffhandGrab; }
    }

	/// <summary>
	/// If true, the object is currently grabbed.
	/// </summary>
    public bool isGrabbed
    {
        get { return m_grabbedBy != null; }
    }

	/// <summary>
	/// If true, the object's position will snap to match snapOffset when grabbed.
	/// </summary>
    public bool snapPosition
    {
        get { return m_snapPosition; }
    }

	/// <summary>
	/// If true, the object's orientation will snap to match snapOffset when grabbed.
	/// </summary>
    public bool snapOrientation
    {
        get { return m_snapOrientation; }
    }

	/// <summary>
	/// An offset relative to the OVRGrabber where this object can snap when grabbed.
	/// </summary>
    public Transform snapOffset
    {
        get { return m_snapOffset; }
    }

	/// <summary>
	/// Returns the OVRGrabber currently grabbing this object.
	/// </summary>
    public OVRGrabber grabbedBy
    {
        get { return m_grabbedBy; }
    }

	/// <summary>
	/// The transform at which this object was grabbed.
	/// </summary>
    public Transform grabbedTransform
    {
        get { return m_grabbedCollider.transform; }
    }

	/// <summary>
	/// The Rigidbody of the collider that was used to grab this object.
	/// </summary>
    public Rigidbody grabbedRigidbody
    {
        get { return m_grabbedCollider.attachedRigidbody; }
    }

	/// <summary>
	/// The contact point(s) where the object was grabbed.
	/// </summary>
    public Collider[] grabPoints
    {
        get { return m_grabPoints; }
    }

	/// <summary>
	/// Notifies the object that it has been grabbed.
	/// </summary>
	virtual public void GrabBegin(OVRGrabber hand, Collider grabPoint)
    {
        m_grabbedBy = hand;
        m_grabbedCollider = grabPoint;
        if (!m_grabbedCollider.attachedRigidbody)
        {
	        var rb = m_grabbedCollider.GetComponentInParent<Rigidbody>();
	        if(rb)
				rb.isKinematic = true;
        }
        gameObject.GetComponent<Rigidbody>().isKinematic = true;
        onGrabBegin?.Invoke();
    }

	/// <summary>
	/// Notifies the object that it has been released.
	/// </summary>
	virtual public void GrabEnd(Vector3 linearVelocity, Vector3 angularVelocity)
    {
        Rigidbody rb = gameObject.GetComponent<Rigidbody>();
        rb.isKinematic = m_grabbedKinematic;
        rb.velocity = linearVelocity;
        rb.angularVelocity = angularVelocity;
        m_grabbedBy = null;
        m_grabbedCollider = null;
        onGrabRelease?.Invoke();
    }

    void Awake()
    {
	    if (m_grabPoints != null)
	    {
		    if (m_grabPoints.Length == 0)
		    {
			    // Get the collider from the grabbable
			    Collider collider = this.GetComponent<Collider>();
			    if (collider == null)
			    {
				    Destroy(this);
				    //throw new ArgumentException("Grabbables cannot have zero grab points and no collider -- please add a grab point or collider.");
			    }

			    // Create a default grab point
			    m_grabPoints = new Collider[1] {collider};
		    }
	    }
        
    }

    //public void memasukidengansepenuhhati()
    //{
	//		// Get the collider from the grabbable
	//	    Collider collider = this.GetComponent<Collider>();
	//	    if (collider == null)
	//	    {
	//		    Destroy(this);
	//		    //throw new ArgumentException("Grabbables cannot have zero grab points and no collider -- please add a grab point or collider.");
	//	    }
	//
	//		    // Create a default grab point
	//		    m_grabPoints = new Collider[1] { collider };
	//	    
  //  }

    protected virtual void Start()
    {
        m_grabbedKinematic = GetComponent<Rigidbody>().isKinematic;
    }

    public virtual void ReInitialize()
    {
	    Debug.Log("Grabbable: " + name + " reinitialized.");
    }

    private void FixedUpdate()
    {
	    if (!grabbedBy)
		    return;
	    
	    float prevFlex = m_prevFlex;
	    // Update values from inputs
	    m_prevFlex = OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger, grabbedBy.controller);
	    
	    if ((m_prevFlex >= TriggerBegin) && (prevFlex < TriggerBegin))
	    {
		    onTriggerBegin?.Invoke();
	    }
	    else if ((m_prevFlex <= TriggerEnd) && (prevFlex > TriggerEnd))
	    {
		    onTriggerRelease?.Invoke();
	    }

	    if (OVRInput.GetDown(OVRInput.RawButton.B, grabbedBy.controller))
	    {
		    onDownB?.Invoke();
	    }
    }

    void OnDestroy()
    {
        if (m_grabbedBy != null)
        {
            // Notify the hand to release destroyed grabbables
            m_grabbedBy.ForceRelease(this);
        }
    }
}
