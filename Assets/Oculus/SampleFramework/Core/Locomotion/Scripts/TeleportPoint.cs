/************************************************************************************

Copyright (c) Facebook Technologies, LLC and its affiliates. All rights reserved.  

See SampleFramework license.txt for license terms.  Unless required by applicable law 
or agreed to in writing, the sample code is provided “AS IS” WITHOUT WARRANTIES OR 
CONDITIONS OF ANY KIND, either express or implied.  See the license for specific 
language governing permissions and limitations under the license.

************************************************************************************/

using System;
using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class TeleportPoint : MonoBehaviour
{
	public UnityEvent onTeleportedTo;
	public UnityEvent onLostFocus;
	
    public float dimmingSpeed = 1;
    public float fullIntensity = 1;
    public float lowIntensity = 0.5f;

    public Transform destTransform;

    [Header("Audio")]
    public AudioClip teleportSound;
    private AudioSource audio;
    public string AudioInResources = "Teleport 2 (CC 0)";
    public bool audioIsPlaying = false;

    private float lastLookAtTime = 0;

    public Transform GetDestTransform()
    {
        return destTransform;
    }
    private void Start()
    {
        if (!teleportSound) teleportSound = Resources.Load<AudioClip>("Audio/Ambience/" + AudioInResources);
        onTeleportedTo.AddListener(delegate { GetComponent<MeshRenderer>().enabled = false; playAudio(); });
        onTeleportedTo.AddListener(delegate { LocationTracker.instance.currentTeleportPoint = this; });
	    onLostFocus.AddListener(delegate { GetComponent<MeshRenderer>().enabled = true; });
    }


    // Update is called once per frame
	void Update () {
        float intensity = Mathf.SmoothStep(fullIntensity, lowIntensity, (Time.time - lastLookAtTime) * dimmingSpeed);
        GetComponent<MeshRenderer>().material.SetFloat("_Intensity", intensity);
        if (audio)
        {
            if (audio.isPlaying)
                audioIsPlaying = true;
            else {
                audioIsPlaying = false;
                if (gameObject.GetComponent<AudioSource>())
                    audio = null;
                    Destroy(gameObject.GetComponent<AudioSource>());
            }
        }
	}

    public void OnLookAt()
    {
        lastLookAtTime = Time.time;
    }

    public void playAudio()
    {

        if (teleportSound) {
            audio = gameObject.AddComponent<AudioSource>();
            audio.clip = teleportSound;
                if (!audio.isPlaying) 
                audio.Play(); 
        }
    }
}
