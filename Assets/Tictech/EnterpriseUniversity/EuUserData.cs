﻿using System;

namespace Tictech.EnterpriseUniversity
{
    [Serializable]
    public struct EuUserLogin
    {
        public string email;
        public string password;

        public EuUserLogin(string un, string pw)
        {
            email = un;
            password = pw;
        }
    }
    
    [Serializable]
    public class EuUserData
    {
        public int id;
        public string username;
        public string password;
        public string token;
        public string first_name;
        public object last_name;
        public object panggilan;
        public DateTime tanggal_lahir;
        public object profile_picture;
        public string gender;
        public object phone_number;
        public object nik;
        public object labour_cost;
        public object rfid_access;
        public object fingerprint_access;
        public object jobdescId;
        public object teamId;
        
        public EuUserData(string un, string pw)
        {
            username = un;
            password = pw;
        }

        public EuUserData(string tk)
        {
            token = tk;
        }
    }
    [Serializable]
    public class EuResponseData
    {
        public string message;
        public string token;
        public EuUserData GetUser()
        {
            return new EuUserData(token);
        }
    }
    
    [Serializable]
    public class EuResponse
    {
        public string status;
        public EuResponseData data;
    }

    [Serializable]
    public struct EuTrainingScore
    {
        public int score;
        public string user_id;
        public string training_id;

        public EuTrainingScore(int sc, string tr, string mbr)
        {
            score = sc;
            user_id = mbr;
            training_id = tr;
        }
    }

    public struct EuTrainingScoreToken
    {
        public int score;
        public string training_id;

        public EuTrainingScoreToken(int sc, string tr)
        {
            score = sc;
            training_id = tr;
        }
    }
}
