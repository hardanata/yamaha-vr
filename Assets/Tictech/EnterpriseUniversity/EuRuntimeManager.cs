﻿using System.Collections.Generic;
using MEC;
using Sirenix.OdinInspector;
using Tictech.Utilities.PersistentCanvas;
using UnityEngine;
using UnityEngine.Networking;
using Tictech.Oculus.Scenario;

namespace Tictech.EnterpriseUniversity
{
    public class EuRuntimeManager : MonoBehaviour
    {
        public static EuRuntimeManager Instance;

        [Header("API Access")]
        public string baseUrl;
        public string loginHandle;
        public string usersHandle;
        public string scoreHandle;
        public string scoreUserParameter;

        #if UNITY_EDITOR
        [Header("Debug")]
        public string debugUsername;
        public string debugPassword;
        public string debugTrainingId;
        public int debugScore;

        [Button]
        void TestPostScore()
        {
            Timing.RunCoroutine(PostScore(debugTrainingId, debugScore));
        }
        #endif

        [SerializeField, ReadOnly]
        public EuUserData User { get; private set; }
    
        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else if (Instance != this)
            {
                Destroy(gameObject);
            }
        }

        public IEnumerator<float> SignIn(string username, string password)
        {
            EuResponse authStatus = null;

            var pc = PersistentCanvas.Instance;
            pc.SetLoading(true, "Signing in...");
            var userLogin = new EuUserLogin(username, password);
            var userBytes =
                System.Text.Encoding.UTF8.GetBytes(JsonUtility.ToJson(userLogin));

            var auth = UnityWebRequest.Put(baseUrl + loginHandle, userBytes);
            //var auth = new UnityWebRequest(baseUrl + loginHandle, UnityWebRequest.kHttpVerbPOST);
            auth.method = UnityWebRequest.kHttpVerbPOST;
            //auth.uploadHandler = new UploadHandlerRaw(userBytes);
            auth.SetRequestHeader("Content-Type", "application/json");
            auth.SetRequestHeader("Accept", "application/json");
            //auth.uploadHandler.contentType = "application/json";
            //auth.certificateHandler = new AcceptAllCertificatesSignedWithASpecificKeyPublicKey();
            Debug.Log("Signing in to: " + auth.url);
            var async = auth.SendWebRequest();
            while (!async.isDone)
            {
                yield return Timing.WaitForOneFrame;
            }

            if (auth.isNetworkError || auth.isHttpError)
            {
                Debug.LogError(auth.error);
            }
            else
            {
                Debug.Log(auth.downloadHandler.text);
                authStatus = JsonUtility.FromJson<EuResponse>(auth.downloadHandler.text);
            }
            
            pc.SetLoading(false, "");

            if (authStatus == null)
            {
                User = null;
                pc.SetLoading(true, "Failed to log in.");
                yield return Timing.WaitForSeconds(1f);
                pc.SetLoading(false, "");
            }
            else
            {
                User = authStatus.data.GetUser();
                pc.SetLoading(true, "Successfully logged in, user id: " + User.id);
                yield return Timing.WaitForSeconds(1f);
                pc.SetLoading(false, "");
            }            
        }

        public IEnumerator<float> PostScore(string trainingId, int score)
        {
#if UNITY_EDITOR
            if (User == null || string.IsNullOrEmpty(User.token))
                yield return Timing.WaitUntilDone(SignIn(debugUsername, debugPassword));
#endif
            if (User == null)
                yield break;
            if (string.IsNullOrEmpty(User.token))
            {
                Debug.LogWarning("User isn't signed in, failed to post score.");
                yield break;
            }
            else
            {
                Debug.Log("Uploading score to:\r\ntraining_id: " + trainingId + "\r\nscore: " + score + "\r\nbearer: " + User.token);
            }

            //var trainingData = JsonUtility.ToJson(new EuTrainingScore(score, trainingId, User.id.ToString()));
            var trainingData = JsonUtility.ToJson(new EuTrainingScoreToken(score, trainingId));
            trainingData = trainingData.Replace("username\":", scoreUserParameter + "\":");
            var post = UnityWebRequest.Put(baseUrl + scoreHandle, System.Text.Encoding.UTF8.GetBytes(trainingData));
            post.method = UnityWebRequest.kHttpVerbPOST;
            post.SetRequestHeader("Content-Type", "application/json");
            post.SetRequestHeader("Accept", "application/json");
            post.SetRequestHeader("Authorization", "Bearer " + User.token);
            post.uploadHandler.contentType = "application/json";
            var async = post.SendWebRequest();
            while (!async.isDone)
            {
                yield return Timing.WaitForOneFrame;
            }
            
            if (post.isNetworkError || post.isHttpError)
            {
                Debug.LogError(post.error);
            }
            else
            {
                Debug.Log("Score submitted. response: " + post.downloadHandler.text);
            }
        }
    }
}