﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Tictech.Utilities.Audio
{
    [CreateAssetMenu(fileName = "New Audio Reference", menuName = "Tictech/Utilities/Sound/Reference", order = 0)]
    public class AudioReference : SerializedScriptableObject
    {
        public Dictionary<string, AudioClip> clips;
        public Dictionary<string, AudioClip[]> clipsArray;

        public AudioClip GetClip(string key)
        {
            return clips.ContainsKey(key) ? clips[key] : null;
        }

        public AudioClip GetClipArray(string key)
        {
            return clipsArray.ContainsKey(key) ? clipsArray[key][Random.Range(0, clipsArray[key].Length)] : null;
        }
    }
}