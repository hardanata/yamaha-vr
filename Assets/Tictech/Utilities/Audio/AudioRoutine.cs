﻿using System.Collections.Generic;
using MEC;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Tictech.Utilities.Audio
{
    public class AudioRoutine : MonoBehaviour
    {
        public float transitionTime;

        public AudioClip start;
        public AudioClip[] loop;
        public AudioClip end;

        private AudioSource _sourceA, _sourceB;

        public CoroutineHandle _playHandle { get; set; }
        private CoroutineHandle _changingTrack;
        private bool _isUsingSourceA;

        public AudioRoutine(CoroutineHandle changingTrack)
        {
            _changingTrack = changingTrack;
        }

        private void Start()
        {
            _sourceA = gameObject.AddComponent<AudioSource>();
            _sourceA.loop = false;
            if (loop.Length <= 0)
                return;
            _sourceB = gameObject.AddComponent<AudioSource>();
            _sourceB.loop = false;
        }

        public void Play()
        {
            if (_playHandle.IsRunning)
                return;

            if (loop.Length > 0)
                _playHandle = Timing.RunCoroutine(Playing());
            else
                _sourceA.PlayOneShot(start);
        }

        public void Stop()
        {
            if (!_playHandle.IsRunning)
                return;

            Timing.KillCoroutines(_playHandle);
            Timing.RunCoroutine(ChangeTrack(end));
        }

        IEnumerator<float> Playing()
        {
            _sourceA.Stop();
            _sourceB.Stop();
            _isUsingSourceA = true;
            var currentClip = start;
            _sourceA.volume = 1f;
            _sourceA.clip = currentClip;
            _sourceA.Play();
            while (true)
            {
                yield return Timing.WaitForSeconds(currentClip.length - (transitionTime) - .05f);
                currentClip = loop[Random.Range(0, loop.Length)];
                if (_changingTrack.IsRunning)
                    Timing.KillCoroutines(_changingTrack);
                Timing.RunCoroutine(ChangeTrack(currentClip));
            }
        }

        IEnumerator<float> ChangeTrack(AudioClip clip)
        {
            var current = _isUsingSourceA ? _sourceA : _sourceB;
            var to = _isUsingSourceA ? _sourceB : _sourceA;
            _isUsingSourceA = !_isUsingSourceA;
            if (clip != null && to != null)
            {
                to.clip = clip;
                to.Play();
                double nextEventTime = AudioSettings.dspTime + to.clip.length;
            }

            float a = 0;
            while (a < 1f)
            {
                if (!current)
                    break;
                a += Timing.DeltaTime / transitionTime;
                current.volume = 1f - a;
                to.volume = a;
                yield return Timing.WaitForOneFrame;
            }

            if (current)
                current.Stop();
        }
    }
}
