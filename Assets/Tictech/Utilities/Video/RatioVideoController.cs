﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class RatioVideoController : MonoBehaviour
{
    private AspectRatioFitter _fitter;
    private VideoPlayer _player;

    private void Start()
    {
        _fitter = GetComponent<AspectRatioFitter>();
        _player = GetComponent<VideoPlayer>();
    }

    public void Play(VideoClip clip)
    {
        if (!clip)
            return;
        _fitter.aspectRatio = (float)clip.width / clip.height;
        _player.Stop();
        _player.clip = clip;
        _player.Play();
    }
}
