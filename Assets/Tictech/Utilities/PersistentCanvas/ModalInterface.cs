﻿using UnityEngine.Events;

namespace Tictech.Utilities.PersistentCanvas
{
    public interface IModal
    {
        void InitiateParameters(UnityAction action, params string[] list);
    }
}
