﻿using System;
using System.Collections;
using System.Collections.Generic;
using FullSerializer;
using Proyecto26;
using TMPro;
using UnityEngine;

public class PlayerScore : MonoBehaviour
{
    public TextMeshProUGUI scoreText;
    public TMP_InputField getScoreText;
    public TMP_InputField emailText;
    public TMP_InputField userNameText;
    public TMP_InputField passwordText;

    private fsSerializer serializer;
    private System.Random random = new System.Random();
    UserTest user = new UserTest();

    public static int playerScore;
    public static string playerName;

    private string getLocalId;
    private string idToken;
    public static string localId;
    
    private const string AuthKey = "AIzaSyDtHYkEoZkrzEGqYmV-JIL8hmC7wP_yPAU";
    private const string ProjectId = "petrokimia-ar"; // You can find this in your Firebase project settings
    private static readonly string DatabaseUrl = $"https://{ProjectId}.firebaseio.com/users/";

    private void Start()
    {
        playerScore = random.Next(0, 101);
        scoreText.text = playerScore.ToString();
    }

    public void OnSubmit()
    {
        PostToDatabase();
    }

    public void OnGetScore()
    {
        GetLocalId();
    }

    private void UpdateScore()
    {
        scoreText.text = user.userScore.ToString();
    }

    private void PostToDatabase(bool emptyScore = false)
    {    
        UserTest user = new UserTest();
        if (string.IsNullOrEmpty(user.userName))
            user.userName = userNameText.text;

        if (emptyScore)
        {
            user.userScore = 0;
        }
        
        RestClient.Put(DatabaseUrl + localId + ".json?auth=" + idToken, user).Then(helper =>
        {
            Debug.Log("Put success.");
        }).Catch(error =>
        {
            Debug.LogError(error);
        });
    }

    private void RetrieveFromDatabase()
    {
        RestClient.Get<UserTest>(DatabaseUrl + getLocalId + ".json?auth=" + idToken).Then(response =>
        {
            user = response;
            UpdateScore();
            Debug.Log("Retrieve success.");
        }).Catch(error =>
        {
            Debug.LogError(error);
        });
    }

    public void SignUpButton()
    {
        SignUpUser(emailText.text, userNameText.text, passwordText.text);
    }

    public void SignInButton()
    {
        SignInUser(emailText.text, passwordText.text);
    }

    private void SignUpUser(string email, string username, string password)
    {
        string userData = "{\"email\":\"" + email + "\",\"password\":\"" + password + "\",\"returnSecureToken\":true}";
        RestClient.Post<SignResponse>("https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key=" + AuthKey, userData).Then(
            response =>
            {
                idToken = response.idToken;
                localId = response.localId;
                playerName = username;
                PostToDatabase(true);
                Debug.Log("Sign up success, localId: " + localId);
            }).Catch(error =>
        {
            Debug.LogError(error);
        });
    }

    private void SignInUser(string email, string password)
    {
        string userData = "{\"email\":\"" + email + "\",\"password\":\"" + password + "\",\"returnSecureToken\":true}";
        RestClient.Post<SignResponse>("https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=" + AuthKey, userData).Then(
            response =>
            {
                idToken = response.idToken;
                localId = response.localId;
                GetUsername();
                Debug.Log("Sign in success, localId: " + localId);
            }).Catch(error =>
        {
            Debug.LogError(error);
        });
    }

    private void GetUsername()
    {
        RestClient.Get<UserTest>(DatabaseUrl + localId + ".json?auth=" + idToken).Then(response => { playerName = response.userName; });
    }

    private void GetLocalId()
    {
        RestClient.Get(DatabaseUrl + ".json?auth=" + idToken).Then(response =>
        {
            var username = getScoreText.text;

            fsData userData = fsJsonParser.Parse(response.Text);
            Dictionary<string, UserTest> users = null;
            serializer.TryDeserialize(userData, ref users);

            foreach (var user in users.Values)
            {
                if (user.userName == username)
                {
                    getLocalId = user.localId;
                    RetrieveFromDatabase();
                    break;
                }
            }
        });
    }
}

public class UserTest
{
    public string userName;
    public int userScore;
    public string localId;

    public UserTest()
    {
        userName = PlayerScore.playerName;
        userScore = PlayerScore.playerScore;
        localId = PlayerScore.localId;
    }
}
