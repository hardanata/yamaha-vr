﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using MEC;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
//using Valve.VR.InteractionSystem;

public class TeleportScaleCanvas : MonoBehaviour
{
    public TeleportPoint point;
    public AudioClip popupSound, shrinkSound;
    public float defaultYSummon;
    public Vector2 summonScale;
    public bool animated = true;

    [Header("Read Only")]
    [SerializeField, ReadOnly]
    private float defaultX;
    [SerializeField, ReadOnly]
    private float defaultY;
    private TextMeshProUGUI contentText;
    private AudioSource audio;

    private bool summoning = false;
    [SerializeField, ReadOnly]
    private bool active = true;
    [SerializeField, ReadOnly]
    private bool isBusy = false;
    private bool initialized = false;

    private IEnumerator Start()
    {
        while (transform.localScale.x > 0f)
        {
            Timing.RunCoroutine(Initialize());
            yield return new WaitForSeconds(.5f);
        }
    }
    
    private IEnumerator<float> Initialize()
    {
        //Debug.Log("Canvas: " + gameObject.name + ", initialized: " + initialized);
        yield return Timing.WaitForOneFrame;

        if (!initialized)
        {
            contentText = GetComponentInChildren<TextMeshProUGUI>();
            defaultX = transform.localScale.x;
            defaultY = transform.localScale.y;

            yield return Timing.WaitForOneFrame;
        }

        transform.DOScaleX(summonScale.x, 0f);
        transform.DOScaleY(summonScale.y, 0f);
        
        if (initialized) yield break;
        initialized = true;

        if (popupSound)
            audio = gameObject.AddComponent<AudioSource>();

        if (!point) yield break;
        point.onTeleportedTo.AddListener(delegate{ Toggle(true); });
        point.onLostFocus.AddListener(delegate{ Toggle(false); });
    }

    public void Toggle(bool value)
    {
        if(!active || isBusy)
            return;

        Timing.RunCoroutine(value ? Show() : Hide());
    }

    public void SetActive(bool value)
    {
        Timing.RunCoroutine(Activation(value));
    }

    private IEnumerator<float> Activation(bool value)
    {
        yield return Timing.WaitForSeconds(.1f);
        active = value;
    }

    public void ChangeText(string value)
    {
        
    }

    public void Summon()
    {
        if (summoning)
            return;
        Timing.RunCoroutine(Summoning());
    }

    private IEnumerator<float> Summoning()
    {
        summoning = true;
        var _player = FindObjectOfType<OVRScreenFade>().transform;

        transform.DOScaleX(summonScale.x, 0f);
        transform.DOScaleY(summonScale.y, 0f);
        yield return Timing.WaitForSeconds(.1f);
        Vector3 pos = _player.position + (_player.forward * .8f) + (_player.up * -.1f);
        if (defaultYSummon != 0f)
            pos.y = defaultYSummon;
        transform.position = pos;
        transform.LookAt(_player.transform);
        Toggle(true);
        yield return Timing.WaitForSeconds(1f);
        summoning = false;
    }

    private IEnumerator<float> Show()
    {
        isBusy = true;
        yield return Timing.WaitForOneFrame;
        if(transform.localScale.x >= defaultX) 
        {
            isBusy = false;
            yield break;
        }
        if (popupSound)
        {
            audio.PlayOneShot(popupSound);
            Debug.Log("audio pop : " + gameObject.name);
        }
        
        transform.DOScaleY(defaultY, animated ? .3f : 0f);
        if(animated)
            yield return Timing.WaitForSeconds(.25f);
        transform.DOScaleX(defaultX, animated ? .5f : 0f);
        isBusy = false;
    }

    private IEnumerator<float> Hide()
    {
        isBusy = true;
        yield return Timing.WaitForOneFrame;
        if (transform.localScale.y <= 0f)
        {
            isBusy = false;
            yield break;
        }
        if (shrinkSound)
        {
            if(audio)
            audio.PlayOneShot(shrinkSound);
            Debug.Log("audio hide : " + gameObject.name);
        }

        transform.DOScaleX(defaultX * .05f, animated ? .5f : 0f);
        if(animated)
            yield return Timing.WaitForSeconds(.45f);
        transform.DOScaleY(0f, animated ? .3f : 0f);
        isBusy = false;
    }
}
