﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Tictech.Oculus.Scenario;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TransformCall : MonoBehaviour
{
    public TextMeshProUGUI titleText;
    public Transform rotationPoint;
    public string grabber;
    public string lookTarget;
    public Vector3 offset;

    private LineRenderer _line;
    private Transform _targetLook;
    private Vector3 _targetPos;
    private Vector3 _startScale;
    private Vector3 _lineOffset;
    private RectTransform _panel;
    private bool _isActive;
    
    // Start is called before the first frame update
    void Start()
    {
        GameObject.Find(grabber).GetComponent<OVRGrabber>().onFocusChanged += FocusChanged;
        _lineOffset = offset * .95f;
        _targetLook = GameObject.Find(lookTarget).transform;
        _startScale = transform.localScale;
        _panel = rotationPoint.GetComponentInChildren<RectTransform>();
        _line = GetComponentInChildren<LineRenderer>();
        SetActive(false);
    }

    private void FocusChanged(OVRGrabbable grb)
    {
        if (!grb)
        {
            titleText.text = "";
            SetActive(false);
            return;
        }
        
        ScenarioItem item = grb.GetComponent<ScenarioItem>();
        if (!item)
            return;
        
        titleText.text = item.profile.displayName;
        Summon(item.indicatorPoint ? item.indicatorPoint : item.transform);
    }

    private void RebuildPanel()
    {
        LayoutRebuilder.ForceRebuildLayoutImmediate(_panel);
    }

    public void Summon(Transform call)
    {
        Invoke(nameof(RebuildPanel), .1f);
        transform.localScale = Vector3.zero;
        _targetPos = call.position;
        transform.position = _targetPos;
        _line.SetPosition(0, _targetPos);
        _line.SetPosition(1, _targetPos + _lineOffset);
        SetActive(true);
    }

    public void SetActive(bool value)
    {
        _isActive = value;
        if (_line)
            _line.enabled = value;
        transform.DOScale(_isActive ? _startScale : Vector3.zero, .2f);
    }

    private void FixedUpdate()
    {
        if (!_isActive)
            return;

        rotationPoint.position = _targetPos + offset;
        rotationPoint.LookAt(_targetLook);
    }
}
