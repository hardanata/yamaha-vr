﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using MEC;
using Tictech.Oculus.Scenario;
using UnityEngine;
using UnityEngine.UI;

public class OculusTransformCall : MonoBehaviour
{
    public OVRInput.Controller controllerType;

    private OVRGrabbable detached { get; set; }
    
    private Transform _player;

    public AudioClip toggleSound;
    public string nameInResources = "Buka Tutup Tool";
    private AudioSource audio;

    public List<GameObject> _baseGrabbables;
    private OVRGrabbable[] _grabbables;
    private Vector3[] _startPos;
    private Vector3[] _startScale;
    private Quaternion[] _startRot;

    private bool _isActive;
    
    // Start is called before the first frame update
    void Start()
    {
        if (!toggleSound)
        {
            if (ScenarioManager.Instance.audioReference.GetClip("Toggle Tool"))
                toggleSound = ScenarioManager.Instance.audioReference.GetClip("Toggle Tool");
            else toggleSound = Resources.Load<AudioClip>("Audio/Ambience/" + nameInResources);
        }

        _player = FindObjectOfType<OVRScreenFade>().transform;
        //_grabbables = GetComponentsInChildren<OVRGrabbable>();
        List<OVRGrabbable> listGrab = new List<OVRGrabbable>();
        foreach (var v in GetComponentsInChildren<OVRGrabbable>())
        {
            if (v.GetComponent<ScenarioGrabbable>())
                if (v.GetComponent<ScenarioGrabbable>().mainBodyGrabbable == true)
                {
                    listGrab.Add(v);
                    Debug.Log("Object v : "+v.gameObject);
                    GameObject vObject = Instantiate(v.gameObject);
                    _baseGrabbables.Add(Instantiate(vObject));
                }
        }
        _grabbables = listGrab.ToArray();
        _startPos = new Vector3[_grabbables.Length];
        _startScale = new Vector3[_grabbables.Length];
        _startRot = new Quaternion[_grabbables.Length];
        for (int i = 0; i < _grabbables.Length; i++)
        {
            int index = i;
            _grabbables[index].onGrabBegin.AddListener(delegate
            {
                if(_isActive) 
                    Timing.RunCoroutine(CopyTransform(index));
            });
            _startScale[index] = _grabbables[index].transform.localScale;
            _startPos[index] = _grabbables[index].transform.localPosition;
            _startRot[index] = _grabbables[index].transform.localRotation;
        }
        transform.localScale = Vector3.zero;
    }

    // Update is called once per frame
    void Update()
    {
        if (OVRInput.GetDown(OVRInput.RawButton.B))
        {
            if(detached)
                Destroy(detached.gameObject);
            else
                ToggleCall();
        }
        if (audio)
        {
            if (!audio.isPlaying)
            {
                if (gameObject.GetComponent<AudioSource>())
                    audio = null;
                Destroy(gameObject.GetComponent<AudioSource>());
            }
        }
    }

    private IEnumerator<float> CopyTransform(int index)
    {
        var dtc = _grabbables[index];
        dtc.transform.SetParent(null);
        dtc.onGrabBegin.RemoveAllListeners();
        dtc.ReInitialize();
        dtc.onGrabBegin.AddListener(delegate { detached = dtc; });
        dtc.onGrabRelease.AddListener(delegate { detached = null; });
        detached = dtc;
        
        yield return Timing.WaitForOneFrame;
        ToggleCall();
        yield return Timing.WaitForOneFrame;
            
        var grb = Instantiate(_baseGrabbables[index].gameObject, transform).transform;
        grb.localScale = _startScale[index];
        grb.localPosition = _startPos[index];
        grb.localRotation = _startRot[index];
        _grabbables[index] = grb.GetComponent<OVRGrabbable>();
        _grabbables[index].onGrabBegin.AddListener(delegate { Timing.RunCoroutine(CopyTransform(index)); });
        bool grabAudio = false;
        if (_grabbables[index].GetComponent<ScenarioGrabbable>())
            if (!string.IsNullOrEmpty(_grabbables[index].GetComponent<ScenarioGrabbable>().gAudio))
                grabAudio = true;
        if (grabAudio == false)
        {
            AudioSource audio;
            if (GetComponent<AudioSource>()) audio = GetComponent<AudioSource>();
            else audio = gameObject.AddComponent<AudioSource>();
            if (audio) audio.PlayOneShot(ScenarioManager.Instance.audioReference.GetClip("Grab Var Tool"));
        }
    }

    private void ToggleCall()
    {
        _isActive = !_isActive;
        playAudio();
        if (!_isActive)
        {
            transform.DOScale(Vector3.zero, .2f);
            return;
        }

        transform.localScale = Vector3.zero;
        
        transform.position = _player.position + (_player.forward * .4f) + (_player.up * -.05f);
        transform.LookAt(_player.transform);
        transform.DOScale(Vector3.one, .4f);
    }

    public void playAudio()
    {
        if (toggleSound) {
            audio = gameObject.AddComponent<AudioSource>();
            audio.clip = toggleSound;
            if (!audio.isPlaying)
                audio.Play();
        }
    }
}
