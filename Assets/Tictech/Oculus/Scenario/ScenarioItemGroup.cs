﻿using UnityEngine;
using UnityEngine.Events;

namespace Tictech.Oculus.Scenario
{
    public class ScenarioItemGroup : MonoBehaviour
    {
        public UnityEvent onCompleted;

        public bool Completed { get; set; }
        protected ScenarioItem[] items { get; set; }
        protected int _completedCount;

        private void Start()
        {
            Invoke(nameof(CompleteCheck), 1f);
            items = GetComponentsInChildren<ScenarioItem>();
            foreach (var e in items)
            {
                e.onCompleted += AddComplete;
            }
            SetActive(false);
        }

        private void CompleteCheck()
        {
            if (items.Length > 0)
                return;

            Completed = true;
            onCompleted?.Invoke();
        }

        private void AddComplete()
        {
            _completedCount++;
            if (_completedCount < items.Length)
                return;
            
            Completed = true;
            onCompleted?.Invoke();
        }

        public void SetActive(bool value)
        {
            foreach (var e in items)
            {
                e.SetActive(value);
            }
        }
    }
}
