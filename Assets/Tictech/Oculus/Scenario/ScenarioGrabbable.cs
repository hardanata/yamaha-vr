﻿using UnityEngine;

namespace Tictech.Oculus.Scenario
{
    public class ScenarioGrabbable : OVRGrabbable
    {
        public bool mainBodyGrabbable = true;
        [SerializeField] protected string grabAudio;
        public string gAudio { get; set; }

        protected override void Start()
        {
            base.Start();
            setAudio();
            gAudio = grabAudio;
        }

        public void setGrabAudio(string aud)
        {
            grabAudio = aud;
            setAudio();
        }

        private void setAudio()
        {
            if (!string.IsNullOrEmpty(grabAudio))
            {
                var source = gameObject.AddComponent<AudioSource>();
                source.loop = false;
                source.clip = ScenarioManager.Instance.audioReference.clips[grabAudio];
                onGrabBegin.AddListener(delegate { source.Play(); });
            }
        }

        public override void ReInitialize()
        {
            base.ReInitialize();
            if (!string.IsNullOrEmpty(grabAudio))
            {
                var source = GetComponent<AudioSource>();
                onGrabBegin.AddListener(delegate { source.Play(); });
            }
        }

        public void Detach()
        {
            if(grabbedBy)
                grabbedBy.GrabEnd();
        }
    }
}
