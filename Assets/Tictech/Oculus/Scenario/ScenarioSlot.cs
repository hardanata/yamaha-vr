﻿using System.Collections.Generic;
using System.Linq;
using MEC;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Rendering;
using DG.Tweening;

namespace Tictech.Oculus.Scenario
{
    public class ScenarioSlot : MonoBehaviour
    {
        public ScenarioSlotProfile profile;
        public SlotInfo[] itemStack;
        public ScenarioSlot[] requiredSlots;
        public bool requiredSlotsCompletion;
        public bool initializeOnStart;

        [ReadOnly]
        public bool occupied;
        [ReadOnly]
        public bool completed;

        public UnityAction onStackOccupied;
        public UnityAction onStackCompleted;
        public UnityEvent onSlotOccupied;
        public string onOccupiedAudio = "Occupy Var Slot";
        private AudioSource audio;
        public UnityEvent onSlotCompleted;

        [HideInInspector]
        public MeshRenderer _hologram;
        private Tween _hologramAnimation;
        public List<MeshRenderer> hologramList;
        private GameObject multipleHologramObject;

        [Header("Hologram")]
        [Tooltip("If true, the hologram will be animated even without the use of ScenarioItemHologramGroup")]
        public bool isHologramAnimated;
        public bool isReverseAnimation;
        public float animationDistance;
        public AxisAlign hologramAxis;
        public Transform hologramPoint;
        public Vector3 hologramOffset;

        /// <summary>
        /// Hologram Simulation
        /// </summary>
        public bool SimulateHologram;
        private bool isSimulating;
        private MeshRenderer hologramSimulation;
        public List<MeshRenderer> hologramSimulationList;
        private GameObject multipleHologramSimulationObject;
        private Vector3 defaultPos;

        private bool initiated = false;
        //private GameObject marker;

        private void Start()
        {
            gameObject.layer = LayerMask.NameToLayer("Interactable");
            GetComponent<Collider>().isTrigger = false;
            ScenarioManager.StepCount += itemStack.Length;
            if (GetComponent<AudioSource>())
                audio = GetComponent<AudioSource>();
            else audio = gameObject.AddComponent<AudioSource>();
            foreach (var e in itemStack)
            {
                if (e.desiredPoint == null)
                    e.desiredPoint = transform;
            }

            if (initializeOnStart)
                Initiate();
        }

        public void Initiate()
        {
            if (initiated)
                return;
            initiated = true;
            gameObject.tag = "Scenario Slot";
            if (requiredSlots == null)
                requiredSlots = new ScenarioSlot[0];
            GetComponent<Collider>().isTrigger = true;

            if (requiredSlots.Length == 0)
            {
                SetHologram(true);
                //marker = Instantiate(ScenarioManager.Instance.slotMarkerPrefab,
                //    itemStack[0].indicatorPoint
                //        ? itemStack[0].indicatorPoint.position
                //        : itemStack[0].desiredPoint.position, Quaternion.identity);
                //if (itemStack[0].indicatorPoint)
                //    marker.transform.parent = itemStack[0].indicatorPoint;
                //else marker.transform.parent = transform;
                return;
            }

            foreach (var e in requiredSlots)
            {
                if (e.completed)
                    CheckAvailability();
                else
                    e.onSlotCompleted.AddListener(CheckAvailability);
            }
        }

        public void Reindicate()
        {
            //if (marker)
            //{
            //    marker.transform.position = itemStack[0].indicatorPoint
            //        ? itemStack[0].indicatorPoint.position
            //        : itemStack[0].desiredPoint.position;
            //}
        }

        private void CheckAvailability()
        {
            foreach (var e in requiredSlots)
            {
                if (!e.Completed) return;
                if (!requiredSlotsCompletion) continue;
                if (e.itemStack.SelectMany(stack => stack.desiredPoint.GetComponentsInChildren<ScenarioSlot>()).Any(stackSlot => !stackSlot.Completed))
                {
                    return;
                }
            }

            //if (marker == null)
            //    marker = Instantiate(ScenarioManager.Instance.slotMarkerPrefab,
            //        itemStack[0].indicatorPoint ? itemStack[0].indicatorPoint.position : itemStack[0].desiredPoint.position,
            //        Quaternion.identity);

            Debug.Log("dff" + gameObject);
            SetHologram(true);

            //if (itemStack[0].indicatorPoint)
            //    marker.transform.parent = itemStack[0].indicatorPoint;
            //else marker.transform.parent = transform;

        }

        private void InitializeHologram()
        {
            if (_hologram || (!profile) || multipleHologramObject)
                return;

            if (!hologramPoint)
                hologramPoint = transform;

            if (!profile.isMultipleMesh)
            {
                var hl = GameObject.CreatePrimitive(PrimitiveType.Cube);

                if (profile)
                {
                    if (profile.meshScaleV3 != new Vector3(1, 1, 1))
                        hl.transform.localScale = profile.meshScaleV3;
                    else
                        hl.transform.localScale = Vector3.one * profile.meshScale;
                }
                else
                    hl.transform.localScale = Vector3.one;

                hl.transform.SetParent(hologramPoint);
                hl.transform.localPosition = Vector3.zero;
                hl.transform.localRotation = Quaternion.Euler(Vector3.zero);
                hl.transform.gameObject.name = transform.gameObject.name + "'s Hologram";

                Destroy(hl.GetComponent<Collider>());
                hl.GetComponent<MeshFilter>().mesh = profile.mesh;
                _hologram = hl.GetComponent<MeshRenderer>();
                _hologram.material = ScenarioManager.Instance.highlightMaterial;
                _hologram.shadowCastingMode = ShadowCastingMode.Off;
                _hologram.receiveShadows = false;
                _hologram.enabled = false;
            }
            else
            {
                multipleHologramObject = new GameObject();
                multipleHologramObject.transform.localScale = profile.meshParentScale;
                multipleHologramObject.transform.SetParent(hologramPoint);
                multipleHologramObject.transform.localPosition = Vector3.zero + hologramOffset;
                multipleHologramObject.transform.localRotation = Quaternion.Euler(Vector3.zero);
                multipleHologramObject.gameObject.name = gameObject.name + "'s Hologram List";

                for (int i = 0; i < profile.meshList.Length; i++)
                {
                    var hl = GameObject.CreatePrimitive(PrimitiveType.Cube);
                    hl.transform.localScale = profile.meshScaleList[i];
                    if (i == 0 && profile.parentHasMesh)
                        hl.transform.localScale = profile.meshParentScale;
                    hl.transform.SetParent(multipleHologramObject.transform);
                    hl.transform.localPosition = profile.meshPosList[i];
                    if (i == 0 && profile.parentHasMesh)
                        hl.transform.localPosition = Vector3.zero;
                    hl.transform.localRotation = profile.meshRotList[i];
                    if (i == 0 && profile.parentHasMesh)
                        hl.transform.localRotation = Quaternion.Euler(Vector3.zero);

                    Destroy(hl.GetComponent<Collider>());

                    hl.GetComponent<MeshFilter>().mesh = profile.meshList[i];

                    var temp = hl.GetComponent<MeshRenderer>();
                    temp.material = ScenarioManager.Instance.highlightMaterial;
                    temp.shadowCastingMode = ShadowCastingMode.Off;
                    temp.receiveShadows = false;
                    temp.enabled = true;
                    temp.gameObject.name = "Simulated Hologram of " + gameObject.name;
                    hologramList.Add(temp);
                }
            }
        }

        public void SetHologram(bool value)
        {
            if (value)
            {
                if (!_hologram)
                    InitializeHologram();

                if (_hologram)
                    _hologram.gameObject.SetActive(true);

                if (multipleHologramObject)
                    multipleHologramObject.SetActive(true);

                if (isHologramAnimated)
                {
                    SetHologramAnimation(true);
                    if (_hologram)
                        _hologram.enabled = true;

                    foreach (var item in hologramList)
                    {
                        item.enabled = true;
                    }
                }
                

            }
            else
            {
                SetHologramAnimation(false);

                if (!profile.isMultipleMesh)
                {
                    Destroy(_hologram);
                    _hologram = null;
                }
                else
                {
                    Destroy(multipleHologramObject);
                    multipleHologramObject = null;
                }

            }
        }

        public void EnableHolo()
        {
            if (!profile.isMultipleMesh)
            {
                _hologram.enabled = true;
            }
            else
            {
                multipleHologramObject.SetActive(true);
            }
            
        }

        public void SetHologramAnimation(bool value)
        {
            if (!profile.isMultipleMesh)
            {
                if (value && animationDistance != 0)
                {
                    if (!_hologram)
                        return;
                    if (_hologramAnimation != null)
                    {
                        _hologramAnimation.Kill();
                        _hologram.transform.localPosition = defaultPos;
                    }

                    defaultPos = _hologram.transform.localPosition;
                    switch (hologramAxis)
                    {
                        case AxisAlign.X:
                            if (isReverseAnimation)
                            {
                                var pos = _hologram.transform.localPosition;
                                _hologram.transform.localPosition = new Vector3(pos.x + animationDistance, pos.y, pos.z);
                                _hologramAnimation = _hologram.transform.DOLocalMoveX(pos.x, 1).SetLoops(-1, LoopType.Restart);
                            }
                            else
                                _hologramAnimation = _hologram.transform.DOLocalMoveX(_hologram.transform.localPosition.x + animationDistance, 1).SetLoops(-1, LoopType.Restart);
                            break;
                        case AxisAlign.Y:
                            if (isReverseAnimation)
                            {
                                var pos = _hologram.transform.localPosition;
                                _hologram.transform.localPosition = new Vector3(pos.x, pos.y + animationDistance, pos.z);
                                _hologramAnimation = _hologram.transform.DOLocalMoveY(pos.y, 1).SetLoops(-1, LoopType.Restart);
                            }
                            else
                                _hologramAnimation = _hologram.transform.DOLocalMoveY(_hologram.transform.localPosition.y + animationDistance, 1).SetLoops(-1, LoopType.Restart);
                            break;
                        case AxisAlign.Z:
                            if (isReverseAnimation)
                            {
                                var pos = _hologram.transform.localPosition;
                                _hologram.transform.localPosition = new Vector3(pos.x, pos.y, pos.z + animationDistance);
                                _hologramAnimation = _hologram.transform.DOLocalMoveZ(pos.z, 1).SetLoops(-1, LoopType.Restart);
                            }
                            else
                                _hologramAnimation = _hologram.transform.DOLocalMoveZ(_hologram.transform.localPosition.z + animationDistance, 1).SetLoops(-1, LoopType.Restart);
                            break;
                    }
                }
                else
                {
                    if (!_hologram)
                        return;

                    if (_hologramAnimation != null)
                        _hologramAnimation.Kill();
                }
            }
            else
            {
                if (value && animationDistance != 0)
                {
                    if (!multipleHologramObject)
                        return;

                    if (_hologramAnimation != null)
                    {
                        _hologramAnimation.Kill();
                        multipleHologramObject.transform.localPosition = defaultPos;
                    }

                    defaultPos = multipleHologramObject.transform.localPosition;
                    switch (hologramAxis)
                    {
                        case AxisAlign.X:
                            if (isReverseAnimation)
                            {
                                var pos = multipleHologramObject.transform.localPosition;
                                multipleHologramObject.transform.localPosition = new Vector3(pos.x + animationDistance, pos.y, pos.z);
                                _hologramAnimation = multipleHologramObject.transform.DOLocalMoveX(pos.x, 1).SetLoops(-1, LoopType.Restart);
                            }
                            else
                                _hologramAnimation = multipleHologramObject.transform.DOLocalMoveX(multipleHologramObject.transform.localPosition.x + animationDistance, 1).SetLoops(-1, LoopType.Restart);
                            break;
                        case AxisAlign.Y:
                            if (isReverseAnimation)
                            {
                                var pos = multipleHologramObject.transform.localPosition;
                                multipleHologramObject.transform.localPosition = new Vector3(pos.x, pos.y + animationDistance, pos.z);
                                _hologramAnimation = multipleHologramObject.transform.DOLocalMoveY(pos.y, 1).SetLoops(-1, LoopType.Restart);
                            }
                            else
                                _hologramAnimation = multipleHologramObject.transform.DOLocalMoveY(multipleHologramObject.transform.localPosition.y + animationDistance, 1).SetLoops(-1, LoopType.Restart);
                            break;
                        case AxisAlign.Z:
                            if (isReverseAnimation)
                            {
                                var pos = multipleHologramObject.transform.localPosition;
                                multipleHologramObject.transform.localPosition = new Vector3(pos.x, pos.y, pos.z + animationDistance);
                                _hologramAnimation = multipleHologramObject.transform.DOLocalMoveZ(pos.z, 1).SetLoops(-1, LoopType.Restart);
                            }
                            else
                                _hologramAnimation = multipleHologramObject.transform.DOLocalMoveZ(multipleHologramObject.transform.localPosition.z + animationDistance, 1).SetLoops(-1, LoopType.Restart);
                            break;
                    }
                }
                else
                {
                    if (!multipleHologramObject)
                        return;

                    if (_hologramAnimation != null)
                        _hologramAnimation.Kill();
                }
            }
        }

        public bool CheckItem(ScenarioItem item)
        {
            foreach (var e in requiredSlots)
            {
                if (!e.Completed)
                    return false;

                if (!requiredSlotsCompletion) continue;
                if (e.itemStack.SelectMany(stack => stack.desiredPoint.GetComponentsInChildren<ScenarioSlot>()).Any(stackSlot => !stackSlot.Completed))
                {
                    return false;
                }
            }

            foreach (var e in itemStack)
            {
                if (e.completed)
                    continue;

                if (e.occupied)
                    return false;

//                Debug.Log("Compare " + e.itemId + " = " + item.profile.itemId + " : " + e.itemId == item.profile.itemId);
                if (e.itemId == item.profile.itemId)
                    return true;
                break;
            }
            return false;
        }

        private bool Completed
        {
            get
            {
                foreach (var e in itemStack)
                {
                    if (!e.completed)
                        return false;
                }
                return true;
            }
        }

        public SlotInfo GetStackPoint(ScenarioItem item)
        {
            foreach (var e in itemStack)
            {
                if (e.completed)
                    continue;
                if (e.itemId == item.profile.itemId)
                    return e;
                break;
            }
            return null;
        }

        public void OccupySlot(ScenarioItem item)
        {
            foreach (var e in itemStack)
            {
                if (e.occupied)
                    continue;
                if (e.itemId == item.profile.itemId)
                {
                    e.occupied = true;
                    onStackOccupied?.Invoke();
                    onSlotOccupied.Invoke();
                    if (audio && !string.IsNullOrEmpty(onOccupiedAudio))
                        if (ScenarioManager.Instance.audioReference.clips[onOccupiedAudio])
                            audio.PlayOneShot(ScenarioManager.Instance.audioReference.clips[onOccupiedAudio]);
                    //if (marker != null)
                    //    marker.SetActive(false);
                    if (_hologram || multipleHologramObject)
                        SetHologram(false);

                    break;
                }
            }
            occupied = true;
        }

        public void CompleteSlot(ScenarioItem item)
        {
            bool transferMarker = false;
            foreach (var e in itemStack)
            {
                if (transferMarker)
                {
                    //marker.SetActive(true);
                    //marker.transform.SetParent(e.desiredPoint);
                    //marker.transform.localPosition = Vector3.zero;
                    return;
                }
                if (e.completed)
                    continue;
                if (e.itemId == item.profile.itemId)
                {
                    transferMarker = true;
                    e.completed = true;
                    ScenarioManager.CompletedStep++;
                    onStackCompleted?.Invoke();
                    //if (!marker)
                    //    CheckAvailability();
                    //marker.SetActive(false);


                    if (_hologram || multipleHologramObject)
                        SetHologram(false);
                }
            }

            completed = true;
            onSlotCompleted?.Invoke();
        }

        void OnDrawGizmos()
        {
            // Draws the Light bulb icon at position of the object.
            // Because we draw it inside OnDrawGizmos the icon is also pickable
            // in the scene view.

            Gizmos.DrawIcon(transform.position, "scenario slot gizmo.png", true);
        }
        private void Update()
        {
            if (SimulateHologram)
            {
                if (!profile.isMultipleMesh)
                {
                    SimulateHologram = false;
                    if (!isSimulating)
                    {
                        isSimulating = true;

                        //Initiating Hologram
                        if (!hologramPoint)
                            hologramPoint = transform;

                        var hl = GameObject.CreatePrimitive(PrimitiveType.Cube);

                        if (profile)
                        {
                            if (profile.meshScaleV3 != new Vector3(1, 1, 1))
                                hl.transform.localScale = profile.meshScaleV3;
                            else
                                hl.transform.localScale = Vector3.one * profile.meshScale;
                        }
                        else
                            hl.transform.localScale = Vector3.one;

                        hl.transform.SetParent(hologramPoint);
                        hl.transform.localPosition = Vector3.zero;
                        hl.transform.localRotation = Quaternion.Euler(Vector3.zero);

                        Destroy(hl.GetComponent<Collider>());

                        hl.GetComponent<MeshFilter>().mesh = profile.mesh;

                        hologramSimulation = hl.GetComponent<MeshRenderer>();
                        hologramSimulation.material = ScenarioManager.Instance.highlightMaterial;
                        hologramSimulation.shadowCastingMode = ShadowCastingMode.Off;
                        hologramSimulation.receiveShadows = false;
                        hologramSimulation.enabled = true;
                        hologramSimulation.gameObject.name = "Simulated Hologram of " + gameObject.name;

                        //Animating Hologram

                        switch (hologramAxis)
                        {
                            case AxisAlign.X:
                                if (isReverseAnimation)
                                {
                                    var pos = hologramSimulation.transform.localPosition;
                                    hologramSimulation.transform.localPosition = new Vector3(pos.x + animationDistance, pos.y, pos.z);
                                    hologramSimulation.transform.DOLocalMoveX(pos.x, 1).SetLoops(-1, LoopType.Restart);
                                }
                                else
                                {
                                    hologramSimulation.transform.DOLocalMoveX(hologramSimulation.transform.localPosition.x + animationDistance, 1).SetLoops(-1, LoopType.Restart);
                                }
                                break;
                            case AxisAlign.Y:
                                if (isReverseAnimation)
                                {
                                    var pos = hologramSimulation.transform.localPosition;
                                    hologramSimulation.transform.localPosition = new Vector3(pos.x, pos.y + animationDistance, pos.z);
                                    hologramSimulation.transform.DOLocalMoveY(pos.y, 1).SetLoops(-1, LoopType.Restart);
                                }
                                else
                                    hologramSimulation.transform.DOLocalMoveY(hologramSimulation.transform.localPosition.y + animationDistance, 1).SetLoops(-1, LoopType.Restart);
                                break;
                            case AxisAlign.Z:
                                if (isReverseAnimation)
                                {
                                    var pos = hologramSimulation.transform.localPosition;
                                    hologramSimulation.transform.localPosition = new Vector3(pos.x, pos.y, pos.z + animationDistance);
                                    hologramSimulation.transform.DOLocalMoveZ(pos.z, 1).SetLoops(-1, LoopType.Restart);
                                }
                                else
                                    hologramSimulation.transform.DOLocalMoveZ(hologramSimulation.transform.localPosition.z + animationDistance, 1).SetLoops(-1, LoopType.Restart);
                                break;
                        }
                    }
                    else
                    {
                        isSimulating = false;
                        Destroy(hologramSimulation.gameObject);
                    }
                }
                else
                {
                    SimulateHologram = false;
                    if (!isSimulating)
                    {
                        isSimulating = true;

                        if (!hologramPoint)
                            hologramPoint = transform;

                        multipleHologramSimulationObject = new GameObject();
                        multipleHologramSimulationObject.transform.localScale = profile.meshParentScale;
                        multipleHologramSimulationObject.transform.SetParent(hologramPoint);
                        multipleHologramSimulationObject.transform.localPosition = Vector3.zero + hologramOffset;
                        multipleHologramSimulationObject.transform.localRotation = Quaternion.Euler(Vector3.zero);
                        multipleHologramSimulationObject.gameObject.name = gameObject.name + "'s Hologram Simulation List";

                        for (int i = 0; i < profile.meshList.Length; i++)
                        {
                            var hl = GameObject.CreatePrimitive(PrimitiveType.Cube);
                            hl.transform.localScale = profile.meshScaleList[i];
                            if (i == 0 && profile.parentHasMesh)
                                hl.transform.localScale = profile.meshParentScale;
                            hl.transform.SetParent(multipleHologramSimulationObject.transform);
                            hl.transform.localPosition = profile.meshPosList[i];
                            if (i == 0 && profile.parentHasMesh)
                                hl.transform.localPosition = Vector3.zero;
                            hl.transform.localRotation = profile.meshRotList[i];
                            if (i == 0 && profile.parentHasMesh)
                                hl.transform.localRotation = Quaternion.Euler(Vector3.zero);

                            Destroy(hl.GetComponent<Collider>());

                            hl.GetComponent<MeshFilter>().mesh = profile.meshList[i];

                            var temp = hl.GetComponent<MeshRenderer>();
                            temp.material = ScenarioManager.Instance.highlightMaterial;
                            temp.shadowCastingMode = ShadowCastingMode.Off;
                            temp.receiveShadows = false;
                            temp.enabled = true;
                            temp.gameObject.name = "Simulated Hologram of " + gameObject.name;
                            hologramSimulationList.Add(temp);
                        }

                        switch (hologramAxis)
                        {
                            case AxisAlign.X:
                                if (isReverseAnimation)
                                {
                                    var pos = multipleHologramSimulationObject.transform.localPosition;
                                    multipleHologramSimulationObject.transform.localPosition = new Vector3(pos.x + animationDistance, pos.y, pos.z);
                                    multipleHologramSimulationObject.transform.DOLocalMoveX(pos.x, 1).SetLoops(-1, LoopType.Restart);
                                }
                                else
                                {
                                    multipleHologramSimulationObject.transform.DOLocalMoveX(multipleHologramSimulationObject.transform.localPosition.x + animationDistance, 1).SetLoops(-1, LoopType.Restart);
                                }
                                break;
                            case AxisAlign.Y:
                                if (isReverseAnimation)
                                {
                                    var pos = multipleHologramSimulationObject.transform.localPosition;
                                    multipleHologramSimulationObject.transform.localPosition = new Vector3(pos.x, pos.y + animationDistance, pos.z);
                                    multipleHologramSimulationObject.transform.DOLocalMoveY(pos.y, 1).SetLoops(-1, LoopType.Restart);
                                }
                                else
                                    multipleHologramSimulationObject.transform.DOLocalMoveY(multipleHologramSimulationObject.transform.localPosition.y + animationDistance, 1).SetLoops(-1, LoopType.Restart);
                                break;
                            case AxisAlign.Z:
                                if (isReverseAnimation)
                                {
                                    var pos = multipleHologramSimulationObject.transform.localPosition;
                                    multipleHologramSimulationObject.transform.localPosition = new Vector3(pos.x, pos.y, pos.z + animationDistance);
                                    multipleHologramSimulationObject.transform.DOLocalMoveZ(pos.z, 1).SetLoops(-1, LoopType.Restart);
                                }
                                else
                                    multipleHologramSimulationObject.transform.DOLocalMoveZ(multipleHologramSimulationObject.transform.localPosition.z + animationDistance, 1).SetLoops(-1, LoopType.Restart);
                                break;
                        }
                    }
                    else
                    {
                        isSimulating = false;
                        Destroy(multipleHologramSimulationObject.gameObject);
                    }
                }
            }
        }
    }

    public enum AxisAlign
    {
        X = 0,
        Y = 1,
        Z = 2,
        None = 3
    }

    [System.Serializable]
    public class SlotInfo
    {
        public string itemId;
        public Transform desiredPoint;
        public Transform indicatorPoint;
        public AxisAlign alignAxis;

        [ReadOnly]
        public bool occupied;
        [ReadOnly]
        public bool completed;

        public bool ValidItem(ScenarioItem item)
        {
            return itemId == item.profile.itemId &&
                   !(Vector3.Distance(desiredPoint.position, item.transform.position) > .1f) &&
                   !(Vector3.Distance(desiredPoint.rotation.eulerAngles, item.transform.rotation.eulerAngles) > 10f);
        }
    }


}
