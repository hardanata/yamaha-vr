﻿using UnityEngine;

namespace Tictech.Oculus.Scenario
{
    [CreateAssetMenu(fileName = "New Tool Profile", menuName = "Tictech/VR/Scenario/Tool Profile", order = 2)]
    public class ScenarioToolProfile : ScriptableObject
    {
        public string toolName;
        public string[] useItems;
    }
}