﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Video;

namespace Tictech.Oculus.Scenario
{
    public class ScenarioStepInfo : MonoBehaviour
    {
        [Header("General")]
        public string title;
        public VideoClip clip;
        [TextArea]
        public string detail;

        [Header("On Active")]
        public GameObject[] activateObject;
        public TeleportPoint[] teleportPath;
        public List<TeleportPoint> targetTeleport;

        [Header("Requirements")]
        public ScenarioItemGroup[] scenarioGroups;
        public ScenarioHologramGroupBase[] HologramGroups;

        public ScenarioItem[] itemCompletion;
        public ScenarioSlot[] stepSlots;
        public bool occupySlotCompletion;

        [Header("Events")]
        public UnityEvent onStepBegin;
        public UnityEvent onStepEnd;

        [SerializeField, ReadOnly]
        public bool Completed { get; set; }

        private int _stepCount;
        private int _currentStep = 0;

        private LocationTracker tracker;


        private IEnumerator Start()
        {
            _stepCount += scenarioGroups.Length;
            _stepCount += itemCompletion.Length;
            foreach (var e in stepSlots)
                _stepCount += e.itemStack.Length;

            yield return new WaitForSeconds(.1f);

            foreach (var e in itemCompletion)
            {
                e.SetActive(false);
            }
        }

        public void Activate()
        {
            foreach (var e in activateObject)
            {
                e.SetActive(true);
            }

            foreach (var e in teleportPath)
                e.gameObject.SetActive(true);

            if (targetTeleport.Count > 0)
            {
                tracker = LocationTracker.instance;
                tracker.SetTeleport(targetTeleport);
            }

            foreach (var e in scenarioGroups)
            {
                e.SetActive(true);
                if (e.Completed)
                    AddStep();
                else
                    e.onCompleted.AddListener(AddStep);
            }

            foreach (var e in itemCompletion)
            {
                if (e.Completed)
                    AddStep();
                else
                {
                    e.onCompleted += AddStep;
                }

                e.SetActive(true); // This will always enable the collider of scenario item assigned to itemCompletion on NextStep() 

                if (e.isAnimateOnStart)
                {
                    e.SetItemHologram(true);
                }
            }
            foreach (var e in stepSlots)
            {
                e.Initiate();
                if (occupySlotCompletion)
                {
                    if (e.occupied)
                        AddStep();
                    else
                        e.onStackOccupied += AddStep;
                }
                else
                {
                    if (e.completed)
                        AddStep();
                    else
                        e.onStackCompleted += AddStep;
                }
            }

            foreach(var e in HologramGroups)
            {
                e.AnimateHologram();
            }
        }

        private void AddStep()
        {
            if (Completed)
                return;

            _currentStep++;
            Debug.Log(title + ": " + _currentStep + "/" + _stepCount);
            if (_currentStep >= _stepCount)
                ScenarioManager.Instance.CompleteStep();
        }

        public void CompleteStep()
        {
            if (Completed)
                return;
            ScenarioManager.Instance.CompleteStep();
        }

        public void SetTeleport(TeleportPoint point)
        {
            tracker = LocationTracker.instance;
            tracker.SetTeleport(point);
        }
    }
}
