﻿using System.Collections;
using System.Collections.Generic;
using MEC;
using Sirenix.OdinInspector;
using Tictech.Oculus.Scenario;
using UnityEngine;

public class ScenarioToolHandRotate : MonoBehaviour
{
    public float rotateAmount;
    public float detachLimit;
    public AxisAlign axis;
    
    [Header("Read Only")] [SerializeField, ReadOnly]
    private float _rotateValue;
    
    private OVRGrabbable _grabber;
    private CoroutineHandle _rotateHandle;

    private void Grab()
    {
        _rotateHandle = Timing.RunCoroutine(HandleRotation());
    }

    private void Release()
    {
        Timing.KillCoroutines(_rotateHandle);
    }

    IEnumerator<float> HandleRotation()
    {
        yield return Timing.WaitForOneFrame;

        float lastRot = GetCurrentAxisRotation();

        //transform.LookAt(rotator);
        float tempRot = _rotateValue;
        float deltaRot;
        
        while (_grabber)
        {
            yield return Timing.WaitForOneFrame;
        }
    }
    
    private float GetCurrentAxisRotation()
    {
        switch (axis)
        {
            case AxisAlign.X:
                return transform.localEulerAngles.x;
            case AxisAlign.Y:
                return transform.localEulerAngles.y;
            case AxisAlign.Z:
                return transform.localEulerAngles.z;
            default:
                return 0f;
        }
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Hand"))
            return;
        
        _grabber = other.GetComponent<OVRGrabbable>();
        _grabber.onGrabBegin.AddListener(Grab);
        _grabber.onGrabBegin.AddListener(Release);
    }
    
    private void OnTriggerExit(Collider other)
    {
        if (!other.CompareTag("Hand"))
            return;
        
        _grabber.onGrabBegin.RemoveListener(Grab);
        _grabber.onGrabBegin.RemoveListener(Release);
        _grabber = null;
    }
}