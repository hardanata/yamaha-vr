﻿using UnityEngine;

namespace Tictech.Oculus.Scenario
{
    [CreateAssetMenu(fileName = "New Item Profile", menuName = "Tictech/VR/Scenario/Item Profile", order = 0)]
    public class ScenarioItemProfile : ScriptableObject
    {
        public string itemId;
        public string displayName;
    }
}