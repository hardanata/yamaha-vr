﻿using System.Collections;
using System.Collections.Generic;
using Arna.AssetManagement;
using Sirenix.OdinInspector;
using Tictech.Oculus.Scenario;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

[CreateAssetMenu(fileName = "New ScenarioDataScriptable", menuName = "Petrokimia/ScenarioData Scriptable", order = 0)]
public class ScenarioDataScriptable : SerializedScriptableObject
{
    public string title;
    public Dictionary<string, ScenarioTopic> data;

#if UNITY_EDITOR
    [Header("Editor Only")]
    [SerializeField]
    private string importFromPath;

    [Button]
    public void ImportData()
    {
        if (data == null)
            data = new Dictionary<string, ScenarioTopic>();
        Debug.Log("Importing scenario topic...");
        string[] guids = AssetDatabase.FindAssets("l:ScenarioTopic", new string[] { importFromPath });
        Debug.Log("Found " + guids.Length + " data...");
        foreach (string g in guids)
        {
            var topic = AssetDatabase.LoadAssetAtPath<ScenarioTopic>(AssetDatabase.GUIDToAssetPath(g));
            if (data.ContainsKey(topic.title))
                continue;
            data.Add(topic.title, topic);
            Debug.Log("Added data: " + topic.title);
        }

        EditorUtility.SetDirty(this);
    }
#endif
}
