﻿using System;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using MEC;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Rendering;

namespace Tictech.Oculus.Scenario
{
    public class ScenarioItem : MonoBehaviour
    {
        public enum State
        {
            Unchanged = 0,
            Locked = 1,
            Unlocked = 2,
            Completed = 3
        }

        public ScenarioItemProfile profile;
        public Transform indicatorPoint;
        public State currentState;
        public ItemToolInfo[] toolStack;

        public UnityAction onPlaced;
        public UnityAction onCompleted;
        public UnityAction<bool> onStateChanged;
        public UnityAction onSetHologram;

        [ReadOnly] public bool Completed;
        public OVRGrabbable Grabbable { get; private set; }
        public Collider OwnCollider { get; private set; }
        public bool IsActive { get; private set; }

        private GameObject _toolMarker;
        [HideInInspector] public ScenarioSlot _slot;
        private Transform _stackPoint;
        private MeshRenderer _highlight;
        private Rigidbody _rbd;
        private bool _detecting;


        [HideInInspector]
        public GameObject _hologram;
        private Tween _hologramAnimation;
        //private List<Tween> _hologramAnimationList = new List<Tween>();
        public List<MeshRenderer> _holoList = new List<MeshRenderer>();

        [Header("Hologram")]
        [Tooltip("If true, the hologram will be animated even without the use of ScenarioItemHologramGroup")]
        public bool isAnimateOnStart;
        public bool isReverseAnimation;
        public float animationDistance;
        public Vector3 hologramScaleOffset;
        public Vector3 hologramOffset;
        public AxisAlign hologramAxis;

        /// <summary>
        /// Hologram Simulation
        /// </summary>
        public bool SimulateHologram;
        private bool isSimulating;
        private GameObject hologramSimulation;
        private List<MeshRenderer> holoSimList = new List<MeshRenderer>();

        private void Awake()
        {
            IsActive = true;
            if (GetComponent<ScenarioItemHologramGroup>())
                Destroy(GetComponent<ScenarioItemHologramGroup>());
        }

        protected void Start()
        {
            gameObject.layer = LayerMask.NameToLayer("Interactable");
            OwnCollider = GetComponent<Collider>();
            Grabbable = GetComponent<OVRGrabbable>();
            if (currentState != State.Unchanged && Grabbable == null)
            {
                Grabbable = gameObject.AddComponent<ScenarioGrabbable>();
                Grabbable.onGrabBegin = new UnityEvent();
                Grabbable.onGrabRelease = new UnityEvent();
            }
            if(gameObject.GetComponent<ScenarioGrabbable>())
                if (string.IsNullOrEmpty(gameObject.GetComponent<ScenarioGrabbable>().gAudio))
                {
                    gameObject.GetComponent<ScenarioGrabbable>().setGrabAudio("Grab Var Item");
                }

            Timing.RunCoroutine(Initialize());
        }

        private IEnumerator<float> Initialize()
        {
            yield return Timing.WaitForOneFrame;
            gameObject.tag = "Scenario Item";
            ScenarioManager.StepCount += toolStack.Length;

            OwnCollider.isTrigger = true;
            if (!Grabbable)
            {
                yield return Timing.WaitForOneFrame;
                CheckToolStack();
            }
            else
            {
                Grabbable.onGrabBegin.AddListener(delegate { _detecting = true; });
                Grabbable.onGrabRelease.AddListener(Released);
                _rbd = GetComponent<Rigidbody>();
                InitializeHighlight();
                InitializeHologram();
            }
            SetState(currentState);
        }

        //public void SetIndicator(bool value)
        //{
        //    if (!indicatorPoint)
        //        return;

        //    if (indicatorPoint.childCount == 0 && currentState != State.Locked)
        //    {
        //        var marker = Instantiate(ScenarioManager.Instance.slotMarkerPrefab).transform;
        //        marker.SetParent(indicatorPoint, true);
        //        marker.localPosition = Vector3.zero;
        //    }

        //    indicatorPoint.gameObject.SetActive(value);
        //}

        public void SetItemHologram(bool value)
        {
            if (value)
            {
                if (currentState == ScenarioItem.State.Locked || _slot)
                {
                    if (_holoList.Count <= 0)
                        foreach (var item in _holoList)
                        {
                            item.enabled = false;
                        }


                    if (isAnimateOnStart)
                        SetToolHologram();
                }
                else
                {
                    if (_holoList.Count <= 0)
                        InitializeHologram();

                    foreach (var item in _holoList)
                    {
                        item.enabled = true;
                    }

                    if (isAnimateOnStart)
                        SetHologramAnimation(true);
                }

                onCompleted += delegate { SetItemHologram(false); };

                if (Grabbable)
                {
                    Grabbable.onGrabBegin.AddListener(delegate
                    {
                        SetHologramAnimation(false);
                        if (_holoList.Count > 0)
                            foreach (var item in _holoList)
                            {
                                item.enabled = false;
                            }
                    });

                    Grabbable.onGrabRelease.AddListener(delegate
                    {
                        if (_holoList.Count > 0)
                            foreach (var item in _holoList)
                            {
                                item.enabled = true;
                            }
                    });
                }
            }
            else
            {
                foreach (var item in _holoList)
                {
                    Destroy(item.gameObject);
                }
                _holoList.Clear();
            }

            if (onSetHologram != null)
                onSetHologram.Invoke();
        }

        public void SetToolHologram()
        {
            foreach (var tool in toolStack)
            {
                if (tool.hologramPrefab && !tool.completed)
                {

                    if (tool.hologramObject)
                        Destroy(tool.hologramObject);

                    if (!tool.hologramPoint)
                        tool.hologramPoint = tool.attachPoint;

                    var _holo = Instantiate(tool.hologramPrefab);
                    tool.hologramObject = _holo;
                    tool.hologramObject.transform.parent = tool.hologramPoint;
                    tool.hologramObject.transform.localPosition = Vector3.zero;
                    tool.hologramObject.transform.localRotation = Quaternion.identity;

                    tool.onCompleted.AddListener(delegate
                    {
                        if (tool.hologramObject)
                            Destroy(tool.hologramObject);
                    });
                    break;

                }
            }
        }
        public void SetHologramAnimation(bool value)
        {
            if (value)
            {
                if (_holoList.Count <= 0)
                    return;

                if (_hologramAnimation != null)
                    _hologramAnimation.Kill();

                //foreach (var item in _holoList)
                //{
                //    item.transform.localPosition = Vector3.zero + hologramOffset;
                //}
                _hologram.transform.localPosition = Vector3.zero + hologramOffset;

                //switch (hologramAxis)
                //{
                //    case AxisAlign.X:
                //        if (isReverseAnimation)
                //        {
                //            foreach (var hs in holoSimList)
                //            {
                //                var pos = hs.transform.localPosition;
                //                hs.transform.localPosition = new Vector3(pos.x + animationDistance, pos.y, pos.z);
                //                _hologramAnimationList.Add(hs.transform.DOLocalMoveX(pos.x, 1).SetLoops(-1, LoopType.Restart));
                //            }
                //        }
                //        else
                //            foreach (var hs in holoSimList)
                //            {
                //                _hologramAnimationList.Add(hs.transform.DOLocalMoveX(hs.transform.localPosition.x + animationDistance, 1).SetLoops(-1, LoopType.Restart));
                //            }
                //        break;
                //    case AxisAlign.Y:
                //        if (isReverseAnimation)
                //        {
                //            foreach (var hs in holoSimList)
                //            {
                //                var pos = hs.transform.localPosition;
                //                hs.transform.localPosition = new Vector3(pos.x, pos.y + animationDistance, pos.z);
                //                _hologramAnimationList.Add(hs.transform.DOLocalMoveY(pos.y, 1).SetLoops(-1, LoopType.Restart));
                //            }
                //        }
                //        else
                //            foreach (var hs in holoSimList)
                //            {
                //                _hologramAnimationList.Add(hs.transform.DOLocalMoveY(hs.transform.localPosition.y + animationDistance, 1).SetLoops(-1, LoopType.Restart));
                //            }
                //        break;
                //    case AxisAlign.Z:
                //        if (isReverseAnimation)
                //        {
                //            foreach (var hs in holoSimList)
                //            {
                //                var pos = hs.transform.localPosition;
                //                hs.transform.localPosition = new Vector3(pos.x, pos.y, pos.z + animationDistance);
                //                _hologramAnimationList.Add(hs.transform.DOLocalMoveZ(pos.z, 1).SetLoops(-1, LoopType.Restart));
                //            }
                //        }
                //        else
                //            foreach (var hs in holoSimList)
                //            {
                //                _hologramAnimationList.Add(hs.transform.DOLocalMoveZ(hs.transform.localPosition.z + animationDistance, 1).SetLoops(-1, LoopType.Restart));
                //            }
                //        break;
                //}




                switch (hologramAxis)
                {
                    case AxisAlign.X:
                        if (isReverseAnimation)
                        {
                            var pos = _hologram.transform.localPosition;
                            _hologram.transform.localPosition = new Vector3(pos.x + animationDistance, pos.y, pos.z);
                            _hologramAnimation = _hologram.transform.DOLocalMoveX(pos.x, 1).SetLoops(-1, LoopType.Restart);
                        }
                        else
                            _hologramAnimation = _hologram.transform.DOLocalMoveX(_hologram.transform.localPosition.x + animationDistance, 1).SetLoops(-1, LoopType.Restart);
                        break;
                    case AxisAlign.Y:
                        if (isReverseAnimation)
                        {
                            var pos = _hologram.transform.localPosition;
                            _hologram.transform.localPosition = new Vector3(pos.x, pos.y + animationDistance, pos.z);
                            _hologramAnimation = _hologram.transform.DOLocalMoveY(pos.y, 1).SetLoops(-1, LoopType.Restart);
                        }
                        else
                            _hologramAnimation = _hologram.transform.DOLocalMoveY(_hologram.transform.localPosition.y + animationDistance, 1).SetLoops(-1, LoopType.Restart);
                        break;
                    case AxisAlign.Z:
                        if (isReverseAnimation)
                        {
                            var pos = _hologram.transform.localPosition;
                            _hologram.transform.localPosition = new Vector3(pos.x, pos.y, pos.z + animationDistance);
                            _hologramAnimation = _hologram.transform.DOLocalMoveZ(pos.z, 1).SetLoops(-1, LoopType.Restart);
                        }
                        else
                            _hologramAnimation = _hologram.transform.DOLocalMoveZ(_hologram.transform.localPosition.z + animationDistance, 1).SetLoops(-1, LoopType.Restart);
                        break;
                }
            }
            else
            {
                if (_holoList.Count <= 0)
                    return;

                if (_hologramAnimation != null)
                {
                    _hologramAnimation.Kill();
                    

                    _hologram.transform.localPosition = Vector3.zero + hologramOffset;
                }
            }
        }


        public void SetActive(bool value)
        {
            if (value && currentState == State.Locked)
                CheckToolStack();
            //if (indicatorPoint)
            //    SetIndicator(value);

            if (currentState == State.Locked)
                return;

            if (Grabbable)
                Grabbable.enabled = value;
            if (!OwnCollider)
                OwnCollider = GetComponent<Collider>();
            if (OwnCollider)
                OwnCollider.enabled = value;
            IsActive = value;
            IsActive = value;

            onStateChanged?.Invoke(value);

//            var HG = transform.parent.GetComponent<ScenarioItemHologramGroup>();
//            
//            print(HG.name + "have group!");
//            if (HG)
//            {
//                print("have group!");
//                if (HG.IsDisableGrabbableOnStart)
//                    HG.DisableGrabbable(this);
//            }

        }

        public void PlayAudio(string key)
        {
            if (!GetComponent<AudioSource>()) gameObject.AddComponent<AudioSource>();
            GetComponent<AudioSource>().PlayOneShot(ScenarioManager.Instance.audioReference.GetClip(key));
        }

        public void PlayAudioArray(string key)
        {
            if (!GetComponent<AudioSource>()) gameObject.AddComponent<AudioSource>();
            GetComponent<AudioSource>().PlayOneShot(ScenarioManager.Instance.audioReference.GetClipArray(key));
        }

        private void CheckToolStack()
        {
            if (toolStack.Length <= 0) return;
            //if (!_toolMarker)
            // _toolMarker = Instantiate(ScenarioManager.Instance.toolMarkerPrefab);

            var tool = toolStack.FirstOrDefault(e => !e.completed);
            if (tool != null)
            {
                //_toolMarker.transform.parent = tool.indicator ? tool.indicator : tool.attachPoint;
                //_toolMarker.transform.localPosition = Vector3.zero;// tool.attachPoint.position;

                SetItemHologram(true);
            }
            else
            {
                //Destroy(_toolMarker);
                if (currentState == State.Unlocked)
                {
                    //SetIndicator(true);
                    SetItemHologram(true);
                }
            }
            if (_slot)
                SetItemHologram(true);
        }


        //Create Hologram From this gameobject's mesh
        private void InitializeHologram()
        {
            if (_holoList.Count > 0)
                foreach (var item in _holoList)
                {
                    Destroy(item.gameObject);
                }
                _holoList.Clear();

            if (!_hologram)
            {
                _hologram = new GameObject();
                _hologram.transform.SetParent(GetComponentInChildren<MeshFilter>().transform);
                _hologram.transform.localPosition = Vector3.zero;
                _hologram.transform.localRotation = Quaternion.Euler(Vector3.zero);
                _hologram.gameObject.name = _hologram.transform.parent.gameObject.name + "'s Hologram List";
                _hologram.transform.localScale = Vector3.one;
            }
            else{
                _hologram.transform.localPosition = Vector3.zero;
                _hologram.transform.localRotation = Quaternion.Euler(Vector3.zero);
                _hologram.transform.localScale = Vector3.one;
            }

            foreach (var mf in GetComponentsInChildren<MeshFilter>())
            {
                if (mf.gameObject.name == gameObject.name + "'s HighlightForSnap")
                    continue;

                if (mf.transform.parent && mf.transform.parent.name == gameObject.name + "'s Hologram List")
                    continue;

                var hl = GameObject.CreatePrimitive(PrimitiveType.Cube);
                hl.transform.SetParent(mf.transform);
                hl.transform.gameObject.name = transform.gameObject.name + "'s Hologram";
                hl.transform.localPosition = Vector3.zero + hologramOffset;
                hl.transform.localRotation = Quaternion.Euler(Vector3.zero);
                hl.transform.localScale = Vector3.one + hologramScaleOffset;
                Destroy(hl.GetComponent<Collider>());
                hl.GetComponent<MeshFilter>().mesh = mf.mesh;

                MeshRenderer _hologramSimulation = new MeshRenderer();

                _hologramSimulation = hl.GetComponent<MeshRenderer>();
                _hologramSimulation.material = ScenarioManager.Instance.highlightMaterial;
                _hologramSimulation.shadowCastingMode = ShadowCastingMode.Off;
                _hologramSimulation.receiveShadows = false;
                _hologramSimulation.enabled = false;
                _hologramSimulation.gameObject.name = "Hologram of " + gameObject.name;
                _hologramSimulation.transform.SetParent(_hologram.transform);

                _holoList.Add(_hologramSimulation);
                print("spawn!" + gameObject.name);
            }

            //var hl = GameObject.CreatePrimitive(PrimitiveType.Cube);
            //hl.transform.SetParent(GetComponentInChildren<MeshFilter>()?.transform);
            //hl.transform.gameObject.name = transform.gameObject.name + "'s Hologram";
            //hl.transform.localPosition = Vector3.zero + hologramOffset;
            //hl.transform.localRotation = Quaternion.Euler(Vector3.zero);
            //hl.transform.localScale = Vector3.one;
            //Destroy(hl.GetComponent<Collider>());
            //hl.GetComponent<MeshFilter>().mesh = GetComponentInChildren<MeshFilter>()?.mesh;
            //_hologram = hl.GetComponent<MeshRenderer>();
            //_hologram.material = ScenarioManager.Instance.highlightMaterial;
            //_hologram.shadowCastingMode = ShadowCastingMode.Off;
            //_hologram.receiveShadows = false;
            //_hologram.enabled = false;
        }

        private void InitializeHighlight()
        {
            var hl = GameObject.CreatePrimitive(PrimitiveType.Cube);
            hl.transform.SetParent(GetComponentInChildren<MeshFilter>()?.transform);
            hl.transform.localPosition = Vector3.zero;
            hl.transform.localRotation = Quaternion.Euler(Vector3.zero);
            hl.transform.localScale = Vector3.one;
            Destroy(hl.GetComponent<Collider>());
            hl.GetComponent<MeshFilter>().mesh = GetComponentInChildren<MeshFilter>()?.mesh;
            _highlight = hl.GetComponent<MeshRenderer>();
            _highlight.material = ScenarioManager.Instance.highlightMaterial;
            _highlight.shadowCastingMode = ShadowCastingMode.Off;
            _highlight.receiveShadows = false;
            _highlight.enabled = false;
            _highlight.gameObject.name = gameObject.name + "'s HighlightForSnap";
        }

        private void Released()
        {
            _detecting = false;
            _highlight.enabled = false;
            if (!_slot || !_stackPoint || ScenarioManager.Instance.IsStepTransition)
                return;

            if (!_slot.CheckItem(this))
                return;

            _slot.OccupySlot(this);
            //SetIndicator(false);
            CheckToolStack();
            transform.SetParent(_stackPoint);

            var complete = !toolStack.Any(e => e.completed == false);
            foreach (var e in toolStack)
            {
                complete = e.completed;
                if (!complete)
                    break;
            }
            var offset = toolStack.Aggregate(Vector3.zero, (current, e) => current + e.offset);

            transform.DOLocalMove(offset, 1f);
            transform.DOLocalRotate(Vector3.zero, 1f);

            Destroy(_rbd);
            Destroy(Grabbable);
            CheckToolStack();
            SetToolHologram();
            onPlaced?.Invoke();
            if (complete)
                Complete();
        }

        public void ForceComplete()
        {
            Complete();
        }

        private void Complete()
        {
            //SetIndicator(false);
            Debug.Log("Item " + name + " completed.");
            if (_slot)
                _slot.CompleteSlot(this);
            Completed = true;
            onCompleted?.Invoke();
            Destroy(GetComponent<Collider>());
            if (_highlight)
                Destroy(_highlight.gameObject);
            Destroy(this);
        }

        protected void OnTriggerEnter(Collider other)
        {
            if (!_detecting || !other.CompareTag("Scenario Slot"))
                return;

            var nSlot = other.GetComponent<ScenarioSlot>();

            if (!nSlot.CheckItem(this))
            {
                ScenarioManager.Mistake++;
                return;
            }

            var p = nSlot.GetStackPoint(this);
            _stackPoint = p.desiredPoint;
            Grabbable.overrideTransform = _stackPoint;
            Grabbable.overrideAxis = (OVRGrabbable.AxisAlign)((int)p.alignAxis);
            _highlight.enabled = true;
            _slot = nSlot;
        }

        protected void OnTriggerExit(Collider other)
        {
            if (!other.CompareTag("Scenario Slot"))
                return;

            var nSlot = other.GetComponent<ScenarioSlot>();
            if (nSlot != _slot) return;
            _stackPoint = null;
            Grabbable.overrideTransform = null;
            _highlight.enabled = false;
            _slot = null;
        }

        public void ToggleHighlight(bool value)
        {
            _highlight.enabled = value;
        }

        public IEnumerator<float> CompleteTool(ItemToolInfo tool, float delay)
        {
           // Debug.Log("Completed tool stack " + tool.toolId + " of " + profile.itemId);
            ScenarioManager.CompletedStep++;

            tool.completed = true;

            bool enableCollider = false;
            if (OwnCollider)
            {
                enableCollider = OwnCollider.enabled;
                if (enableCollider)
                    OwnCollider.enabled = false;
            }

            if (delay > 0f)
            {
                yield return Timing.WaitForSeconds(1f);
                var offset = toolStack.Aggregate(Vector3.zero, (current, e) => current + (e.completed ? Vector3.zero : e.offset)); 
                transform.DOLocalMove(offset, delay - 1f);
                yield return Timing.WaitForSeconds(delay - 1f);
            }

            yield return Timing.WaitForOneFrame;

            if (OwnCollider) OwnCollider.enabled = enableCollider;
            //yield return Timing.WaitForSeconds(1f);
            tool.onCompleted?.Invoke();
            
            SetState(tool.completeState);
            CheckToolStack();
            
            if (toolStack.Any(e => !e.completed) || currentState != State.Unchanged)
                yield break;

            Complete();
        }

        public void SetState(int id)
        {
            SetState((State)id);
        }

        public void SetState(State state)
        {
            if (currentState == State.Completed || state == State.Unchanged)
                return;

            currentState = state;

            switch (state)
            {
                case State.Unchanged:
                    break;
                case State.Locked:
                    OwnCollider.enabled = false;
                    Grabbable.enabled = false;
                    break;
                case State.Unlocked:
                    OwnCollider.enabled = true;
                    Grabbable.enabled = true;
                    //SetIndicator(true);
                    SetItemHologram(true);
                    break;
                case State.Completed:
                    Complete();
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(state), state, null);
            }
        }

        public ItemToolInfo GetToolPoint(string toolId)
        {
            print("grab null: " + (Grabbable == null) + ", tool id: " + toolId);
            return (Grabbable && currentState == State.Unchanged) ? null : toolStack.FirstOrDefault(e => !e.completed && e.toolId == toolId);
            //print("item null: " + tool == null + " toolid: " + tool == null ? "null" : tool.toolId);
            //return tool == null || tool.completed ? null : tool;
        }

        void OnDrawGizmos()
        {
            // Draws the Light bulb icon at position of the object.
            // Because we draw it inside OnDrawGizmos the icon is also pickable
            // in the scene view.

            Gizmos.DrawIcon(transform.position, "scenario item gizmo.png", true);
        }
        private void Update()
        {
            if (SimulateHologram)
            {
                SimulateHologram = false;
                if (!isSimulating)
                {
                    isSimulating = true;


                    hologramSimulation = new GameObject();
                    hologramSimulation.transform.SetParent(GetComponentInChildren<MeshFilter>().transform);
                    hologramSimulation.transform.localPosition = Vector3.zero;
                    hologramSimulation.transform.localRotation = Quaternion.Euler(Vector3.zero);
                    hologramSimulation.gameObject.name = gameObject.name + "'s Hologram Simulation List";
                    hologramSimulation.transform.localScale = Vector3.one;


                    foreach (var mf in GetComponentsInChildren<MeshFilter>())
                    {
                        if (mf.gameObject.name == gameObject.name + "'s HighlightForSnap" || mf.transform.parent.gameObject.name == hologramSimulation.transform.parent.gameObject.name + "'s Hologram List")
                            continue;

                        var hl = GameObject.CreatePrimitive(PrimitiveType.Cube);
                        hl.transform.SetParent(mf.transform);
                        hl.transform.gameObject.name = transform.gameObject.name + "'s Hologram";
                        hl.transform.localPosition = Vector3.zero + hologramOffset;
                        hl.transform.localRotation = Quaternion.Euler(Vector3.zero);
                        hl.transform.localScale = Vector3.one;
                        Destroy(hl.GetComponent<Collider>());
                        hl.GetComponent<MeshFilter>().mesh = mf.mesh;

                        MeshRenderer _hologramSimulation = new MeshRenderer();

                        _hologramSimulation = hl.GetComponent<MeshRenderer>();
                        _hologramSimulation.material = ScenarioManager.Instance.highlightMaterial;
                        _hologramSimulation.shadowCastingMode = ShadowCastingMode.Off;
                        _hologramSimulation.receiveShadows = false;
                        _hologramSimulation.enabled = true;
                        _hologramSimulation.gameObject.name = "Simulated Hologram of " + gameObject.name;
                        _hologramSimulation.transform.SetParent(hologramSimulation.transform);

                        holoSimList.Add(_hologramSimulation);
                    }
                    switch (hologramAxis)
                    {
                        case AxisAlign.X:
                            if (isReverseAnimation)
                            {
                                var pos = hologramSimulation.transform.localPosition;
                                hologramSimulation.transform.localPosition = new Vector3(pos.x + animationDistance, pos.y, pos.z);
                                hologramSimulation.transform.DOLocalMoveX(pos.x, 1).SetLoops(-1, LoopType.Restart);
                            }
                            else
                                hologramSimulation.transform.DOLocalMoveX(hologramSimulation.transform.localPosition.x + animationDistance, 1).SetLoops(-1, LoopType.Restart);

                            break;
                        case AxisAlign.Y:
                            if (isReverseAnimation)
                            {
                                var pos = hologramSimulation.transform.localPosition;
                                hologramSimulation.transform.localPosition = new Vector3(pos.x, pos.y + animationDistance, pos.z);
                                hologramSimulation.transform.DOLocalMoveY(pos.y, 1).SetLoops(-1, LoopType.Restart);
                            }
                            else
                                hologramSimulation.transform.DOLocalMoveY(hologramSimulation.transform.localPosition.y + animationDistance, 1).SetLoops(-1, LoopType.Restart);
                            break;
                        case AxisAlign.Z:
                            if (isReverseAnimation)
                            {
                                var pos = hologramSimulation.transform.localPosition;
                                hologramSimulation.transform.localPosition = new Vector3(pos.x, pos.y, pos.z + animationDistance);
                                hologramSimulation.transform.DOLocalMoveZ(pos.z, 1).SetLoops(-1, LoopType.Restart);
                            }
                            else
                                hologramSimulation.transform.DOLocalMoveZ(hologramSimulation.transform.localPosition.z + animationDistance, 1).SetLoops(-1, LoopType.Restart);
                            break;
                    }
                }
                else
                {
                    isSimulating = false;
                    //foreach (var hs in holoSimList)
                    //{
                    //    hs.gameObject.Destroy();
                    //}
                    Destroy(hologramSimulation);
                    holoSimList.Clear();
                }

            }
        }
    
        public void SetHologramOff()
        {
            if (_hologram)
            {
                _hologram.SetActive(false);
            }
        }
    }

    [Serializable]
    public class ItemToolInfo
    {
        public string toolId;
        public Vector3 offset;
        public Transform attachPoint;
        public Transform indicator;
        public Transform hologramPoint;
        public ScenarioItem.State completeState;
        [ReadOnly]
        public bool completed;
        public GameObject hologramPrefab;
         public GameObject hologramObject;

        public UnityEvent onCompleted;
    }
}