﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using MEC;
using UnityEngine.UI;

public class Transition : MonoBehaviour
{
    public RectTransform LayoutRoot { get; set; }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void summonToggle(GameObject go)
    {
        if (go.activeSelf)
            g_Out(go);
        else g_In(go);
    }

    public void g_Out(GameObject go)
    {
        StartCoroutine("growOut", go);
    }

    public void g_In(GameObject go)
    {
        StartCoroutine("growIn", go);
    }

    public void setRefreshLayoutRoot(RectTransform layoutRoot)
    {
        LayoutRoot = layoutRoot;
    }

    public IEnumerator growOut(GameObject go)
    {
        go.transform.DOScale(Vector3.zero, 1f);
        yield return Timing.WaitForSeconds(1f);
        go.gameObject.SetActive(false);
        if (LayoutRoot) LayoutRebuilder.ForceRebuildLayoutImmediate(LayoutRoot);
    }

    public IEnumerator growIn(GameObject go)
    {
        yield return Timing.WaitForSeconds(1f);
        go.gameObject.SetActive(true);
        go.transform.DOScale(Vector3.one, 1f);
        yield return Timing.WaitForSeconds(1f);
        if (LayoutRoot) LayoutRebuilder.ForceRebuildLayoutImmediate(LayoutRoot);
    }

    public void rectHeightLerp(GameObject go)
    {
        /*go.GetComponent<RectTransform>().DOSizeDelta(
            new Vector2(go.GetComponent<RectTransform>().rect.width, 
            go.GetComponent<RectTransform>().rect.height), 
            1f, 
            false);*/
        //float xFloat = go.GetComponent<RectTransform>().rect.height;
        //DOTween.To(() => go.GetComponent<RectTransform>().rect.height, x => go.GetComponent<RectTransform>().rect.height, go.GetComponent<RectTransform>().rect.height, 1);
    }
}
