﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System.Linq;

namespace Tictech.Oculus.Scenario
{
    public class SubScenarioManager : SerializedMonoBehaviour
    {
        public static SubScenarioManager instance;
        public enum basedOf
        {
            ScenarioLoader,
            SubScenarioList
        }

        public basedOf sortingBasedOf = basedOf.ScenarioLoader;
        public GameObject ActiveSubScenario { get; set; }
        public string defaultTopic;
        public string defaultScenario;
        [SerializeField]
        public Dictionary<string, GameObject> SubScenarioList;
        GameObject pref;

        private int currentIndex;

        private void Start()
        {
            if (string.IsNullOrEmpty(ScenarioLoader.SelectedTopic))
            {
                ScenarioLoader.SelectedTopic = defaultTopic;
                ScenarioLoader.SelectedScenario = defaultScenario;
            }

            if (!ScenarioLoader.GetSelectedScenario())
            {
                Debug.LogError("No selected scenario, consider set up default topic and scenario.");
                return;
            }
            
            currentIndex = ScenarioLoader.GetSelectedTopic().scenarios.IndexOf(ScenarioLoader.GetSelectedScenario());
            if (sortingBasedOf == basedOf.SubScenarioList)
                SpawnSubScenario();
            else SpawnSubScenario(ScenarioLoader.GetSelectedScenario().prefabName);
        }

        private void Awake()
        {
            instance = this;
        }

        public void SpawnSubScenario(string subScenario)
        {
            if (ActiveSubScenario)
                Destroy(ActiveSubScenario);
            if (ScenarioManager.Instance)
                ScenarioManager.Instance = null;
            ActiveSubScenario = Instantiate(SubScenarioList[subScenario]);
            pref = SubScenarioList[subScenario];
            Debug.Log("Scenario Spawn " + ActiveSubScenario);
        }
        
        public void SpawnSubScenario()
        {
            if (ActiveSubScenario)
                Destroy(ActiveSubScenario);
            if (ScenarioManager.Instance)
                ScenarioManager.Instance = null;

            var key = ScenarioLoader.GetSelectedScenario().prefabName;
            //if (sortingBasedOf == basedOf.SubScenarioList)
                ActiveSubScenario = Instantiate(SubScenarioList[key]);
            //else ActiveSubScenario = Instantiate(SubScenarioList.Values.Where(x => x.name == ScenarioLoader.GetSelectedScenario().prefabName).FirstOrDefault());
            pref = SubScenarioList[key];
            Debug.Log("Scenario Spawn " + ActiveSubScenario);
        }

        public void NextSubScenario()
        {
            LocationTracker.instance.ClearTarget();
            if(ScenarioManager.Instance)
                ScenarioManager.Instance.ClearTool();
            ScenarioManager.Instance = null;
            foreach (var item in GameObject.FindGameObjectsWithTag("Scenario Tool"))
            {
                Destroy(item.gameObject);
            }
            if (sortingBasedOf == basedOf.ScenarioLoader)
            {
                if (ScenarioLoader.instance)
                {
                    bool next = false;
                    
                    foreach (var item in ScenarioLoader.instance.scenarioData.data[ScenarioLoader.SelectedTopic].scenarios)
                    {
                        if (!next)
                            if (item.prefabName == ScenarioLoader.SelectedScenario)
                            {
                                next = true;
                                continue;
                            }
                        if (next &&
                    item.tags.Count == ScenarioLoader.GetSelectedScenario().tags.Count &&
                    item.tags.FirstOrDefault() == ScenarioLoader.GetSelectedScenario().tags.FirstOrDefault() &&
                    item.tags.LastOrDefault() == ScenarioLoader.GetSelectedScenario().tags.LastOrDefault())
                        {
                            ScenarioLoader.SelectedScenario = item.prefabName;
                            break;
                        }
                    }
                    if (next) SpawnSubScenario();
                }
            }
            else if (sortingBasedOf == basedOf.SubScenarioList)
            {
                bool next = false;
                foreach (var item in SubScenarioList)
                {
                    if (!next)
                        if (pref == item.Value)
                        {
                            next = true;
                            continue;
                        }
                    if (next)
                    {
                        SpawnSubScenario(item.Key);
                        break;
                    }
                }
            }
        }

        public string nextSubName ()
        {
            string nextName = null;
            bool next = false;
            foreach (var item in ScenarioLoader.instance.scenarioData.data[ScenarioLoader.SelectedTopic].scenarios)
            {
                if (!next)
                    if (item.prefabName == ScenarioLoader.SelectedScenario)
                    {
                        next = true;
                        continue;
                    }
                if (next && 
                    item.tags.Count == ScenarioLoader.GetSelectedScenario().tags.Count &&
                    item.tags.FirstOrDefault() == ScenarioLoader.GetSelectedScenario().tags.FirstOrDefault() &&
                    item.tags.LastOrDefault() == ScenarioLoader.GetSelectedScenario().tags.LastOrDefault())
                {
                    nextName = item.title;
                    break;
                }
            }
            if (string.IsNullOrEmpty(nextName))
                return null;
            else return nextName;
        }
    }
}