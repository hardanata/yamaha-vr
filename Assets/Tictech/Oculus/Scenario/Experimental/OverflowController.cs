﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class OverflowController : MonoBehaviour
{
    [System.Serializable]
    public class savedClass
    {
        public GameObject _gameObject;
        public float savedValue;
    }
    public List<savedClass> savedValue;
    private HorizontalOrVerticalLayoutGroup layoutGroup;

    private void Start()
    {
        layoutGroup = GetComponent<HorizontalOrVerticalLayoutGroup>();
    }

    public void expandHeight()
    {
        if (layoutGroup)
            layoutGroup.childControlHeight = true;
    }

    public void revertExpandHeight(int index)
    {
        if (layoutGroup)
            layoutGroup.childControlHeight = false;
        RectTransform _rect = savedValue[index]._gameObject.GetComponent<RectTransform>() as RectTransform;
        _rect.sizeDelta = new Vector2(_rect.rect.width, savedValue[index].savedValue);
        //Debug.Log("current rect height : " + _rect.rect.height);
    }

    public void moveY (int index)
    {
        RectTransform _rect = savedValue[index]._gameObject.GetComponent<RectTransform>() as RectTransform;
        float newY = _rect.anchoredPosition.y + savedValue[index].savedValue;
        _rect.DOAnchorPos(new Vector2(_rect.anchoredPosition.x, newY), .5f, false);
    }

    public void moveX (int index)
    {
        RectTransform _rect = savedValue[index]._gameObject.GetComponent<RectTransform>() as RectTransform;
        float newX = _rect.anchoredPosition.x + savedValue[index].savedValue;
        _rect.DOAnchorPos(new Vector2(newX, _rect.anchoredPosition.x), .5f, false);
    }
}
