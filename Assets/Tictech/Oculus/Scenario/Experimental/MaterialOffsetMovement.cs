﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MaterialOffsetMovement : MonoBehaviour
{
    public Material material;
    public Vector2 offsetChange;
    public float changeTime;

    public string textureName = "_BaseColorMap";
    private float xValue, yValue;

    public bool isFlow;
    private float xFlow, yFlow;
    public float xFlowSpeed, yFlowSpeed;

    private void Start()
    {
        material = GetComponent<Renderer>().material;

        DOTween.To(() => xValue, x => xValue = x, offsetChange.x, changeTime).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InOutSine); ;
        DOTween.To(() => yValue, y => yValue = y, offsetChange.y, changeTime).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InOutSine); ;


    }
    private void Update()
    {
        if (isFlow)
        {
            xFlow += Time.deltaTime * xFlowSpeed;
            yFlow += Time.deltaTime * yFlowSpeed;
            material.SetTextureOffset(textureName, new Vector2(xValue + xFlow, yValue + yFlow));
        }
        else
            material.SetTextureOffset(textureName, new Vector2(xValue, yValue));
    }
}

