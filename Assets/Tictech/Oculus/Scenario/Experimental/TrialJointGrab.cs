﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using MEC;
using Tictech.Oculus.Scenario;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.PlayerLoop;
using UnityEngine.XR;

public class TrialJointGrab : MonoBehaviour
{
    public enum jointState
    {
        basic = 0,
        active = 1,
        nonActive = 2
    }

    public float triggerAngles = 15f;
    public jointState state;
    public List<OVRGrabbable> listGrabbable;
    public List<Collider> listCollider;
    public List<FollowHandler> listHandlers;
    public List<Joint> listJoint;
    public UnityEvent OnPumped;

    private jointState prevState;
    private bool trigger;

    // Start is called before the first frame update
    void Start()
    {
        trigger = false;
        prevState = state;
        setBasic();
        StartCoroutine("StateMachine");
    }
    void setBasic()
    {
        listGrabbable = GetComponentsInChildren<OVRGrabbable>(true).ToList();
        listCollider = GetComponentsInChildren<Collider>(true).ToList();
        listHandlers = GetComponentsInChildren<FollowHandler>(true).ToList();
        listJoint = GetComponentsInChildren<Joint>(true).ToList();
    }

    void FixedUpdate()
    {
        if (state == jointState.active)
        {
            int indexJoint = 0, indexTrigger = 0;
            foreach (var vJoint in listJoint)
            {
                if (vJoint.GetComponent<HingeJoint>() != null)
                {
                    indexJoint++;
                    HingeJoint hj = vJoint.GetComponent<HingeJoint>();
                    if (hj.motor.targetVelocity > hj.limits.min)
                    {
                        if (Mathf.Abs(hj.angle - hj.limits.min) < triggerAngles)
                        {
                            indexTrigger++;
                        }
                    }
                    else
                    {
                        if (Mathf.Abs(hj.angle - hj.limits.max) < triggerAngles)
                        {
                            indexTrigger++;
                        }
                    }
                }
            }
            if (indexTrigger >= indexJoint && trigger == false)
            {
                trigger = true;
                OnPumped.Invoke();
                Debug.Log("Trigger Joint");
            }
            else if (indexTrigger < indexJoint && trigger == true)
            {
                trigger = false;
            }
        }
    }

    IEnumerator StateMachine()
    {
        while (true)
        {
            //Debug.Log("prevstate - " + prevState + " | currentstate - " + state);
            if (prevState!=state)
            {
                switch (state)
                {
                    case jointState.basic :
                        setBasic();
                        break;
                    case jointState.active :
                        active();
                        break;
                    case jointState.nonActive :
                        nonActive();
                        break;
                }
                prevState = state;
                //Debug.Log("Current Joint State : " + state);
            }
            yield return new WaitForSeconds(1f);
        }

        
    }

    void active()
    {
        foreach (var vGrabbable in listGrabbable)
        {
            if (vGrabbable.gameObject.GetComponent<HandleGrabber>() != null)
            {
                vGrabbable.enabled = true;
            }
            else
            {
                vGrabbable.enabled = false;
            }
        }
    }

    void nonActive()
    {
        foreach (var vGrabbable in listGrabbable)
        {
            if (vGrabbable.gameObject.GetComponent<HandleGrabber>() != null)
            {
                vGrabbable.enabled = false;
            }
            else
            {
                vGrabbable.enabled = true;
            }
        }
    }

    public void ChangeState(int newState)
    {
        state = (jointState)newState;
    }
}
