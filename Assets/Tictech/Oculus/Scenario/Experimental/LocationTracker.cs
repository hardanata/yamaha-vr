﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocationTracker : MonoBehaviour
{
    [SerializeField] Transform camera;
    [SerializeField] Vector3 offset;
    [SerializeField] Transform tracker;
    [SerializeField] Transform trackerChild;
    public List<TeleportPoint> targets;
    private Transform cachedTransform;
    private Vector3 cachedPosition;
    public TeleportPoint currentTeleportPoint;


    public static LocationTracker instance;
    private void Awake()
    {
        instance = this;
    }

    void Start()
    {
        cachedTransform = GetComponent<Transform>();
        if (camera)
        {
            cachedPosition = camera.position;
        }

        tracker.gameObject.SetActive(false);
    }

    public void ClearTarget()
    {
        targets = new List<TeleportPoint>();
    }

    void Update()
    {
        if (camera && cachedPosition != camera.position)
        {
            cachedPosition = camera.position;
            transform.position = cachedPosition + offset;
        }

        //if (targets.Count > 0 && tracker != null)
        //{
        //    Vector3 relativePos = targets[0].transform.position - tracker.transform.position;
        //    Quaternion LookAtRotation = Quaternion.LookRotation(relativePos);

        //    Quaternion LookAtRotationYOnly = Quaternion.Euler(tracker.transform.rotation.eulerAngles.x, LookAtRotation.eulerAngles.y, tracker.transform.rotation.eulerAngles.z);


        //    tracker.transform.rotation = LookAtRotationYOnly;
        //}
        if (targets.Count > 0 && trackerChild != null)
        {
            Vector3 relativePos = targets[0].transform.position - trackerChild.transform.position;
            Quaternion LookAtRotation = Quaternion.LookRotation(relativePos);

            Quaternion LookAtRotationXnly = Quaternion.Euler(LookAtRotation.eulerAngles.x, LookAtRotation.eulerAngles.y, 0f);


            trackerChild.transform.rotation = LookAtRotationXnly;
            if(targets[0] == currentTeleportPoint) tracker.gameObject.SetActive(false);
            else tracker.gameObject.SetActive(true);

        }
    }

    public void SetTeleport(List<TeleportPoint> points)
    {
        targets = points;
        foreach (var point in targets)
        {
            point.onTeleportedTo.AddListener(delegate { NextListener(point); });
        }

        tracker.gameObject.SetActive(true);
    }
    public void SetTeleport(TeleportPoint point)
    {
        targets.Add(point);
        point.onTeleportedTo.AddListener(delegate { NextListener(point); });
        tracker.gameObject.SetActive(true);
    }

    public void RemoveTeleport(TeleportPoint point)
    {
        point.onTeleportedTo.RemoveListener(delegate { NextListener(point); });
    }


    void NextListener(TeleportPoint _point)
    {

        if (targets.Contains(_point))
        {
            foreach (var item in targets)
            {
                if (item != _point)
                {
                    item.onTeleportedTo.RemoveListener(delegate { NextListener(item); });
                    //targets.Remove(item);
                }
                else
                {
                    item.onTeleportedTo.RemoveListener(delegate { NextListener(item); });
                    //targets.Remove(item);
                    break;

                }
            }

            while (true)
            {
                if (targets[0] != _point)
                {
                    targets.RemoveAt(0);
                }
                else
                {
                    targets.RemoveAt(0);
                    if (targets.Count <= 0)
                    {
                        tracker.gameObject.SetActive(false);
                        return;
                    }
                    break;
                }
            }
        }
    }

}
