﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(SwipeControl))]
public class PageLayout : MonoBehaviour
{
    public Transform contentRef;
    public GameObject contentPrefab;
    public int contentPerPage = 7;
    public int currentPage = 1;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("refresh");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator refresh()
    {
        while (true)
        {
            yield return new WaitForEndOfFrame();
            reloadContent();
            yield return new WaitForSeconds(.5f);
        }
    }

    public void reloadContent()
    {
        if (contentRef.GetComponent<SwipeControl>() && GetComponent<SwipeControl>())
            if (contentRef.GetComponent<SwipeControl>().getFirstHierarchyChild(contentRef.gameObject).Count != GetComponent<SwipeControl>().getFirstHierarchyChild(gameObject, true).Count)
            {
                SwipeControl swipe = GetComponent<SwipeControl>();
                if(swipe.firstHierarchyChild.Count > 0)
                foreach (var item in swipe.getFirstHierarchyChild(gameObject, true))
                {
                    Destroy(item.gameObject);
                }
                swipe.firstHierarchyChild.Clear();
                selectiveActivation();
                if (audioButton.Instance) audioButton.Instance.refreshAudioButton();
            }
    }

    void selectiveActivation()
    {
        int length = contentRef.GetComponent<SwipeControl>().getFirstHierarchyChild(contentRef.gameObject).Count;

        for (int i = 0; i < length; i++)
        {
            Transform item = contentRef.GetComponent<SwipeControl>().getFirstHierarchyChild(contentRef.gameObject)[i];
            GameObject instance;
            if (contentPrefab)
                instance = Instantiate(contentPrefab, transform);
            else
                instance = new GameObject(item.name, typeof(TextMeshProUGUI));
            instance.transform.SetParent(transform);
            if (i < currentPage * 7 && (i >= (currentPage - 1) * 7 || currentPage - 1 == 0))
                instance.gameObject.SetActive(true);
            else instance.gameObject.SetActive(false);
            if (instance.GetComponentInChildren<TextMeshProUGUI>(true))
                instance.GetComponentInChildren<TextMeshProUGUI>(true).text = item.gameObject.name;
            if (instance.GetComponentInChildren<Button>(true))
                instance.GetComponentInChildren<Button>(true).onClick.AddListener(() => { contentRef.GetComponentInChildren<SwipeControl>().selectIndex(instance.transform.GetSiblingIndex()); });
        }
    }

    public void AddPage(int i)
    {
        currentPage = (int) Mathf.Clamp(currentPage + i, 1, Mathf.Ceil(GetComponent<SwipeControl>().getFirstHierarchyChild(gameObject, true).Count/7));
        selectiveActivation();
        LayoutRebuilder.ForceRebuildLayoutImmediate(GetComponent<RectTransform>());
    }
}
