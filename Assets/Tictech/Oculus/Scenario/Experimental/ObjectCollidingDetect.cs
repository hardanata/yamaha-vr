﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Tictech.Oculus.Scenario;

public class ObjectCollidingDetect : MonoBehaviour
{
    public GameObject TriggerObject;
    public string ObjectName;
    public string ObjectTag;
    public UnityEvent OnTouch;
    private AudioSource audio;
    public string collidingAudio;
    
    void Start()
    {
        if(!string.IsNullOrEmpty(collidingAudio))
            if (GetComponent<AudioSource>())
                audio = GetComponent<AudioSource>();
            else audio = gameObject.AddComponent<AudioSource>();
    }

    void OnTriggerEnter(Collider col)
    {
        if (ObjectName == null)
        {
            if (col == TriggerObject)
            {
                OnTouch?.Invoke();
                playAudio();
            }
        }
        else
        {
            if (col.gameObject.name == ObjectName || col.gameObject.tag == ObjectTag)
            {
                OnTouch?.Invoke();
                playAudio();
            }
        }
    }

    void playAudio()
    {
        if (audio)
            if (ScenarioManager.Instance.audioReference.clips[collidingAudio])
                audio.PlayOneShot(ScenarioManager.Instance.audioReference.clips[collidingAudio]);
    }
}
