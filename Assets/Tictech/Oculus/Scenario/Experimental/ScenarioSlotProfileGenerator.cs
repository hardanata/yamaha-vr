﻿using Sirenix.Utilities;
using System.Collections;
using System.Collections.Generic;
using Tictech.Oculus.Scenario;
using UnityEditor;
using UnityEngine;
using System.IO;

public class ScenarioSlotProfileGenerator : MonoBehaviour
{
#if UNITY_EDITOR
    public enum MachineName
    {
        CUSTOM,
        BucketElevator,
        ConveyorBelt,
        Cyclone,
        Furnace,
        MaterialCrusher,
        RotaryDryer
    }


    //public string path;
    [HideInInspector] public MachineName machineName;
    [HideInInspector] public string customMachineName;
    [HideInInspector] public string fileName;
    [HideInInspector] public string slotId;
    private int duplicateCount;
    [HideInInspector] public string _path;
    [HideInInspector] public bool isMultipleMesh;

    public void GenerateButton()
    {
        if (!CheckFolder())
            return;

        string path = GetClickedDirFullPath();

        if (string.IsNullOrEmpty(fileName) || string.IsNullOrEmpty(slotId))
        {
            if (EditorUtility.DisplayDialog("Fil Name or Slot ID is Empty", "File Name dan Slot Id nya tolong diisi guna menghindari kesalahan ya.", "Oke Bos"))
            {

            }
        }
        else if (!isMultipleMesh && !GetComponent<MeshFilter>())
        {
            if (EditorUtility.DisplayDialog("No Mesh Found", "Aduh aduh, ini teh script dipake di gameobject yang ada mesh-nya, coba lagi ya.", "Oke Bos"))
            {

            }
        }
        else
        {
            if (EditorUtility.DisplayDialog("Create Slot Profile?", "Slot Profile akan di generate di " + path + ". ", "Gaskeun", "Cancel gan"))
            {
                Generate();
            }
        }
        Debug.Log(path);
    }

    public void Generate()
    {
        ScenarioSlotProfile asset = ScriptableObject.CreateInstance<ScenarioSlotProfile>();

        string tempName = "";
        if (machineName == MachineName.CUSTOM)
            tempName = customMachineName + "_" + fileName;
        else
            tempName = machineName.ToString() + "_" + fileName;

        string path = GetClickedDirFullPath();

        if (duplicateCount <= 0)
            AssetDatabase.CreateAsset(asset, path + "/" + tempName + ".asset");
        else
            AssetDatabase.CreateAsset(asset, path + "/" + tempName + " (" + duplicateCount + ").asset");

        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;

        asset.slotId = slotId;
        if (!isMultipleMesh)
        {
            asset.mesh = GetComponent<MeshFilter>().sharedMesh;
            asset.meshScale = transform.lossyScale.x;
            asset.meshScaleV3 = transform.lossyScale;
        }
        else
        {
            List<Mesh> _meshList = new List<Mesh>();
            List<Vector3> _meshScaleList = new List<Vector3>();
            List<Vector3> _meshPosList = new List<Vector3>();
            List<Quaternion> _meshRotList = new List<Quaternion>();

            foreach (var item in GetComponentsInChildren<MeshFilter>())
            {
                _meshList.Add(item.sharedMesh);
                _meshScaleList.Add(item.transform.lossyScale);

                if (item.transform.parent == transform || item.transform == transform)
                {
                    _meshPosList.Add(item.transform.localPosition);
                    _meshRotList.Add(item.transform.localRotation);
                }
                else
                {
                    GameObject tempGO = new GameObject();
                    tempGO.transform.parent = item.transform;
                    tempGO.transform.localPosition = Vector3.zero;
                    tempGO.transform.localRotation = Quaternion.Euler(Vector3.zero);
                    tempGO.transform.parent = transform;
                    _meshPosList.Add(tempGO.transform.localPosition);
                    _meshRotList.Add(tempGO.transform.localRotation);
                    DestroyImmediate(tempGO.gameObject);
                }
            }

            asset.isMultipleMesh = true;
            asset.meshList = _meshList.ToArray();
            asset.meshScaleList = _meshScaleList.ToArray();
            asset.meshPosList = _meshPosList.ToArray();
            asset.meshRotList = _meshRotList.ToArray();
            asset.meshParentScale = transform.lossyScale;
            if (GetComponent<MeshFilter>())
                asset.parentHasMesh = true;
        }
        EditorUtility.SetDirty(asset);
        Debug.Log(path);
    }

    public bool CheckFolder()
    {
        List<ScenarioSlotProfile> profilesList = new List<ScenarioSlotProfile>();

        string tempName = "";
        if (machineName == MachineName.CUSTOM)
            tempName = customMachineName + "_" + fileName;
        else
            tempName = machineName.ToString() + "_" + fileName;

        string path = GetClickedDirFullPath();


        duplicateCount = 0;
        string[] assetNames = AssetDatabase.FindAssets("t:ScenarioSlotProfile", new[] { path });
        profilesList.Clear();
        foreach (string SOName in assetNames)
        {
            var SOpath = AssetDatabase.GUIDToAssetPath(SOName);
            var profile = AssetDatabase.LoadAssetAtPath<ScenarioSlotProfile>(SOpath);
            profilesList.Add(profile);

            if (profile.name == tempName)
                duplicateCount++;
            else if (profile.name == tempName + " (" + duplicateCount + ")")
                duplicateCount++;
            else if (profile.name == tempName + " (" + (duplicateCount + 1) + ")")
                duplicateCount++;
        }
        Debug.Log(duplicateCount);

        var tempDupCount = duplicateCount;
        //bool check = true;

        foreach (var item in profilesList)
        {
            if (!isMultipleMesh)
                if (item.mesh == GetComponent<MeshFilter>().sharedMesh)
                {
                    if (EditorUtility.DisplayDialog("Mesh Already Exist", "Slot Profile dengan mesh ini sudah pernah dibuat, coba cek '" + item.name + "', tetap buat slot profil? ", "Buat Duplikat", "Cancel gan"))
                    {
                        break;
                    }
                    else
                    {
                        return false;
                    }
                }
        }

        foreach (var item in profilesList)
        {
            if (item.slotId == slotId)
            {
                if (EditorUtility.DisplayDialog("Slot Id Already Exist", "Slot Profile dengan slot id ini sudah pernah dibuat, coba cek '" + item.name + "', tetap buat slot profil? ", "Buat Duplikat", "Cancel gan"))
                {
                    break;
                }
                else
                {
                    return false;
                }
            }
        }
        foreach (var item in profilesList)
        {
            //Debug.Log(item.name + " " + tempName + " (" + (duplicateCount - 1) + ")");
            if ((item.name == tempName) || (item.name == tempName + " (" + duplicateCount + ")") || (item.name == tempName + " (" + (duplicateCount - 1) + ")"))
            {
                if (EditorUtility.DisplayDialog("File Name Already Exist", "Slot Profile dengan nama ini sudah pernah dibuat, coba cek '" + item.name + "', tetap buat slot profil? ", "Buat Duplikat", "Cancel gan"))
                {
                    break;
                }
                else
                {
                    return false;
                }
            }
        }

        return true;
    }
    private string GetClickedDirFullPath()
    {

        //string clickedAssetGuid = Selection.assetGUIDs[0];
        //string clickedPath = AssetDatabase.GUIDToAssetPath(clickedAssetGuid);
        //string clickedPathFull = Path.Combine(Directory.GetCurrentDirectory(), clickedPath);

        //FileAttributes attr = File.GetAttributes(clickedPathFull);

        //var temp = attr.HasFlag(FileAttributes.Directory) ? clickedPathFull : Path.GetDirectoryName(clickedPathFull);
        Debug.Log(_path);
        var relativePath = "Assets" + _path.Substring(Application.dataPath.Length);
        Debug.Log(relativePath);
        return relativePath;

    }
#endif
}