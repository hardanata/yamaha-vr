﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Tictech.Oculus.Scenario;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ToolTip : MonoBehaviour
{
    public Transform rotationPoint;
    public string lookTarget;
    public GameObject target1, target2;
    private LineRenderer _line;
    private Transform _targetLook;
    
    // Start is called before the first frame update
    void Start()
    {
        _targetLook = GameObject.Find(lookTarget).transform;
        _line = GetComponentInChildren<LineRenderer>();
    }

    void FixedUpdate()
    {
        Vector3 vec = GetComponentInChildren<Canvas>().GetComponent<RectTransform>().anchoredPosition3D;
        vec = Vector3.zero;
        _line.SetPosition(0, target1.transform.position);
        _line.SetPosition(1, target2.transform.position);
        rotationPoint.LookAt(_targetLook);
    }
}
