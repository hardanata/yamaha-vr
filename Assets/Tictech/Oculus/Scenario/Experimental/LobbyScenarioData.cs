﻿using System.Collections;
using System.Collections.Generic;
using Arna.AssetManagement;
using Sirenix.OdinInspector;
using Tictech.Oculus.Scenario;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

[CreateAssetMenu(fileName = "New LobbyScenarioData", menuName = "Petrokimia/LobbyScenarioData", order = 0)]
public class LobbyScenarioData : ScriptableObject
{
    public List<ScenarioTopic> data;

    #if UNITY_EDITOR
    [Header("Editor Only")] 
    [SerializeField]
    private string importFromPath;
    [Button]
    public void ImportData()
    {
        if(data == null)
            data = new List<ScenarioTopic>();
        Debug.Log("Importing scenario topic...");
        string[] guids = AssetDatabase.FindAssets("l:ScenarioTopic", new string[] { importFromPath });
        Debug.Log("Found " + guids.Length + " data...");
        foreach (string g in guids)
        {
            var topic = AssetDatabase.LoadAssetAtPath<ScenarioTopic>(AssetDatabase.GUIDToAssetPath(g));
            if(data.Contains(topic))
                continue;
            data.Add(topic);
            Debug.Log("Added data: " + topic.title);
        }

        EditorUtility.SetDirty(this);
    }
    #endif
}
