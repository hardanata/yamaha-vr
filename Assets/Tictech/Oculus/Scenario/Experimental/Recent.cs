﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Recent : MonoBehaviour
{
    public List<Text> textUI;
    public LobbyMenu2 lobby;
    public Sprite bgSprite;
    
    // Start is called before the first frame update
    void Start()
    {
        LobbyMenu2 newLobby;
        if (!GetComponent<LobbyMenu2>())
        {
            newLobby = gameObject.AddComponent<LobbyMenu2>();
            newLobby.scenarioData = lobby.scenarioData;
            newLobby.scenarioDetailPanel = lobby.scenarioDetailPanel;
            newLobby.scenarioTypeText = lobby.scenarioTypeText;
            newLobby.scenarioTitleText = lobby.scenarioTitleText;
            newLobby.scenarioDetailText = lobby.scenarioDetailText;
            newLobby.scenarioPreviewImage = lobby.scenarioPreviewImage;
        }
        else newLobby = GetComponent<LobbyMenu2>();
        List<string> sText = new List<string>(3);
        sText.AddRange(PlayerPrefsX.GetStringArray("Recent", "None", 3));
        List<string> sceneName = new List<string>(3);
        sceneName.AddRange(PlayerPrefsX.GetStringArray("RecentScene", "None", 3));
        for (int i = 0; i < 3; i++)
        {
            textUI[i].text = sText[i];
            List<Image> im = new List<Image>();
            im.Clear();
            im.AddRange(textUI[i].transform.GetComponentInParent<Button>().GetComponentsInChildren<Image>());
            for (int j = 0; j < im.Count; j++)
            {
                foreach (var item in newLobby.scenarioData.data)
                {
                    foreach (var item2 in item.scenarios)
                    {
                        //Debug.LogError("check : stext " + sText[i] + " |item " + item + " |item2 " + item2);
                        if (sText[i] == item2.title && sceneName[i] == item2.prefabName)
                        {
                            if (GetComponentInParent<OVRCustomRaycaster>())
                            {
                                if (j != 0) im[j].sprite = item2.preview2;
                                else im[j].sprite = bgSprite;
                            }
                            else im[j].sprite = item2.preview2;
                            im[j].transform.GetComponent<Button>().onClick.AddListener(() => newLobby.LoadScenario(item.scenarios[item.scenarios.IndexOf(item2)]));
                        }
                    }
                }
            }
        }
    }
}
