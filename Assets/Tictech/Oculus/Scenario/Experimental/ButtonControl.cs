﻿using System.Collections;
using System.Collections.Generic;
using MEC;
using UnityEngine;
using  UnityEngine.UI;

public class ButtonControl : MonoBehaviour
{
    public float zAxis { get; set; }
    public bool interactable = false;
    public float timer = 1f;
    public float bgResize = 1f;
    public bool invoked = false;
    Vector3 childScale, scale;
    public OVRCustomRaycaster ocr;
    public Sprite usedSprite, changeSprite;
    private Sprite sprite;

    public class audioClone : MonoBehaviour
    {
        private Vector3 saveScale;
        public AudioSource audioSource;
        void Start()
        {
            StartCoroutine("update");
        }

        IEnumerator update()
        {
            while (!audioSource)
            {
                yield return Timing.WaitForOneFrame;
            }
            audioSource.Play();
            /*while (true)
            {
                if (audioSource)
                {
                    Debug.Log("button audio play : " + audioSource.isPlaying);
                    if (!audioSource.isPlaying)
                    {
                        Destroy(audioSource.gameObject);
                    }
                }
                yield return Timing.WaitForSeconds(3f);
            }*/
            while (audioSource.isPlaying)
            {
                yield return Timing.WaitForSeconds(3f);
            }
            Destroy(this.gameObject);
        }
    }

    void Start()
    {
        if (usedSprite) sprite = usedSprite;
        invoked = false;
        timer = 1f;
        interactable = false;
        
        childScale = transform.TransformVector(new Vector3(transform.localScale.x, transform.localScale.y, transform.localScale.z));
        if (transform.parent)
            scale = transform.parent.localScale;
        transform.localPosition = new Vector3(0, 0, zAxis);
        Delay();
    }
    // Start is called before the first frame update
    void Awake()
    {
        
    }

    IEnumerator Delay()
    {
        GetComponent<BoxCollider>().enabled = false;
        yield return Timing.WaitForSeconds(1f);
        transform.localPosition = new Vector3(0, 0, zAxis);
        GetComponent<BoxCollider>().enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (!interactable)
        {
            if (transform.localPosition == new Vector3(0, 0, zAxis) && !invoked) return;
            timer -= Time.deltaTime;
            //Debug.Log("timer : " + timer);
            transform.localPosition =
                Vector3.Lerp(transform.localPosition, new Vector3(0, 0, zAxis), (Time.deltaTime * 3));
            if (timer > 0f) return;
            transform.localPosition = new Vector3(0, 0, zAxis);
            invoked = false;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag != "Yubi") return;
        //GetComponent<Rigidbody>().isKinematic = false;
        timer = 0.2f;
        interactable = true;
        if (transform.parent)
            if (transform.parent.GetComponent<Button>())
            {
                GetComponent<Button>().onClick = transform.parent.GetComponent<Button>().onClick;
            }
    }

    void OnTriggerStay(Collider other)
    {
        //if (transform.localPosition.x != 0 && transform.localPosition.y != 0)
        if (other.tag == "Yubi")
            transform.localPosition = new Vector3(0, 0,
                Mathf.Clamp(transform.InverseTransformPoint(other.transform.position).z, zAxis, 0));
        if (transform.localPosition.z < -0.01f) return;
        //GetComponent<Rigidbody>().isKinematic = true;
        //pressAble = false;
        if (transform.parent)
            if (transform.parent.GetComponent<Button>().enabled)
                if (!invoked)
                {
                    transform.parent.GetComponent<Button>().onClick.Invoke();
                    triggerAudio();
                    if (gameObject.activeInHierarchy)
                        StartCoroutine("fadeOut");
                }
        invoked = true;
    }

    public void triggerAudio()
    {
        GameObject go = new GameObject("Audio Clone", typeof(audioClone), typeof(AudioSource));
        go.transform.position = transform.position;
        go.GetComponent<audioClone>().audioSource = go.GetComponent<AudioSource>();
        go.GetComponent<audioClone>().audioSource.clip = ocr.buttonSound;
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag != "Yubi") return;
        //GetComponent<Rigidbody>().isKinematic = true;
        interactable = false;
    }

    IEnumerator fadeOut()
    {
        float f = 0.1f, fTime = f;
        Image im = transform.parent.GetComponentInChildren<Image>();
        if(im.sprite != changeSprite) usedSprite = im.sprite;
        
        if(changeSprite) im.sprite = changeSprite;
        while (fTime > 0)
        {
            transform.parent.localScale = (Time.deltaTime * scale * bgResize) + transform.parent.localScale;
            transform.localScale = Vector3.one / transform.parent.localScale.x;
            im.CrossFadeAlpha(0.7f, f, false);
            fTime -= Time.deltaTime;
            yield return Timing.WaitForOneFrame;
        }
        im.CrossFadeAlpha(1f, 0.1f, false);
        transform.localScale = Vector3.one / transform.parent.localScale.x;
        while (fTime < f)
        {
            im.CrossFadeAlpha(1f, f, false);
            transform.parent.localScale = transform.parent.localScale - (Time.deltaTime * scale * bgResize);
            transform.localScale = Vector3.one / transform.parent.localScale.x;
            fTime += Time.deltaTime;
            yield return Timing.WaitForOneFrame;
        }
        im.sprite = usedSprite;
        //GameObject g = Instantiate(gameObject, null);
        transform.parent.localScale = scale;
        transform.localScale = Vector3.one / transform.parent.localScale.x;
        //transform.localScale = childScale;
        gameObject.SetActive(true);
    }
}
