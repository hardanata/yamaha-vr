﻿using System.Collections;
using System.Collections.Generic;
using Tictech.Oculus.Scenario;
using UnityEngine;
using UnityEngine.UI;

public class StepIndicator : MonoBehaviour
{
    GameObject Instance;
    [System.Serializable]
    public class SpriteIndicator
    {
        public Sprite startSprite, midSprite, endSprite;
    }

    public SpriteIndicator spriteIndicator;
    private int current = 0;
    // Start is called before the first frame update
    void Start()
    {
        current = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.childCount != ScenarioManager.Instance.stepsInfo.Length)
        {
            for (int i = 0; i < ScenarioManager.Instance.stepsInfo.Length; i++)
            {
                Instance = new GameObject("progress bar " + i, typeof(Image));
                Instance.transform.parent = transform;
                Instance.transform.localScale = Vector3.one;
                Instance.transform.localPosition = Vector3.zero;
                Instance.transform.localRotation = Quaternion.identity;
                Vector2 size = transform.GetComponent<RectTransform>().sizeDelta;
                float spacing = 0;
                if (GetComponent<HorizontalLayoutGroup>())
                    spacing = transform.GetComponent<HorizontalLayoutGroup>().spacing;
                Instance.GetComponent<RectTransform>().sizeDelta = new Vector2((size.x / ScenarioManager.Instance.stepsInfo.Length) - spacing, size.y);
                Image im = Instance.GetComponent<Image>();
                if(ScenarioManager.Instance.stepsInfo.Length == 1)
                {
                    im.sprite = spriteIndicator.midSprite;
                    im.color = new Color32(0, 150, 255, 255);
                }
                else if (i == 0) {
                    im.sprite = spriteIndicator.startSprite;
                    im.color = new Color32(0, 150, 255, 255);
                }
                else if (i == ScenarioManager.Instance.stepsInfo.Length - 1) im.sprite = spriteIndicator.endSprite;
                else im.sprite = spriteIndicator.midSprite;
                //im.preserveAspect = true;
            }
        }
        if (current < ScenarioManager.currentStep)
        {
            for (int i = 0; i < ScenarioManager.currentStep + 1; i++)
            {
                transform.GetChild(Mathf.Clamp(i, 0, ScenarioManager.Instance.stepsInfo.Length - 1)).GetComponent<Image>().color = new Color32(0, 150, 255, 255);
            }
        }
    }
}
