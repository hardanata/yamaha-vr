﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptCamera : MonoBehaviour
{
    private Camera cam;
    // Start is called before the first frame update
    void Start()
    {
        if (this.GetComponent<Camera>())
        {
            cam = this.GetComponent<Camera>();
            cam.stereoTargetEye = StereoTargetEyeMask.None;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
