﻿using MEC;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Tictech.Oculus.Scenario;
using UnityEngine;
using UnityEngine.UI;

public class audioButton : MonoBehaviour
{
    public static audioButton Instance;
    public AudioClip buttonSound;
    public string AudioInResources = "Button Press(CC BY 3.0)";
    public List<Button> buttonList;
    // Start is called before the first frame update
    void Start()
    {
        Instance = this;
        refreshAudioButton();
    }

    public void refreshAudioButton()
    {
        StartCoroutine("refresh");
    }

    IEnumerator refresh()
    {
        yield return new WaitForEndOfFrame();
        if (!buttonSound)
        {
            if (ScenarioManager.Instance.audioReference.GetClip("Button"))
                buttonSound = ScenarioManager.Instance.audioReference.GetClip("Button");
            else
                buttonSound = Resources.Load<AudioClip>("Audio/Ambience/" + AudioInResources); ;
        }
        buttonList.Clear();
        buttonList = GetComponentsInChildren<Button>(true).ToList();
        foreach (var item in buttonList)
        {
            item.onClick.RemoveListener(() => { audioPlay(item.transform); });
            item.onClick.AddListener(() => { audioPlay(item.transform); });
        }
    }

    public void audioPlay(Transform _transform)
    {
        if (buttonSound)
        {
            GameObject audioSource = new GameObject("AudioPlayerWithAutoDestroy", typeof(AudioSource), typeof(destroyer));
            //audioSource.transform.parent = _transform;
            audioSource.transform.position = _transform.position;
            audioSource.GetComponentInChildren<AudioSource>(true).clip = buttonSound;
        }
    }

    public class destroyer : MonoBehaviour
    {
        private void Start()
        {
            if (audioButton.Instance)
                if (!GetComponent<AudioSource>().clip) GetComponent<AudioSource>().clip = audioButton.Instance.buttonSound;
            GetComponent<AudioSource>().PlayOneShot(GetComponent<AudioSource>().clip);
        }

        private void Update()
        {
            if (!GetComponent<AudioSource>().isPlaying) Destroy(gameObject);
        }
    }
}
