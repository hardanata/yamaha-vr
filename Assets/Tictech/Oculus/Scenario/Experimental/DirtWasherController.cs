﻿using MEC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Events;
namespace Tictech.Oculus.Scenario
{
    public class DirtWasherController : MonoBehaviour
    {
        [SerializeField] GameObject DirtUnwashed;
        [SerializeField] GameObject DirtWashed;
        [SerializeField] string toolId;
        [SerializeField] float washSpeed;
        [SerializeField] Vector3 targetScale;
        float scaleX, scaleY, scaleZ;
        [SerializeField] Transform indicator;
        [SerializeField] UnityEvent onWashed;
        Transform marker;
        bool isWashed = false;



        private void Start()
        {
            if (DirtUnwashed == null)
                DirtUnwashed = this.gameObject;

            if (indicator != null)
            {
                marker = Instantiate(ScenarioManager.Instance.toolMarkerPrefab).transform;
                marker.name = gameObject.name + " marker";
                marker.SetParent(indicator, true);
                marker.localPosition = Vector3.zero;
                marker.SetParent(null, true);
            }

            scaleX = DirtUnwashed.transform.localScale.x;
            scaleY = DirtUnwashed.transform.localScale.y;
            scaleZ = DirtUnwashed.transform.localScale.z;
        }

        private void OnTriggerStay(Collider other)
        {
            if (other.GetComponent<ScenarioTool>() != null)
            {
                if (!isWashed)
                {
                    var tool = other.GetComponent<ScenarioTool>();
                    if (tool.profile.toolName == toolId && other.gameObject.tag == "Scenario Tool")
                    {

                        scaleX -= Time.deltaTime * washSpeed;
                        scaleY -= Time.deltaTime * washSpeed;
                        scaleZ -= Time.deltaTime * washSpeed;

                        DirtUnwashed.transform.localScale = new Vector3(scaleX, scaleY, scaleZ);

                        if (DirtUnwashed.transform.localScale.x <= targetScale.x && DirtUnwashed.transform.localScale.y <= targetScale.y && DirtUnwashed.transform.localScale.z <= targetScale.z)
                        {
                            onWashed.Invoke();
                            if (marker != null)
                                marker.gameObject.SetActive(false);
                            if (DirtWashed != null)
                                DirtWashed.SetActive(true);
                            DirtUnwashed.SetActive(false);
                        }
                    }
                }
            }
        }
    }
}
