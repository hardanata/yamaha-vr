﻿using UnityEngine;
using UnityEngine.UI;

public class ButtonCustomRaycasterException : MonoBehaviour
{
    private ButtonControl buttonControl;
    public Sprite usedSprite, changeSprite;
    private bool changed = false;
    // Start is called before the first frame update
    void Start()
    {
        if (transform.GetComponent<ButtonControl>())
        {
            buttonControl = transform.GetComponent<ButtonControl>();
            changingSprite();
            changed = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (buttonControl != null)
        {
            if (changed == false)
                changingSprite();
            else
                Destroy(this);
        }
    }
    private void changingSprite ()
    {
        if (usedSprite)
        {
            buttonControl.usedSprite = usedSprite;
            if (transform.parent.GetComponent<Image>())
                transform.parent.GetComponent<Image>().sprite = usedSprite;
            changed = true;
        }
        if (changeSprite)
        {
            buttonControl.changeSprite = changeSprite;
            changed = true;
        }
    }
}
