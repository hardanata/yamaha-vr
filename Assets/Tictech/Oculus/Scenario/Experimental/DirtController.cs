﻿using MEC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
namespace Tictech.Oculus.Scenario
{
    public class DirtController : ScenarioItem
    {

        public string toolId;
        Vector3 baseScale;
        [SerializeField] float maxHealth;
        [SerializeField] float health;
        [SerializeField] Transform targetMesh;
        [SerializeField] float animationTime;
        bool isScaling;
        public class destroyOnEndPlay:MonoBehaviour
        {
            AudioSource audio;
            void Start ()
            {
                audio = transform.parent.gameObject.AddComponent<AudioSource>();
                gameObject.GetComponent<AudioSource>().PlayOneShot(ScenarioManager.Instance.audioReference.GetClip("Dirt"));
            }

            private void Update()
            {
                if (!audio.isPlaying) Destroy(gameObject);
            }
        }

        private void Start()
        {

            health = maxHealth;
            if (targetMesh == null)
                targetMesh = transform;

            baseScale = targetMesh.localScale;
        }

        public void decreaseHealth(int damage)
        {
            if (isScaling)
                return;

            health -= damage;
            StartCoroutine(scaled());
        }

        IEnumerator scaled()
        {
            isScaling = true;
            if (health <= 0)
            {
                Timing.RunCoroutine(CompleteTool(GetToolPoint(toolId), 0f));
                gameObject.SetActive(false);
                GameObject Instance = new GameObject("Dirt Audio", typeof(destroyOnEndPlay) );
                Instance.transform.position = transform.position;
                yield break;
            }

            var _health = health / maxHealth;
            targetMesh.DOScale(new Vector3(baseScale.x, baseScale.y, baseScale.z * _health), animationTime);
            yield return new WaitForSeconds(animationTime);
            isScaling = false;
        }
    }
}
