﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.UI;
using Tictech.Oculus.Scenario;
using MEC;

public class OVRCustomRaycaster : MonoBehaviour
{
    public float colliderResize = 200;
    public float buttonDepth = -30;
    public float bgResize = 1f;
    [Header("Base Sprite Changer")]
    public Sprite baseSprite;
    public Sprite onClickSprite;
    public Sprite baseSpriteHorizontal;
    public Sprite onClickSpriteHorizontal;
    public Sprite baseSpriteVertical;
    public Sprite onClickSpriteVertical;

    public string instaceLayer = "Water";

    public AudioClip buttonSound;
    public string AudioInResources = "Button Press(CC BY 3.0)";

    public List<Button> buttonList;
    public bool forceActive = false;

    // Start is called before the first frame update

    public void ReInit() 
    {
        var temp = new List<Button>();
        temp.AddRange(buttonList);
        buttonList.Clear();
        foreach (var item in temp)
        {
            if (item)
            {
                buttonList.Add(item);
            }
        }
        foreach (var item in GetComponentsInChildren<Button>(true).ToList())
        {
            if (item.interactable == true && !item.GetComponent<ButtonControl>() && !item.GetComponentInChildren<ButtonControl>())
                if (!buttonList.Contains(item) && !buttonList.Contains(item.transform.parent.GetComponent<Button>()))
                {
                    buttonList.Add(item);
                    createButton(item);
                }
        }
    }

    void createButton(Button item)
    {
        if (!this.isActiveAndEnabled) return;
        GameObject Instance = Instantiate(item.gameObject, item.transform.position, item.transform.rotation);
        Instance.transform.SetParent(item.transform);
        Instance.transform.localScale = Vector3.one;
        Instance.SetActive(true);
        Vector3 pos = Instance.GetComponent<RectTransform>().anchoredPosition3D;
        Instance.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(pos.x, pos.y, buttonDepth);
        Instance.layer = LayerMask.NameToLayer(instaceLayer);
        BoxCollider col = Instance.AddComponent<BoxCollider>();
        //Rigidbody rb = Instance.AddComponent<Rigidbody>();
        ButtonControl bc = Instance.AddComponent<ButtonControl>();
        bc.ocr = this;
        bc.bgResize = bgResize;
        if (item.GetComponent<Image>() != null)
            if (item.GetComponent<Image>().sprite != null)
            {
                col.size = item.GetComponent<Image>().sprite.bounds.extents * colliderResize;
                if (col.size == Vector3.one || col.size == Vector3.zero)
                    col.size = new Vector3(
                        col.GetComponent<RectTransform>().rect.width, 
                        col.GetComponent<Rect>().height, 
                        col.GetComponent<Rect>().height * 0.5f);
                List<Transform> allChildren = new List<Transform>();
                allChildren = item.GetComponentsInChildren<Transform>(true).ToList();
                foreach (Transform child in allChildren)
                {
                    //if(item.transform != child || Instance.transform != child)
                    if (!child.GetComponent<Button>() && !child.GetComponent<Scrollbar>()) child.gameObject.SetActive(false);
                }
                if (forceActive) item.gameObject.SetActive(true);
                allChildren = Instance.GetComponentsInChildren<Transform>(true).ToList();
                foreach (Transform child in allChildren)
                {
                    //if(item.transform != child || Instance.transform != child)
                    if (!child.GetComponent<Button>() && !child.GetComponent<Scrollbar>()) child.gameObject.SetActive(true);
                }
            }
        col.isTrigger = true;
        //if(!rb) rb = Instance.GetComponent<Rigidbody>();
        if(!bc) bc = Instance.AddComponent<ButtonControl>();
        //rb.isKinematic = true;
        //rb.useGravity = false;
        //rb.freezeRotation = true;
        bc.zAxis = buttonDepth;
        RectTransform rect = item.GetComponent<RectTransform>();
        if (item.GetComponent<Image>())
        {
            if (baseSpriteHorizontal && rect.rect.width >= rect.rect.height * 1.2)
            {
                item.GetComponent<Image>().sprite = baseSpriteHorizontal;
                if (onClickSpriteHorizontal) Instance.GetComponent<ButtonControl>().changeSprite = onClickSpriteHorizontal;
                else Instance.GetComponent<ButtonControl>().changeSprite = baseSpriteHorizontal;
            }
            else if (baseSpriteVertical && rect.rect.height >= rect.rect.width * 1.2)
            {
                item.GetComponent<Image>().sprite = baseSpriteVertical;
                if (onClickSpriteVertical) Instance.GetComponent<ButtonControl>().changeSprite = onClickSpriteVertical;
                else Instance.GetComponent<ButtonControl>().changeSprite = baseSpriteVertical;
            }
            else if (baseSprite)
            {
                item.GetComponent<Image>().sprite = baseSprite;
                if (onClickSprite) Instance.GetComponent<ButtonControl>().changeSprite = onClickSprite;
                else Instance.GetComponent<ButtonControl>().changeSprite = baseSprite;
            }
        }
    }

    void Init()
    {
        if (!buttonSound)
        {
            if (ScenarioManager.Instance.audioReference.GetClip("Button"))
                buttonSound = ScenarioManager.Instance.audioReference.GetClip("Button");
            else
                buttonSound = Resources.Load<AudioClip>("Audio/Ambience/" + AudioInResources); ;
        }
        buttonList.Clear();
        buttonList = GetComponentsInChildren<Button>(true).ToList();
        foreach (var item in buttonList)
        {
            if (item.interactable == true && !item.GetComponent<ButtonControl>() && !item.GetComponentInChildren<ButtonControl>())
                createButton(item);
        }
    }

    public void cleanUp()
    {
        foreach (var item in buttonList)
        {
            if (!item)
            {
                buttonList.Remove(item);
                //Destroy(item.gameObject);
            }
            else if (item.transform.parent.GetComponent<Button>() && item.transform.parent.GetComponentInChildren<ButtonControl>(true).gameObject != item.gameObject)
            {
                Destroy(item.gameObject);
                buttonList.Remove(item);
            }
        }
    }

    void Start()
    {
        
    }

    void Awake()
    {
        //Debug.LogError("Start with button list : " + buttonList.Count);
        Init();
        StartCoroutine("restart");
    }

    IEnumerator restart()
    {
        while (true)
        {
            yield return Timing.WaitForSeconds(1f);
            if (buttonList.Count > 0)
                foreach (var item in buttonList)
                {
                    if (item)
                        if (item.IsActive())
                        {
                            yield return Timing.WaitForOneFrame;
                            /*if (!GetComponentInChildren<ButtonControl>() && !transform.parent.GetComponent<Button>())
                            {
                                createButton(item);
                                buttonList.Add(item.GetComponentInChildren<ButtonControl>().GetComponent<Button>());
                            }*/
                            if (item.GetComponentInChildren<ButtonControl>())
                                item.GetComponentInChildren<ButtonControl>().transform.localPosition = new Vector3(0, 0, item.GetComponentInChildren<ButtonControl>().zAxis);
                        }
                    yield return Timing.WaitForOneFrame;
                }
        }
    }
}
