﻿using System;
using System.Collections;
using System.Collections.Generic;
using MEC;
using Sirenix.OdinInspector;
using Tictech.Utilities.Audio;
using UnityEngine;

public class HydroPumpController : MonoBehaviour
{
    [ReadOnly, SerializeField] private float fill = 20f;

    public Action<float> OnFillUpdated;

    [SerializeField] ParticleSystem powder;
    private CoroutineHandle workingHandle;
    private Collider ownCollider;
    [SerializeField] AudioRoutine audio;

    private Vector3 _initPos;
    private Quaternion _initRot;
    [SerializeField] GameObject sprayCollider;
    [SerializeField] private bool isResetOnDetached;

    private void Awake()
    {
        var grb = GetComponent<OVRGrabbable>();
        grb.onGrabBegin.AddListener(OnAttached);
        grb.onGrabRelease.AddListener(OnDetached);
        grb.onGrabRelease.AddListener(Release);
        grb.onTriggerBegin += Trigger;
        grb.onTriggerRelease += Release;
    }

    private void Start()
    {
        _initPos = transform.localPosition;
        _initRot = transform.localRotation;

        ownCollider = GetComponent<Collider>();
        Reset();
    }

    private void Update()
    {
        // UpdateInteractable();

#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.C))
            Trigger();

        if (Input.GetKeyUp(KeyCode.C))
            Release();
#endif
    }

    public void Reset()
    {
        OnFillUpdated?.Invoke(fill / 20f);
    }

    private void OnAttached()
    {
    }

    private void OnDetached()
    {
        if (isResetOnDetached)
            ResetPosition();
    }

    public void ResetPosition()
    {
        var grb = GetComponent<OVRGrabbable>();
        if (grb.grabbedBy != null)
        {
            grb.grabbedBy.GrabEnd();
        }

        var rb = GetComponent<Rigidbody>();
        rb.isKinematic = false;
        rb.isKinematic = true;
        transform.localPosition = _initPos;
        transform.localRotation = _initRot;
    }

    private void Trigger()
    {
        Debug.Log("Adam trigger");
        if (fill <= 0)
            return;

        powder.Play();
        audio.Play();
        workingHandle = Timing.RunCoroutine(Working());
        sprayCollider.SetActive(true);
    }

    private void Release()
    {
        if (!workingHandle.IsRunning)
            return;

        Timing.KillCoroutines(workingHandle);
        audio.Stop();
        powder.Stop();
        sprayCollider.SetActive(false);
    }

    IEnumerator<float> Working()
    {
        while (true)
        {
            // fill -= Time.deltaTime;
            // OnFillUpdated?.Invoke(fill / 20f);
            yield return Timing.WaitForOneFrame;
            // if(fill <= 0)
            //     Release();
        }
    }
}