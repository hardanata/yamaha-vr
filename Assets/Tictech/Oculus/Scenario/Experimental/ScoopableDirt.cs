﻿using MEC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Events;

namespace Tictech.Oculus.Scenario
{
    public class ScoopableDirt : ScenarioItem
    {
        
        public string toolId;
        [SerializeField] bool isScoopable = false;
        public bool isScooped = false;
        [SerializeField] public UnityEvent onScooped;
        ShovelController shovel;
        Transform marker;
        public bool isDefaultInit = false;
        public bool isTeleport = false;
        public Transform dropPoint;
        AudioSource audio;

        [SerializeField] bool isDestroyRB;
        private void Start()
        {
            if (!audio)
                if (GetComponent<AudioSource>())
                    audio = GetComponent<AudioSource>();
                else audio = gameObject.AddComponent<AudioSource>();
            audio.volume = .5f;

            if (isDefaultInit)
                base.Start();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (isScoopable)
            {
                ScenarioTool tool = new ScenarioTool();

                if (other.GetComponent<ScenarioTool>() != null && other.GetComponent<OVRGrabbable>() != null && other.GetComponent<ShovelController>() != null)
                {
                    shovel = other.GetComponent<ShovelController>();
                    tool = other.GetComponent<ScenarioTool>();
                }
                else if (other.transform.parent.GetComponent<ScenarioTool>() != null && other.transform.parent.GetComponent<OVRGrabbable>() != null && other.transform.parent.GetComponent<ShovelController>() != null)
                {
                    shovel = other.transform.parent.GetComponent<ShovelController>();
                    tool = other.transform.parent.GetComponent<ScenarioTool>();
                }
                else
                    return;

                if (tool.profile.toolName == toolId && shovel.gameObject.tag == "Scenario Tool")
                {
                    Transform shovelslot = null;
                    foreach (var slot in shovel.targetPositions)
                    {
                        if (slot.childCount <= 0)
                            shovelslot = slot;
                    }
                    if (shovelslot == null)
                        return;

                    var rb = GetComponent<Rigidbody>();
                    rb.useGravity = false;
                    rb.isKinematic = true;
                    var col = GetComponent<Collider>();
                    col.isTrigger = true;
                    if (marker)
                        marker.gameObject.SetActive(false);

                    transform.parent = shovelslot;
                    transform.position = shovelslot.position;
                    transform.localRotation = Quaternion.identity;
                    isScooped = true;

                    onScooped.Invoke();

                    if(isScoopable)
                        audio.PlayOneShot(ScenarioManager.Instance.audioReference.GetClip("Scoop"));
                    isScoopable = false;

                    if (_holoList.Count > 0)
                        foreach (var item in _holoList)
                        {
                            item.enabled = false;
                        }
                }
            }

            if (isScooped)
            {
                if (other.gameObject.tag == "DirtBucket")
                {
                    if (!isTeleport)
                    {
                        transform.parent = other.transform;
                        Timing.RunCoroutine(CompleteTool(GetToolPoint(toolId), 0f));

                        if (isDefaultInit)
                            GetComponent<OVRGrabbable>().grabbedBy.GrabEnd();
                        gameObject.SetActive(false);
                    }
                    else
                    {
                        Timing.RunCoroutine(CompleteTool(GetToolPoint(toolId), 0f));

                        var f = gameObject.AddComponent<MeshCollider>();
                        f.convex = true;

                        if (isDefaultInit)
                            GetComponent<OVRGrabbable>().grabbedBy.GrabEnd();

                        Transform point = transform;

                        if (!dropPoint && shovel)
                            point = shovel.fallPositions[Random.Range(0, shovel.fallPositions.Length)];
                        else
                            point = dropPoint;

                        transform.parent = point;
                        transform.localPosition = Vector3.zero;
                        var rb = GetComponent<Rigidbody>();
                        rb.useGravity = true;
                        rb.isKinematic = false;

                        if (isDestroyRB)
                            Destroy(transform.GetComponent<Rigidbody>(),2);
                    }
                    if(!audio.isPlaying) audio.PlayOneShot(ScenarioManager.Instance.audioReference.GetClip("Dirt"));
                    isScooped = false;
                }
            }
        }

        public void SetScoop(bool value)
        {
            isScoopable = value;
        }
    }
}
