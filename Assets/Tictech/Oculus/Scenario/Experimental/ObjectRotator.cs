﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ObjectRotator : MonoBehaviour
{
    public float x, y, z;

    [Header("Loop")]
    public bool isLoopYoyo;
    public Vector3 yoyoVector;
    public float yoyoTime;


    private void Start()
    {
        if (isLoopYoyo)
            transform.DOLocalRotate(yoyoVector, yoyoTime).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InOutSine);
    }
    void Update()
    {
        if (!isLoopYoyo)
            transform.Rotate(x, y, z * Time.deltaTime); //rotates 50 degrees per second around z axis
    }
}
