﻿using System.Collections;
using System.Collections.Generic;
using Arna.AssetManagement;
using Sirenix.OdinInspector;
using Tictech.Oculus.Scenario;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

[CreateAssetMenu(fileName = "New TagListScriptable", menuName = "Petrokimia/Tag Data Scriptable", order = 0)]
public class TagListScriptable : SerializedScriptableObject
{
    [System.Serializable]
    public class Tag
    {
        public string tagName = "Maintenance";
        public Color tagColor = new Color32(29, 250, 0, 255);
        [TextArea]
        public string detail = "Maintenance ";
    }
    public Tag[] tagList;
}
