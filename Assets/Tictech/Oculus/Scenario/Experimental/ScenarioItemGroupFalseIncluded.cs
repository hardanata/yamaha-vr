﻿using UnityEngine;
using UnityEngine.Events;

namespace Tictech.Oculus.Scenario
{
    public class ScenarioItemGroupFalseIncluded : ScenarioItemGroup
    {
        private void Start()
        {
            Invoke(nameof(CompleteCheck), 1f);
            items = GetComponentsInChildren<ScenarioItem>(true);
            foreach (var e in items)
            {
                e.onCompleted += AddComplete;
            }
            SetActive(false);
        }

        private void CompleteCheck()
        {
            if (items.Length > 0)
                return;

            Completed = true;
            onCompleted?.Invoke();
        }

        private void AddComplete()
        {
            _completedCount++;
            if (_completedCount < items.Length)
                return;

            Completed = true;
            onCompleted?.Invoke();
        }

        public void SetActive(bool value)
        {
            foreach (var e in items)
            {
                e.SetActive(value);
            }
        }
    }
}
