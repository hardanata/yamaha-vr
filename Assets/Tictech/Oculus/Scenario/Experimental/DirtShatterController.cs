﻿using MEC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Events;
namespace Tictech.Oculus.Scenario
{
    public class DirtShatterController : MonoBehaviour
    {
        [SerializeField] GameObject DirtUnshattered;
        [SerializeField] GameObject[] DirtShattered;
        [SerializeField] public string toolId;
        [SerializeField] float shatterForce;
        [SerializeField] float shatterRadius = 1;
        [SerializeField] int health;
        [SerializeField] Transform indicator;
        [SerializeField] UnityEvent onShattered;
        Transform marker;

        [Header("DestroyShatter")]
        [SerializeField] bool isDestroyShattered;
        [SerializeField] float destroyDelay;
        AudioSource audio;

        public class destroyOnEndPlay : MonoBehaviour
        {
            AudioSource audio;
            void Start()
            {
                audio = transform.gameObject.AddComponent<AudioSource>();
                gameObject.GetComponent<AudioSource>().PlayOneShot(ScenarioManager.Instance.audioReference.GetClip("Dirt"));
            }

            private void Update()
            {
                if (!audio.isPlaying) Destroy(gameObject);
            }
        }

        private void Start()
        {
            if (DirtUnshattered == null)
                DirtUnshattered = this.gameObject;

            if (indicator != null)
            {
                marker = Instantiate(ScenarioManager.Instance.toolMarkerPrefab).transform;
                marker.name = gameObject.name + " marker";
                marker.SetParent(indicator, true);
                marker.localPosition = Vector3.zero;
                marker.SetParent(null, true);
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.GetComponent<ScenarioTool>() != null && other.GetComponent<ScenarioGrabbable>() != null)
            {
                var tool = other.GetComponent<ScenarioTool>();
                if (tool.profile.toolName == toolId && other.gameObject.tag == "Scenario Tool")
                {
                    Damage();
                }
            }
        }

        public void Damage()
        {
            Debug.Log("BERGHA");
            health--;
            
            if (health <= 0)
            {
                foreach (var item in DirtShattered)
                {
                    item.SetActive(true);
                    var rb = item.GetComponent<Rigidbody>();
                    rb.AddExplosionForce(shatterForce, transform.position, shatterRadius);
                    GameObject Instance = new GameObject("Dirt Audio", typeof(destroyOnEndPlay));
                    Instance.transform.position = transform.position;
                    if (isDestroyShattered)
                    {
                        Destroy(item, destroyDelay);
                    }
                }
                onShattered.Invoke();
                if (marker != null)
                    marker.gameObject.SetActive(false);
                DirtUnshattered.SetActive(false);
            }
        }
    }
}
