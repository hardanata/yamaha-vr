﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using MEC;
using Sirenix.Utilities;
using Tictech.EnterpriseUniversity;
using Tictech.Oculus.Scenario;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LobbyMenu2 : MonoBehaviour
{
    public Transform startingPoint;
    public LobbyScenarioData scenarioData;
    public TagListScriptable tagListData;

    [Header("Scenario Detail")]
    public GameObject scenarioDetailPanel;
    public TextMeshProUGUI scenarioTypeText;
    public TextMeshProUGUI scenarioTopicText;
    public TextMeshProUGUI scenarioTitleText;
    public TextMeshProUGUI scenarioDetailText;
    public Image scenarioPreviewImage;


    [Header("UI Content")]
    public GameObject topicPrefab;
    public GameObject tagPrefab;
    public GameObject scenarioPrefab;
    public Transform topicContainer;
    public Transform tagContainer;
    public Transform scenarioContainer;
    public Transform tagInfo;

    [Header("Tag List")]
    public List<TagList> tagList;
    [System.Serializable]
    public class TagList
    {
        public bool addString = true;
        public string tag;
        public string sub;
        public Sprite sprite, spriteDetailPanel;
        public Color tagColorDetailPanel;
    }

    private List<GameObject> topics;
    private Dictionary<string, GameObject> tags;
    public List<GameObject> scenarios;

    private bool loadingScene = false;

    private ScenarioTopic _selectedTopic;
    private ScenarioData _selectedScenario;

#if UNITY_EDITOR
    [Header("Debug")]
    public bool runDebugSequence;
    public int debugTopicIndex;
    public int debugScenarioIndex;

    IEnumerator<float> DebugSequence()
    {
        Debug.Log("Started lobby debug sequence");
        yield return Timing.WaitForSeconds(2f);
        SelectTopic(topics[debugTopicIndex]);
        yield return Timing.WaitForSeconds(2f);
        LoadTags(scenarioData.data[debugTopicIndex]);
        yield return Timing.WaitForSeconds(2f);
        LoadScenario(_selectedTopic.scenarios[debugScenarioIndex]);
        //FindObjectOfType<InScenePrefabToggle>().StartCoroutine("ZeroIndex");
        yield return Timing.WaitForSeconds(2f);
        LoadScenarioScene();
    }
#endif

    private void Awake()
    {
        loadingScene = false;
        QualitySettings.SetQualityLevel(0, true);
        topics = new List<GameObject>();
        tags = new Dictionary<string, GameObject>();
        scenarios = new List<GameObject>();

        GameObject.FindWithTag("Player").transform.position = startingPoint.position;

        if(topicPrefab) LoadTopics(0);

#if UNITY_EDITOR
        if (runDebugSequence)
            Timing.RunCoroutine(DebugSequence());
#endif
    }

    public void LoadTopics(int i)
    {
        foreach (var t in topics)
        {
            Destroy(t);
        }
        topics.Clear();

        foreach (var e in scenarioData.data)
        {
            var item = Instantiate(topicPrefab, topicContainer);
            item.transform.GetChild(1).GetComponent<Text>().text = e.title;
            item.transform.GetChild(2).GetComponent<Image>().sprite = e.preview;
            item.transform.GetChild(3).GetComponent<Text>().text = e.summary;
            item.name = e.title;
            item.GetComponentInChildren<Button>().onClick.AddListener(delegate
            {
                SelectTopic(item);
                LoadTags(e);
            });
            topics.Add(item);
        }
        SelectTopic(topics[(i)]);
        LoadTags(scenarioData.data[i]);
    }

    private void SelectTopic(GameObject item)
    {
        foreach (var e in topics)
        {
            e.GetComponentInChildren<Image>().color = e == item ? Color.gray : Color.black;
        }
    }

    private void LoadTags(ScenarioTopic data)
    {
        _selectedTopic = data;
        foreach (var t in tagContainer.GetComponentsInChildren<Transform>())
        {
            if (t.parent == tagContainer.transform)
                Destroy(t.gameObject);
        }
        tags.Clear();

        //bool firstTag = false;
        foreach (var s in data.scenarios)
        {
            foreach (var t in s.tags)
            {
                var key = t.ToUpper();
                if (tags.ContainsKey(key))
                    continue;

                GameObject tagObject = Instantiate(tagPrefab, tagContainer);
                tagObject.name = key;
                tags.Add(key, tagObject);
                TextMeshProUGUI[] ugui = tags[key].GetComponentsInChildren<TextMeshProUGUI>();
                tags[key].GetComponentInChildren<Button>().onClick.AddListener(delegate
                {
                    alphaChange(key);
                    filterScenarios(key);
                });
                /*if(!firstTag)
                {
                    tags[key].GetComponentInChildren<Button>().onClick.Invoke();
                    firstTag = true;
                }*/
                if (tagList.Count > 0) tags[key].GetComponentInChildren<Image>().sprite = tagList[0].sprite;
                foreach (var item in tagList)
                {
                    if (item.tag == t)
                    {
                        tags[key].GetComponentInChildren<Image>().sprite = item.sprite;
                        if (ugui.Length > 0 && item.addString) tags[key].GetComponentsInChildren<TextMeshProUGUI>()[0].text = item.tag;
                        if (ugui.Length > 1 && item.addString) tags[key].GetComponentsInChildren<TextMeshProUGUI>()[1].text = item.sub;
                    }
                }
            }
        }
        LoadScenarios(data.scenarios[0].tags[0].ToUpper());
        newTagInfo(tagListData);
        tags.FirstOrDefault().Value.GetComponentInChildren<Button>().onClick.Invoke();
    }

    public void alphaChange(string key)
    {
        foreach (var item in tagContainer.GetComponentsInChildren<Image>())
        {
            if (item.transform.parent == tagContainer)
            {
                if (item.gameObject == tags[key].gameObject)
                    foreach (var item2 in item.transform.GetComponentsInChildren<Button>().ToList())
                        //item2.interactable = true;
                        item.GetComponentInChildren<Image>().color = new Color(
                            item.GetComponentInChildren<Image>().color.r,
                            item.GetComponentInChildren<Image>().color.g,
                            item.GetComponentInChildren<Image>().color.b,
                            1f);
                
                else foreach (var item2 in item.transform.GetComponentsInChildren<Button>().ToList())
                        //item2.interactable = false;
                        item.GetComponentInChildren<Image>().color = new Color(
                            item.GetComponentInChildren<Image>().color.r,
                            item.GetComponentInChildren<Image>().color.g,
                            item.GetComponentInChildren<Image>().color.b,
                            .5f);
            }
        }
    }

    private void LoadScenarios(string scenarioTag)
    {
        //scenarioDetailPanel.SetActive(false);
        scenarioDetailPanel.GetComponentsInChildren<Button>().LastOrDefault().interactable = false;
        foreach (var e in scenarioContainer.GetComponentsInChildren<Transform>())
        {
            if (e.parent == scenarioContainer)
                Destroy(e.gameObject);
        }
        scenarios.Clear();

        foreach (var e in _selectedTopic.scenarios)
        {
            if (e.exclude)
                continue;

            var item = Instantiate(scenarioPrefab, scenarioContainer);
            item.name = e.prefabName;
            item.GetComponentInChildren<TextMeshProUGUI>().text = e.title;
            item.GetComponentInChildren<Image>().sprite = e.preview;
            item.GetComponentInChildren<Button>().onClick.AddListener(delegate
            {
                item.GetComponentInParent<SwipeControl>()?.forceSelect(item);
                scenarioDetailPanel.GetComponentsInChildren<Button>().LastOrDefault().interactable = true;
                LoadScenario(e);
            });
            //Debug.Log("yay" + item.GetComponentInChildren<Button>().onClick);
            scenarios.Add(item);
            foreach (var tag in e.tags)
            {
                if (!tags.FirstOrDefault().Value.name.ToUpper().Equals(tag.ToUpper()))
                {
                    item.gameObject.SetActive(false);
                    break;
                }
            }
        }
        if (GetComponentInChildren<OVRCustomRaycaster>())
            GetComponentInChildren<OVRCustomRaycaster>().ReInit();
        else if (audioButton.Instance)
            audioButton.Instance.refreshAudioButton();
        bool first = false;
        foreach (var item in tags)
        {
            if (first) {
                item.Value.GetComponentsInChildren<Button>().ForEach(f => f.GetComponentInChildren<Image>().color = new Color(
                            f.GetComponentInChildren<Image>().color.r,
                            f.GetComponentInChildren<Image>().color.g,
                            f.GetComponentInChildren<Image>().color.b,
                            1));
            }
            else first = true;
        }
        if (scenarioContainer.GetComponent<SwipeControl>())
            scenarioContainer.transform.localPosition += new Vector3(10000, 0, 0);
    }

    void refreshActivation(string key, bool isActive)
    {
        float alpha = .5f;
        if (isActive) alpha = 1f;
        foreach (var s in _selectedTopic.scenarios)
        {
            if (tags[key])
                if (tags[key].GetComponentInChildren<ButtonControl>())
                    if (tags[key].GetComponentInChildren<ButtonControl>().GetComponent<Button>().GetComponentInChildren<Image>().color.a == alpha)
                        foreach (var e in scenarioContainer.GetComponentsInChildren<Transform>(true))
                        {
                            if (e.parent != scenarioContainer) continue;
                            //Debug.LogError("0. obj name : " + e.gameObject.name + " - scene name : " + s.sceneName)
                            //Debug.Log(e.gameObject.name + " : t " + s.tags[0] + " | key " + key + " | bool " + s.tags.Any(t => t.ToUpper().Equals(key.ToUpper())));
                            if (e.gameObject.name == s.prefabName
                                && /*s.tags.Any(t => t.ToUpper().Equals(key.ToUpper()))*/ s.tags.FirstOrDefault().ToUpper().Equals(key.ToUpper())
                                && /*s.tags.Any(t => t.ToUpper().Equals(key.ToUpper()))*/ s.tags.LastOrDefault().ToUpper().Equals(key.ToUpper()))
                            {
                                //Debug.Log("trigger change");
                                e.gameObject.SetActive(isActive);
                                //Debug.LogError("active " + e.gameObject.activeSelf);
                            }
                        }
                    else continue;
                else if (tags[key].GetComponentInChildren<Button>())
                    if (tags[key].GetComponentInChildren<Button>().GetComponentInChildren<Image>().color.a == alpha)
                        foreach (var e in scenarioContainer.GetComponentsInChildren<Transform>(true))
                        {
                            if (e.parent != scenarioContainer) continue;
                            if (e.gameObject.name == s.prefabName
                                && s.tags.FirstOrDefault().ToUpper().Equals(key.ToUpper())
                                && s.tags.LastOrDefault().ToUpper().Equals(key.ToUpper()))
                            {
                                e.gameObject.SetActive(isActive);
                            }
                        }
        }
    }

    private void filterScenarios(string scenarioTag)
    {
        //scenarioDetailPanel.SetActive(false);
        scenarioDetailPanel.GetComponentsInChildren<Button>().LastOrDefault().interactable = false;
        foreach (var item in tagContainer.GetComponentsInChildren<Button>())
        {
            Transform t = item.GetComponent<Transform>();
            if (t.parent != tagContainer) continue;
            if (!t.gameObject.name.ToUpper().Equals(scenarioTag.ToUpper())) refreshActivation(t.gameObject.name, false);
        }

        foreach (var item in tagContainer.GetComponentsInChildren<Button>())
        {
            Transform t = item.GetComponent<Transform>();
            if (t.parent != tagContainer) continue;
            if (t.gameObject.name.ToUpper().Equals(scenarioTag.ToUpper())) refreshActivation(scenarioTag, true);
        }

        foreach (var t in scenarioContainer.GetComponentsInChildren<Transform>(true))
        {
            if (t.parent == scenarioContainer.transform && !scenarios.Contains(t.gameObject))
                Destroy(t.gameObject);
        }
        newTagInfo(tagListData);
        if (scenarioContainer.GetComponent<SwipeControl>())
            scenarioContainer.transform.localPosition += new Vector3(10000, 0, 0);
    }

    public void LoadScenario(ScenarioData data)
    {
        _selectedScenario = data;
        if(scenarioData.data.Find(d => d.scenarios.Contains(data)))
            scenarioTopicText.text = scenarioData.data.Find(d => d.scenarios.Contains(data)).title;
        scenarioTypeText.text = data.type.ToUpper();
        scenarioTypeText.enabled = false;
        scenarioTitleText.text = data.title;
        scenarioDetailText.text = data.detail;
        scenarioPreviewImage.sprite = data.preview;
        scenarioDetailPanel.SetActive(true);
        scenarioDetailPanel.GetComponentsInChildren<Button>().LastOrDefault().interactable = true;
        foreach (var item in scenarioTypeText.GetComponentsInChildren<Transform>())
        {
            if (item.transform.parent == scenarioTypeText.transform)
            {
                Destroy(item.gameObject);
            }
        }
        foreach (var item in data.tags)
        {
            GameObject Instance = new GameObject(item, typeof(Image), typeof(ContentSizeFitter), typeof(HorizontalLayoutGroup));
            if (tagList.Any(t => t.tag.ToUpper() == item.ToUpper()))
            {
                Instance.GetComponent<Image>().sprite = tagList.Find(t => t.tag.ToUpper().Contains(item.ToUpper())).spriteDetailPanel;
                Instance.GetComponent<Image>().color = tagList.Find(t => t.tag.ToUpper().Contains(item.ToUpper())).tagColorDetailPanel;
            }
            Instance.transform.SetParent(scenarioTypeText.transform);
            Instance.transform.localScale = Vector3.one;
            Instance.transform.localPosition = Vector3.zero;
            Instance.transform.localRotation = Quaternion.identity;
            Instance.GetComponent<ContentSizeFitter>().horizontalFit = ContentSizeFitter.FitMode.PreferredSize;
            Instance.GetComponent<HorizontalLayoutGroup>().childAlignment = TextAnchor.MiddleLeft;
            Instance.GetComponent<HorizontalLayoutGroup>().padding = new RectOffset(25, 25, 25, 25);

            GameObject Instance2 = new GameObject(item, typeof(TextMeshProUGUI), typeof(ContentSizeFitter));
            Instance2.transform.SetParent(Instance.transform);
            Instance2.transform.localScale = Vector3.one;
            Instance2.transform.localPosition = Vector3.zero;
            Instance2.transform.localRotation = Quaternion.identity;
            Instance2.GetComponent<TextMeshProUGUI>().overflowMode = TextOverflowModes.Overflow;
            Instance2.GetComponent<TextMeshProUGUI>().enableWordWrapping = false;
            Instance.GetComponent<ContentSizeFitter>().horizontalFit = ContentSizeFitter.FitMode.PreferredSize;
            Instance2.GetComponent<TextMeshProUGUI>().text = item + " " + data.type;
        }
        loadingScene = false;
        Debug.Log("Load Scenario : " + data);
        ScenarioLoader.SelectedScenario = data.prefabName;
        ScenarioLoader.SelectedId = data.trainingId;
        if (ScenarioLoader.instance)
        {
            foreach (var item in ScenarioLoader.instance.scenarioData.data)
            {
                //if (!_selectedTopic)
                    _selectedTopic = scenarioData.data.Find(t => t.scenarios.Contains(data));
                if (item.Value.title.Equals(_selectedTopic.title))
                {
                    ScenarioLoader.SelectedTopic = item.Key;
                }
                
            }
        }
        LayoutRebuilder.ForceRebuildLayoutImmediate(scenarioDetailPanel.GetComponentInChildren<LayoutGroup>().GetComponent<RectTransform>());
    }

    private void LoadScene(string sname)
    {
        SceneManager.LoadScene(sname, LoadSceneMode.Single);
    }

    public void LoadScenarioScene()
    {
        Debug.Log("LoadingScene : " + loadingScene);
        if (loadingScene) return;
        if (_selectedScenario)
            if (!string.IsNullOrEmpty(_selectedTopic.sceneName))
            {
                Debug.Log("Loading scenario master scene: " + _selectedTopic.sceneName);
                LoadScene(_selectedTopic.sceneName);
            }
            else if (!string.IsNullOrEmpty(_selectedScenario.sceneName))
            {
                Debug.Log("Loading scenario scene: " + _selectedScenario.sceneName);
                LoadScene(_selectedScenario.sceneName);
            }
            else
            {
                var path = "Scenarios/" + _selectedTopic.title + "/" + _selectedScenario.prefabName;
                //Debug.Log("Loading prefab at: " + path + "\r\nResource: " + Resources.Load(path));
                FindObjectOfType<InScenePrefabToggle>().TogglePrefabFromResource(path);
            }
        loadingScene = true;
        string[] recent = PlayerPrefsX.GetStringArray("Recent", "None", 3);
        string[] recentScene = PlayerPrefsX.GetStringArray("RecentScene", "None", 3);
        bool savetoRecent = false;
        foreach (var item in scenarioData.data)
        {
            foreach (var item2 in item.scenarios)
            {
                if (_selectedScenario)
                    if (Equals(item2.prefabName, _selectedScenario.prefabName))
                    {
                        savetoRecent = true;
                        Debug.Log("save to recent : " + savetoRecent);
                    }
            }
        }
        if (savetoRecent == true)
        {
            bool sameValue = false;
            foreach (var item in recentScene)
            {
                if (Equals(item, _selectedScenario.prefabName)) sameValue = true;
            }
            Debug.Log("same value : " + sameValue);
            if (!sameValue)
            {
                for (int i = recent.Length; i > 1; i--)
                {
                    recent[i - 1] = recent[i - 2];
                    recentScene[i - 1] = recentScene[i - 2];
                    //Debug.Log("Assign Prefs : " + recent[i - 1] + " " + _selectedScenario.title);
                }
            }
            else
            {
                for (int i = recent.Length; i > 1; i--)
                {
                    //Debug.Log("Assign Prefs : " + recent[i - 1]  + " " + _selectedScenario.title);
                    if (recent[i - 1] == _selectedScenario.title)
                    {
                        string rec = recent[i - 1];
                        string recScene = recentScene[i - 1];
                        recent[i - 1] = recent[i - 2];
                        recentScene[i - 1] = recentScene[i - 2];
                        recent[i - 2] = rec;
                        recentScene[i - 2] = recScene;
                    }
                    //Debug.Log("Result Assign Prefs : " + recent[i - 1] + " " + _selectedScenario.title);
                }
            }
        }
        if (_selectedScenario)
        {
            recent[0] = _selectedScenario.title;
            recentScene[0] = _selectedScenario.prefabName;
        }

        PlayerPrefsX.SetStringArray("Recent", recent);
        PlayerPrefsX.SetStringArray("RecentScene", recentScene);
    }

    public void newTagInfo(TagListScriptable t)
    {
        if (!t) return;
        Transform tag;
        if (tagInfo)
            tag = tagInfo;
        else return;
        if (tag)
            foreach (var item in t.tagList)
                if (tags.Any(s => s.Value.name.ToUpper().Equals(item.tagName.ToUpper())))
                    foreach (var s in tags)
                        if (s.Value.GetComponentsInChildren<ButtonControl>().Length > 0)
                            if (s.Value.GetComponentsInChildren<ButtonControl>().FirstOrDefault().GetComponentInParent<Button>().IsInteractable() &&
                                s.Value.name.ToUpper().Equals(item.tagName.ToUpper()))
                            {
                                //Debug.Log("tagging" + item.tagName.ToTitleCase());
                                tag.GetComponentsInChildren<TextMeshProUGUI>(true).FirstOrDefault().text = item.tagName.ToTitleCase();
                                tag.GetComponentsInChildren<TextMeshProUGUI>(true).LastOrDefault().text = item.detail;
                                break;
                            }
                            else continue;
                        else if (s.Value.GetComponentsInChildren<Button>().Length > 0)
                            if (s.Value.GetComponentsInChildren<Button>().FirstOrDefault().GetComponentInChildren<Image>().color.a == 1f &&
                                s.Value.name.ToUpper().Equals(item.tagName.ToUpper()))
                            {
                                //Debug.Log("tagging" + item.tagName.ToTitleCase());
                                tag.GetComponentsInChildren<TextMeshProUGUI>(true).FirstOrDefault().text = item.tagName.ToTitleCase();
                                tag.GetComponentsInChildren<TextMeshProUGUI>(true).LastOrDefault().text = item.detail;
                                break;
                            }
                            else continue;
        Canvas.ForceUpdateCanvases();
        LayoutRebuilder.ForceRebuildLayoutImmediate(tagInfo.GetComponent<RectTransform>());
    }
}