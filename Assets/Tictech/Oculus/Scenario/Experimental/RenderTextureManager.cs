﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RenderTextureManager : MonoBehaviour
{
    public enum qualitySetting
    {
        low, normal
    }
    public qualitySetting _qualitySetting;
    // Start is called before the first frame update
    void Start()
    {
        switch (_qualitySetting)
        {
            case qualitySetting.low:
                QualitySettings.SetQualityLevel(1);
                break;
            case qualitySetting.normal:
                QualitySettings.SetQualityLevel(0);
                break;
            default:
                QualitySettings.SetQualityLevel(1);
                break;
        }
    }
}
