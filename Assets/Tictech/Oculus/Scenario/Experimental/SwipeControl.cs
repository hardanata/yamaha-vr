﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Events;
using DG.Tweening;
using UnityEngine.UI;
using System.Linq;
using MEC;
using Sirenix.OdinInspector;

public class SwipeControl : MonoBehaviour
{
    public Color[] colors;
    public GameObject scrollbar, imageContent;
    public Sprite contentStart, contentMid, contentEnd;
    public List<Transform> firstHierarchyChild; 
    public bool enableSwipe = true;
    [ReadOnly]
    public float scroll_pos = 0;
    float[] pos;
    public bool autoFocus = true;
    private bool runIt = false;
    private float time;
    private Button takeTheBtn;
    int btnNumber;
    public int selected { get; set; }
    private int currentSelected;
    [Header("Select OnTouch")]
    public bool directSelection = true;
    public float colliderResize = 200;
    private Vector3 offset = Vector3.zero;
    private bool collide = false;
    [Header("Click Button")]
    public GameObject confirmButton;
    public List<Button.ButtonClickedEvent> onClick;
    public static GameObject thisGO { get; set; }
    [ReadOnly]
    public float distance;
    [Header("Selection Mark")]
    public Sprite selectMarker;
    public float markerSize = 1;
    public GameObject marker { get; set; }
    public LobbyMenu2 lobbyMenu;

    public enum axis
    {
        X,Y
    }
    public axis axisScroll = axis.X;
    public class viewportClass : MonoBehaviour
    {
        void Awake()
        {
            if (SwipeControl.thisGO.GetComponent<Collider>())
                SwipeControl.thisGO.GetComponent<Collider>().enabled = false;
        }
        void OnTriggerStay(Collider other)
        {
            if (other.tag != "Yubi") return;
            if (SwipeControl.thisGO.GetComponent<Collider>())
                SwipeControl.thisGO.GetComponent<Collider>().enabled = true;
            //Debug.Log("BBB");
        }

        void OnTriggerExit(Collider other)
        {
            if (other.tag != "Yubi") return;
            if (SwipeControl.thisGO.GetComponent<Collider>()) 
                SwipeControl.thisGO.GetComponent<Collider>().enabled = false;
        }
    }

    public class dirSelect : MonoBehaviour
    {
        BoxCollider col;
        void Awake()
        {
            transform.parent.parent.tag = "Boundary";
            GetComponent<RectTransform>().pivot = new Vector2(.5f, .5f);
            col = gameObject.AddComponent<BoxCollider>();
            Rigidbody rb = gameObject.AddComponent<Rigidbody>();
            if (transform.parent.GetComponent<SwipeControl>()) col.size = GetComponentInChildren<Image>().sprite.bounds.extents * (transform.parent.GetComponent<SwipeControl>().colliderResize);
            col.isTrigger = true;
            if (rb)
            {
                rb.isKinematic = true;
                rb.useGravity = false;
            }
            transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y, transform.localScale.z);
            gameObject.layer = LayerMask.NameToLayer("Water");
            if (transform.GetComponentInParent<SwipeControl>().marker)
                if (transform.GetComponentInParent<SwipeControl>().marker.transform.parent != transform)
                    transform.GetComponentInParent<SwipeControl>().marker.transform.SetParent(transform);
        }

        void OnTriggerEnter(Collider other)
        {
            if (other.tag == "Boundary" && transform.parent.GetComponent<SwipeControl>()) col.size = GetComponentInChildren<Image>().sprite.bounds.extents * (transform.parent.GetComponent<SwipeControl>().colliderResize);
            if (other.tag != "Yubi") return;
            if (transform.GetComponentInParent<SwipeControl>())
            {
                SwipeControl s = transform.GetComponentInParent<SwipeControl>();
                s.createMarker();
                s.marker.transform.SetParent(transform);
                s.marker.GetComponent<RectTransform>().anchoredPosition = Vector3.zero;
                s.selected = transform.GetSiblingIndex();
                s.selecting();
            }
            /*if (s.selected == transform.GetSiblingIndex())*/
            GetComponent<Button>().onClick.Invoke();
        }

        void OnTriggerExit(Collider other)
        {
            if (other.tag == "Boundary") col.size = Vector3.one;
        }
    }

    public void forceSelect(GameObject itemSub)
    {
        itemSub.GetComponentInParent<SwipeControl>().marker.transform.SetParent(itemSub.transform);
        itemSub.GetComponentInParent<SwipeControl>().marker.transform.GetComponent<RectTransform>().anchoredPosition = Vector3.zero;
    }

    public List<Transform> getFirstHierarchyChild(GameObject parentObj)
    {
        List<Transform> childList = new List<Transform>();
        foreach (Transform item in parentObj.transform.GetComponentsInChildren<Transform>())
        {
            if (item.parent == transform) childList.Add(item);
        }
        return childList;
    }

    public List<Transform> getFirstHierarchyChild(GameObject parentObj, bool includeInactive)
    {
        List<Transform> childList = new List<Transform>();
        foreach (Transform item in parentObj.transform.GetComponentsInChildren<Transform>(includeInactive))
        {
            if (item.parent == transform) childList.Add(item);
        }
        return childList;
    }

    void Start()
    {
        currentSelected = -1;
        selected = 0;
    }

    void Awake()
    {
        collide = false;
        thisGO = this.gameObject;
        if(!transform.parent.gameObject.GetComponent<viewportClass>())transform.parent.gameObject.AddComponent<viewportClass>();
        onClick.Clear();
        Button[] b = GetComponentsInChildren<Button>();
        foreach (var v in b)
        {
            onClick.Add(v.onClick);
        }
        firstHierarchyChild = getFirstHierarchyChild(gameObject);
    }

    public void selectIndex(int i)
    {
        if (scroll_pos >= 0 && scroll_pos <= 1)
        {
            scroll_pos = distance * (float) i;
            scroll_pos = Mathf.Clamp(scroll_pos, 0, 1);
            scrollbar.GetComponent<Scrollbar>().value = scroll_pos;

            //selected = Mathf.Clamp(selected += i, 0, transform.childCount - 1);
            firstHierarchyChild = getFirstHierarchyChild(gameObject);
            if(firstHierarchyChild.Count > i)
            selected = firstHierarchyChild[Mathf.Clamp(i, 0, firstHierarchyChild.Count - 1)].GetSiblingIndex();
            selected = Mathf.Clamp(selected,
                firstHierarchyChild.FirstOrDefault().GetSiblingIndex(),
                firstHierarchyChild.LastOrDefault().GetSiblingIndex());

            if (marker != null)
            {
                marker.transform.SetParent(transform.GetChild(selected));
                marker.GetComponent<RectTransform>().anchoredPosition = Vector3.zero;
                marker.transform.localScale = new Vector2(markerSize, markerSize);
                Awake();
            }
        }
    }

    public void scrollAdd(int i)
    {
        if (scroll_pos >= 0 && scroll_pos <= 1)
        {
            scroll_pos += distance * i;
            scroll_pos = Mathf.Clamp(scroll_pos, 0, 1);
            scrollbar.GetComponent<Scrollbar>().value = scroll_pos;

            //selected = Mathf.Clamp(selected += i, 0, transform.childCount - 1);
            firstHierarchyChild = getFirstHierarchyChild(gameObject);
            selected = firstHierarchyChild[Mathf.Clamp(firstHierarchyChild.FindIndex(f => f.transform == transform.GetChild(selected)) + i, 0, firstHierarchyChild.Count - 1)].GetSiblingIndex();
            selected = Mathf.Clamp(selected,
                firstHierarchyChild.FirstOrDefault().GetSiblingIndex(),
                firstHierarchyChild.LastOrDefault().GetSiblingIndex());

            if (marker != null)
            {
                marker.transform.SetParent(transform.GetChild(selected));
                marker.GetComponent<RectTransform>().anchoredPosition = Vector3.zero;
                marker.transform.localScale = new Vector2(markerSize, markerSize);
                Awake();
            }
        }
    }

    public void scrollMoveX(float i)
    {
        collide = true;
        //transform.localPosition += new Vector3(i, 0, 0);
        IEnumerator cor = TweenMove(new Vector3(i, 0, 0));
        StartCoroutine(cor);
        Awake();
        collide = false;
    }

    public void scrollMoveY(float i)
    {
        collide = true;
        //transform.localPosition += new Vector3(0, i, 0);
        IEnumerator cor = TweenMove(new Vector3(0, i, 0));
        StartCoroutine(cor);
        Awake();
        collide = false;
    }

    public void instantMoveX(float i)
    {
        collide = true;
        transform.localPosition += new Vector3(i, 0, 0);
        Awake();
        collide = false;
    }

    public void instantMoveY(float i)
    {
        collide = true;
        transform.localPosition += new Vector3(0, i, 0);
        Awake();
        collide = false;
    }

    IEnumerator TweenMove (Vector3 i)
    {
        transform.DOLocalMove(transform.localPosition + i, 1f);
        yield return Timing.WaitForSeconds(1f);
    }

    public void createMarker()
    {
        if (GetComponentsInChildren<Button>().Length > 0 && directSelection && GetComponentsInChildren<dirSelect>().Length == 0)
        {
            Button[] b = GetComponentsInChildren<Button>();
            foreach (var v in b)
            {
                v.gameObject.AddComponent<dirSelect>();
                /*Debug.Log(v.GetComponent<dirSelect>() + " dirSelectAdded");*/
            }
        }
        
        if (selectMarker != null)
        {
            if (marker == null && transform.childCount > 0)
            {
                GameObject g = new GameObject();
                marker = GameObject.Instantiate(g, this.transform.GetChild(Mathf.Clamp(selected, 0, transform.childCount - 1)));
                Destroy(g);
                Image im = marker.AddComponent<Image>();
                im.sprite = selectMarker;
                marker.transform.localScale = new Vector2(markerSize, markerSize);
                Debug.Log("yay create selector marker");
            }
        }
    }

    public void changeScrollPos()
    {
        if (Input.GetMouseButton(0) || collide == true)
        {
            scroll_pos = scrollbar.GetComponent<Scrollbar>().value;
        }
        else
        {
            transform.localPosition = new Vector3(transform.localPosition.x, 0, transform.localPosition.z);
            for (int i = 0; i < pos.Length; i++)
            {
                if (scroll_pos < pos[i] + (distance / 2) && scroll_pos > pos[i] - (distance / 2))
                {
                    if (autoFocus)
                        if (enableSwipe)
                            scrollbar.GetComponent<Scrollbar>().value = Mathf.Lerp(scrollbar.GetComponent<Scrollbar>().value, pos[i], 0.1f);
                        else scrollbar.GetComponent<Scrollbar>().value = pos[i];
                }
            }
        }

    }

    // Update is called once per frame
    void Update()
    {
        createMarker();
        pos = new float[transform.childCount];
        distance = 1f / (pos.Length - 1f);

        if (runIt)
        {
            Lerping(distance, pos, takeTheBtn);
            time += Time.deltaTime;

            if (time > 1f)
            {
                time = 0;
                runIt = false;
            }
        }

        for (int i = 0; i < pos.Length; i++)
        {
            pos[i] = distance * i;
        }
        
        changeScrollPos();

        selecting();
    }

    public void selecting ()
    {
        if (firstHierarchyChild.Count != transform.childCount) firstHierarchyChild = getFirstHierarchyChild(gameObject);
        if (imageContent)
            if (imageContent.transform.childCount != transform.childCount)
            {
                foreach (var item in firstHierarchyChild)
                {
                    GameObject imInstance = new GameObject("Image Content", typeof(Image));
                    imInstance.transform.SetParent(imageContent.transform);
                    imInstance.transform.localScale = Vector3.one;
                    imInstance.transform.localPosition = Vector3.zero;
                    imInstance.GetComponent<RectTransform>().sizeDelta = new Vector2(
                        ((imageContent.GetComponent<RectTransform>().sizeDelta.x * .9f / firstHierarchyChild.Count) /** 
                        (GetComponentInParent<Canvas>().GetComponent<RectTransform>().localScale.x)*/),
                        imInstance.GetComponent<RectTransform>().sizeDelta.y);
                    Image im = imInstance.GetComponent<Image>();
                    im.sprite = contentMid;
                    if(im.transform.GetSiblingIndex() == 0) im.sprite = contentStart;
                    if (im.transform.GetSiblingIndex() == transform.parent.childCount-1) im.sprite = contentEnd;
                    im.preserveAspect = true;
                }
            }
        for (int i = 0; i < pos.Length; i++)
        {
            if (scroll_pos < pos[i] + (distance / 2) && scroll_pos > pos[i] - (distance / 2))
            {
                //Debug.LogWarning("Current Selected Level" + i);
                transform.GetChild(i).localScale = Vector3.Lerp(transform.GetChild(i).localScale, new Vector3(1f, 1f, 1f), 0.1f);
                if (imageContent)
                {
                    if (imageContent.transform.childCount >= i+1 && colors.Length >= 2)
                    {
                        imageContent.transform.GetChild(i).localScale = Vector3.Lerp(imageContent.transform.GetChild(i).localScale, new Vector3(1f, 1f, 1f), 0.1f);
                        imageContent.transform.GetChild(i).GetComponent<Image>().color = colors[1];
                    }
                }
                for (int j = 0; j < pos.Length; j++)
                {
                    if (j != i)
                    {
                        if (imageContent)
                            if (imageContent.transform.childCount >= j+1)
                            {
                                imageContent.transform.GetChild(j).GetComponent<Image>().color = colors[0];
                                imageContent.transform.GetChild(j).localScale = Vector3.Lerp(imageContent.transform.GetChild(j).localScale, new Vector3(1f, 1f, 1f), 1f);
                            }
                        transform.GetChild(j).localScale = Vector3.Lerp(transform.GetChild(j).localScale, new Vector3(1f, 1f, 1f), 0.1f);
                    }
                }
                reassignButton();
            }
        }
    }

    public void reassignButton()
    {
        if (selected < transform.childCount)
            if (transform.GetChild(selected).gameObject.activeInHierarchy)
            {
                if (transform.GetChild(selected).GetComponent<Button>())
                    if (transform.GetChild(selected).GetComponent<Button>().onClick !=
                        confirmButton.GetComponent<Button>().onClick)
                    {
                        Debug.Log(currentSelected + " yay " + selected + " " + gameObject.name + " ChildCount " + transform.childCount);
                        if (currentSelected != selected)
                        {
                            if (lobbyMenu) lobbyMenu.LoadTopics(selected);

                            currentSelected = selected;
                            //Debug.Log("yay reassign : " + selected);
                            confirmButton.GetComponent<Button>().onClick = transform.GetChild(selected).GetComponent<Button>().onClick;
                            if (directSelection)
                            {
                                confirmButton.GetComponent<Button>().onClick.Invoke();
                                //confirmButton.SetActive(true);
                                if (confirmButton.GetComponentInChildren<ButtonControl>(true))
                                    confirmButton.GetComponentInChildren<ButtonControl>(true).triggerAudio();
                                confirmButton.SetActive(false);
                            }
                            if (marker) marker.transform.position = marker.transform.parent.position;
                            if (lobbyMenu)
                            {
                                lobbyMenu.scenarioDetailPanel.gameObject.SetActive(false);
                                lobbyMenu.scenarioDetailPanel.GetComponentsInChildren<Button>().LastOrDefault().interactable = false;
                            }
                            else if (GetComponentInParent<LobbyMenu2>())
                            {
                                GetComponentInParent<LobbyMenu2>().scenarioDetailPanel.gameObject.SetActive(false);
                                GetComponentInParent<LobbyMenu2>().scenarioDetailPanel.GetComponentsInChildren<Button>().LastOrDefault().interactable = false;
                            }
                        }
                    }
            }
            else if (lobbyMenu)
            {
                lobbyMenu.scenarioDetailPanel.SetActive(false);
                lobbyMenu.scenarioDetailPanel.GetComponentsInChildren<Button>().LastOrDefault().interactable = false;
            }
    }

    private void Lerping(float distance, float[] pos, Button btn)
    {
        for (int i = 0; i < pos.Length; i++)
        {
            if (scroll_pos < pos[i] + (distance / 2) && scroll_pos > pos[i] - (distance / 2))
            {
                if (autoFocus)
                    if (enableSwipe)
                        scrollbar.GetComponent<Scrollbar>().value = Mathf.Lerp(scrollbar.GetComponent<Scrollbar>().value, pos[btnNumber], Time.deltaTime);
                    else scrollbar.GetComponent<Scrollbar>().value = pos[btnNumber];
            }
        }

        for (int i = 0; i < btn.transform.parent.transform.childCount; i++)
        {
            btn.transform.name = ".";
        }

    }
    public void WhichBtnClicked(Button btn)
    {
        btn.transform.name = "clicked";
        for (int i = 0; i < btn.transform.parent.transform.childCount; i++)
        {
            if (btn.transform.parent.transform.GetChild(i).transform.name == "clicked")
            {
                btnNumber = i;
                takeTheBtn = btn;
                time = 0;
                scroll_pos = (pos[btnNumber]);
                runIt = true;
            }
        }
    }
    
    public void debugTest(String y) {Debug.Log(y);}

    void OnTriggerEnter(Collider other)
    {
        if (other.tag != "Yubi" || !enableSwipe) return;
        offset = new Vector3(transform.parent.InverseTransformPoint(other.transform.position).x - transform.localPosition.x,
                    transform.parent.InverseTransformPoint(other.transform.position).y - transform.localPosition.y,
                    transform.parent.InverseTransformPoint(other.transform.position).z - transform.localPosition.z);
        collide = true;
    }

    void OnTriggerStay(Collider other)
    {
        if (other.tag != "Yubi" || !enableSwipe) return;
        transform.localPosition = transform.parent.InverseTransformPoint(other.transform.position) - offset;
        if (axisScroll == axis.X)
            transform.localPosition = new Vector3(transform.localPosition.x, 0, 0); 
        else
            transform.localPosition = new Vector3(0, transform.localPosition.y, 0);

    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag != "Yubi" || !enableSwipe) return;
        collide = false;
    }
}