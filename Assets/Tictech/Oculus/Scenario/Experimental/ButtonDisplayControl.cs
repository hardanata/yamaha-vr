﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class ButtonDisplayControl : MonoBehaviour
{
    Image im;
    // Start is called before the first frame update
    void Start()
    {
        im = GetComponent<Image>();
    }

    public void DisplayChange(Sprite _sprite)
    {
        im.sprite = _sprite;
        if (transform.GetChild(0).GetComponentInChildren<Image>()) transform.GetChild(0).GetComponentInChildren<Image>().sprite = _sprite;
    }
}
