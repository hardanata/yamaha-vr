﻿using System;
using System.Collections.Generic;
using MEC;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Tictech.Oculus.Scenario
{
    public class ScenarioToolRotaryMk3 : MonoBehaviour
    {
        public string toolId;
        public Transform attachPoint;
        
        //public Transform rotationReference;
        [Range(-50,50)]
        public float rotateAmount;
        [Header("-20 to 20 for Z axis")]
        public float detachLimit;
        public AxisAlign axis;
       // public bool enableOnInit = false;

        //[SerializeField]
        //private Vector3 _oriRot;

        [Header("Read Only")] [SerializeField, ReadOnly]
        private float _rotateValue;

        [SerializeField, ReadOnly] private Vector3 _realRotation;
        //private bool _completed;

        private CoroutineHandle _rotaryHandle;
        private OVRGrabbable _tool;
        private ScenarioItem _item;
        
        private void Start()
        {
           // enableOnInit = false;
            
            //if(_oriRot == Vector3.zero)
            //    _oriRot = transform.localRotation.eulerAngles;
            //Debug.Log("Original rotation: " + _oriRot);
            _item = GetComponentInParent<ScenarioItem>();
            if (!_item)
                Destroy(gameObject);
        }

        private IEnumerator<float> HandleRotary(Transform rotator)
        {
            //_constraint.AddSource(new ConstraintSource{sourceTransform = rotator, weight = 1});
            //yield return Timing.WaitForOneFrame;
            //_constraint.constraintActive = true;
            transform.LookAt(GetConstraintLookTarget(rotator));
            //GetLookRotation(_oriRot, rotator.position, out var lookRot);
            //transform.localRotation = Quaternion.Euler(lookRot);
            //SetRotationByAxisConstraint(Quaternion.LookRotation(rotator.position - transform.position, Vector3.up), _oriRot);
            yield return Timing.WaitForOneFrame;

            float lastRot = GetCurrentAxisRotation();

            //transform.LookAt(rotator);
            float tempRot = _rotateValue;
            float deltaRot;
            
            while (true)
            {
                if (AxisAlign.Z == axis)
                {
                    /*Quaternion newRotation = Quaternion.LookRotation(-(transform.position-GetConstraintLookTarget(rotator)),Vector3.up);*/
                    transform.LookAt(GetConstraintLookTarget(rotator), transform.position + Vector3.up);
                    /*transform.rotation = Quaternion.Lerp(transform.rotation,
                        Quaternion.LookRotation(-(transform.position - GetConstraintLookTarget(rotator)),
                            Vector3.up),
                        0.5f);*/
                    /*transform.rotation = newRotation;*/
                }
                else
                {
                    transform.LookAt(GetConstraintLookTarget(rotator));
                    print("muter: " + transform.rotation.eulerAngles);
                }

                //GetLookRotation(_oriRot, rotator.position, out lookRot);
                //transform.localRotation = Quaternion.Euler(lookRot);
                //SetRotationByAxisConstraint(Quaternion.LookRotation(rotator.position - transform.position, Vector3.up), _oriRot);

                if (Vector3.Distance(transform.position, rotator.position) > detachLimit)
                    Detach();

                yield return Timing.WaitForOneFrame;
                if (Mathf.Abs(GetCurrentAxisRotation() - lastRot) > 30f)
                {
                    lastRot = GetCurrentAxisRotation();
                    tempRot = _rotateValue;
                }

                deltaRot = GetCurrentAxisRotation() - lastRot;

                if (AxisAlign.Z == axis)
                    _rotateValue = Mathf.Clamp(tempRot + deltaRot, -20, 20);
                else
                    _rotateValue = Mathf.Clamp(tempRot + deltaRot, -50, 50);

                // Debug.Log(startRot + " | " + deltaRot + " | " + _rotateValue);
                if (!RotaryFulfilled) continue;
                Debug.Log("Rotary fulfilled at: " + _rotateValue);
                Detach();
                Complete();
            }
        }

        private void SetRotationByAxisConstraint(Quaternion worldRotation, Vector3 oriRot)
        {
            transform.rotation = worldRotation;
            var look = transform.localRotation.eulerAngles;
            switch (axis)
            {
                case AxisAlign.X:
                    look.y = oriRot.y;
                    look.z = oriRot.z;
                    //lookPos.x = transform.position.x;
                    break;
                case AxisAlign.Y:
                    look.x = oriRot.x;
                    look.z = oriRot.z;
                    //lookPos.y = transform.position.y;
                    break;
                case AxisAlign.Z:
                    look.x = oriRot.x;
                    look.y = oriRot.y;
                    //lookPos.z = transform.position.z;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            transform.localRotation = Quaternion.Euler(look);
        }

        private void GetLookRotation(Vector3 oriRot, Vector3 target, out Vector3 look)
        {
            look = Quaternion.LookRotation(target - transform.position, Vector3.up).eulerAngles;
            // look = WorldToLocalRotation(Quaternion.LookRotation(target - transform.position, Vector3.up)).eulerAngles;
            /*
            //lookPos = rotator.position;
            switch (axis)
            {
                case AxisAlign.X:
                    look.y = oriRot.y;
                    look.z = oriRot.z;
                    //lookPos.x = transform.position.x;
                    break;
                case AxisAlign.Y:
                    look.x = oriRot.x;
                    look.z = oriRot.z;
                    //lookPos.y = transform.position.y;
                    break;
                case AxisAlign.Z:
                    look.x = oriRot.x;
                    look.y = oriRot.y;
                    //lookPos.z = transform.position.z;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }*/
        }

        private Vector3 GetConstraintLookTarget(Transform target)
        {
            var lookPos = target.position;
            switch (axis)
            {
                case AxisAlign.X:
                    lookPos.x = transform.position.x;
                    break;
                case AxisAlign.Y:
                    lookPos.y = transform.position.y;
                    break;
                case AxisAlign.Z:
                    lookPos.z = transform.position.z;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return lookPos;
        }

        private float GetCurrentAxisRotation()
        {
            switch (axis)
            {
                case AxisAlign.X:
                    return transform.eulerAngles.x;
                case AxisAlign.Y:
                    return transform.eulerAngles.y;
                case AxisAlign.Z:
                    return transform.eulerAngles.z;
                default:
                    return 0f;
            }
        }

        private Quaternion WorldToLocalRotation(Quaternion world)
        {
            var parent = transform.parent;
            while (parent != null)
            {
                world *= Quaternion.Inverse(parent.rotation);
                parent = parent.parent;
            }

            return world;
        }

        private Quaternion RealRotation()
        {
            Vector3 angle = transform.eulerAngles;
            float x = angle.x;
            float y = angle.y;
            float z = angle.z;

            if (Vector3.Dot(transform.up, Vector3.up) >= 0f)
            {
                if (angle.x >= 0f && angle.x <= 90f)
                {
                    x = angle.x;
                }

                if (angle.x >= 270f && angle.x <= 360f)
                {
                    x = angle.x - 360f;
                }
            }

            if (Vector3.Dot(transform.up, Vector3.up) < 0f)
            {
                if (angle.x >= 0f && angle.x <= 90f)
                {
                    x = 180 - angle.x;
                }

                if (angle.x >= 270f && angle.x <= 360f)
                {
                    x = 180 - angle.x;
                }
            }

            if (angle.y > 180)
            {
                y = angle.y - 360f;
            }

            if (angle.z > 180)
            {
                z = angle.z - 360f;
            }

            Debug.Log(angle + " :::: " + Mathf.Round(x) + " , " + Mathf.Round(y) + " , " + Mathf.Round(z));
            return Quaternion.Euler(x, y, z);
        }

        private void Detach()
        {
            //_constraint.constraintActive = false;
            //_constraint.RemoveSource(0);
            _tool.GetComponent<ScenarioTool>().IsPaused = false;
            _tool.overrideTransform = null;
            Timing.KillCoroutines(_rotaryHandle);
        }

        private void Complete()
        {
            var item = GetComponentInParent<ScenarioItem>();
            var point = item.GetToolPoint(toolId);
           
            Debug.Log("item : " + item + "point :" + point);
            if (point == null) return;
            //if (enableOnInit)
            //{
            //    var collider = transform.parent.gameObject;
            //    ScenarioGrabbable grabb;
            //    /*if (!collider.GetComponent<ScenarioGrabbable>())
            //   {
            //        grabb = collider.AddComponent<ScenarioGrabbable>();
            //    }
            //    else
            //    {
            //        grabb = collider.GetComponent<ScenarioGrabbable>();
            //    }*/
            //    transform.parent.gameObject.AddComponent<ScenarioGrabbable>();
            //    
            //    transform.parent.GetComponent<ScenarioGrabbable>().memasukidengansepenuhhati();
            //    Debug.Log("item : " + item + "point :" + point);
            //    Timing.RunCoroutine(item.CompleteTool(point, 0f, enableOnInit));
            //}
            //else
            //{
                Timing.RunCoroutine(item.CompleteTool(point, 0f));
            //}

            
            //_completed = true;
            Destroy(gameObject);
        }

        private void OnTriggerEnter(Collider other)
        {

            if (RotaryFulfilled || _rotaryHandle.IsRunning || (_item.Grabbable && _item.currentState == ScenarioItem.State.Unchanged))
            {
                Debug.Log("Rotary Debug : " + RotaryFulfilled + " " + _rotaryHandle.IsRunning + " " + _item.Grabbable + "  " + _item.currentState);
                return;
            }

            var tool = other.GetComponent<ScenarioTool>();
            if (tool== null) return;
            if (!other.CompareTag("Scenario Tool") || tool.profile.toolName != toolId || tool.IsPaused) return;

            tool.IsPaused = true;
            _tool = other.GetComponent<ScenarioGrabbable>();
            if (!_tool.grabbedBy)
                return;
            _tool.overrideTransform = attachPoint;
            _tool.overrideAxis = OVRGrabbable.AxisAlign.None;
            _tool.onGrabRelease.AddListener(Detach);
            _rotaryHandle = Timing.RunCoroutine(HandleRotary(_tool.grabbedBy.transform).CancelWith(_tool));
        }

        private bool RotaryFulfilled => rotateAmount > 0f ? _rotateValue > rotateAmount : _rotateValue < rotateAmount;
    }
}