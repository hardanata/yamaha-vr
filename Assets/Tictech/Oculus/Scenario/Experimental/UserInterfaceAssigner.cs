﻿using Tictech.Oculus.Scenario;
using UnityEditor;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections.Generic;

public class UserInterfaceAssigner : MonoBehaviour
{
#if UNITY_EDITOR
    [Header("User Interface")]
    public TeleportScaleCanvas menuCanvas;
    public TeleportScaleCanvas completionCanvas;
    public TextMeshProUGUI stepTitleText;
    public TextMeshProUGUI stepDetailText;
    public RatioVideoController videoController;
    public Button nextButton;
    public GameObject objectivePanel;
    public TransformCall partName1, partName2;

    [Header("Finish Interface")]
    public TextMeshProUGUI timeText;
    public TextMeshProUGUI scoreText;
    public TeleportScaleCanvas finishCanvas;

    [Header("Button Function")]
    public Button completeButton;
    public Button exitButton;
    public Button exitButtonFinish;
    public string exitScene = "Garage Extended STG";
    public Button startSessionButton;
    public Button nextStepButton;

    [Header("Info Panel")]
    public Image scenarioImage1;
    public Image scenarioImage2;
    public TextMeshProUGUI scenarioTitle1, scenarioTitle2;
    public TextMeshProUGUI scenarioDetail1, scenarioDetail2;
    public Image subScenarioImage1, subScenarioImage2;
    public GameObject tagPanel1, tagPanel2;
    public TextMeshProUGUI subScenarioTitle1, subScenarioTitle2;
    public TextMeshProUGUI subScenarioDetail1, subScenarioDetail2;
    public TextMeshProUGUI endText;

    public void Assign()
    {
        ScenarioManager scenarioManager = transform.GetComponentInParent<ScenarioManager>();
        if (scenarioManager)
        {
            if (scenarioManager.menuCanvas) scenarioManager.menuCanvas.gameObject.SetActive(false);
            if (scenarioManager.completionCanvas) scenarioManager.completionCanvas.gameObject.SetActive(false);
            if (scenarioManager.finishCanvas) scenarioManager.finishCanvas.gameObject.SetActive(false);
            List<TransformCall> tc = new List<TransformCall>();
            tc.AddRange(transform.root.GetComponentsInChildren<TransformCall>());
            foreach (var item in tc)
            {
                if (item != partName1 && item != partName2)
                {
                    item.gameObject.SetActive(false);
                }
            }

            scenarioManager.menuCanvas = menuCanvas;
            scenarioManager.menuCanvas.gameObject.SetActive(true);
            scenarioManager.completionCanvas = completionCanvas;
            scenarioManager.completionCanvas.gameObject.SetActive(true);
            scenarioManager.stepTitleText = stepTitleText;
            scenarioManager.stepDetailText = stepDetailText;
            scenarioManager.videoController = videoController;
            scenarioManager.nextButton = nextButton;
            scenarioManager.objectivePanel = objectivePanel;
            scenarioManager.timeText = timeText;
            scenarioManager.scoreText = scoreText;
            scenarioManager.finishCanvas = finishCanvas;
            scenarioManager.finishCanvas.gameObject.SetActive(true);

            scenarioManager.scenarioImage1 = scenarioImage1;
            scenarioManager.scenarioImage2 = scenarioImage2;
            scenarioManager.scenarioTitle1 = scenarioTitle1;
            scenarioManager.scenarioTitle2 = scenarioTitle2;
            scenarioManager.scenarioDetail1 = scenarioDetail1;
            scenarioManager.scenarioDetail2 = scenarioDetail2;
            scenarioManager.subScenarioImage1 = subScenarioImage1;
            scenarioManager.subScenarioImage2 = subScenarioImage2;
            scenarioManager.subScenarioTitle1 = subScenarioTitle1;
            scenarioManager.subScenarioTitle2 = subScenarioTitle2;
            scenarioManager.subScenarioDetail1 = subScenarioDetail1;
            scenarioManager.subScenarioDetail2 = subScenarioDetail2;
            scenarioManager.endText = endText;
            scenarioManager.tagPanel1 = tagPanel1;
            scenarioManager.tagPanel2 = tagPanel2;

            UnityEditor.Events.UnityEventTools.RemovePersistentListener(completeButton.onClick, scenarioManager.CompleteStep);
            UnityEditor.Events.UnityEventTools.AddPersistentListener(completeButton.onClick, scenarioManager.CompleteStep);
            bool eventTest = false;
            eventTest = CheckListener(eventTest, exitButton, "LoadScene");
            if (eventTest == false)
                UnityEditor.Events.UnityEventTools.AddStringPersistentListener(exitButton.onClick, scenarioManager.LoadScene, exitScene);
            UnityEditor.Events.UnityEventTools.RemovePersistentListener(startSessionButton.onClick, scenarioManager.StartSession);
            UnityEditor.Events.UnityEventTools.AddPersistentListener(startSessionButton.onClick, scenarioManager.StartSession);
            UnityEditor.Events.UnityEventTools.RemovePersistentListener(nextStepButton.onClick, scenarioManager.NextStep);
            UnityEditor.Events.UnityEventTools.AddPersistentListener(nextStepButton.onClick, scenarioManager.NextStep);
            eventTest = CheckListener(eventTest, exitButtonFinish, "LoadScene");
            if (eventTest == false)
                UnityEditor.Events.UnityEventTools.AddStringPersistentListener(exitButtonFinish.onClick, scenarioManager.LoadScene, exitScene);

            EditorUtility.SetDirty(scenarioManager);
            EditorUtility.SetDirty(completeButton);
            EditorUtility.SetDirty(exitButton);
            EditorUtility.SetDirty(exitButtonFinish);
            EditorUtility.SetDirty(startSessionButton);
            EditorUtility.SetDirty(nextStepButton);

            
            endText.text = "Selamat! Anda berhasil menyelesaikan subskenario " + scenarioManager.subScenarioName + ".";

            //EditorUtility.SetDirty(scenarioImage1);
            //EditorUtility.SetDirty(scenarioImage2);
            //EditorUtility.SetDirty(scenarioTitle1);
            //EditorUtility.SetDirty(scenarioTitle2);
            //EditorUtility.SetDirty(scenarioDetail1);
            //EditorUtility.SetDirty(subScenarioDetail2);
            //EditorUtility.SetDirty(subScenarioImage1);
            //EditorUtility.SetDirty(subScenarioImage2);
            //EditorUtility.SetDirty(subScenarioTitle1);
            //EditorUtility.SetDirty(subScenarioTitle2);
            //EditorUtility.SetDirty(subScenarioDetail1);
            //EditorUtility.SetDirty(subScenarioDetail2);
            //EditorUtility.SetDirty(endText);

            if (EditorUtility.DisplayDialog("Sukses Om", "Scenario Manager telah diassign. \nMohon cek kembali.", "OK"))
            {
                DestroyImmediate(this);
            }
        }
        else
        if (EditorUtility.DisplayDialog("Anda Belum Beruntung", "Scenario Manager tidak ditemukan.", "Coba Lagi"))
        {

        }
    }

    public bool CheckListener(bool eventTest, Button b, string sName)
    {
        eventTest = false;
        bool test = false;
        for (int i = 0; i < b.onClick.GetPersistentEventCount(); i++)
        {
            if (b.onClick.GetPersistentMethodName(i) == sName)
            {
                UnityEditor.Events.UnityEventTools.RemovePersistentListener(b.onClick, i);
                //test = true;
            }
        }
        eventTest = test;
        return eventTest;
    }
#endif
}

