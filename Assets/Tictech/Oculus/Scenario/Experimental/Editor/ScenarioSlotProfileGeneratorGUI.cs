﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(ScenarioSlotProfileGenerator))]
public class ScenarioSlotProfileGeneratorGUI : Editor
{
    public override void OnInspectorGUI()
    {

        DrawDefaultInspector();
        ScenarioSlotProfileGenerator myScript = (ScenarioSlotProfileGenerator)target;


        myScript.machineName = (ScenarioSlotProfileGenerator.MachineName)EditorGUILayout.EnumPopup("Machine Name", myScript.machineName);
        if (myScript.machineName == (ScenarioSlotProfileGenerator.MachineName.CUSTOM))
        {
            myScript.customMachineName = EditorGUILayout.TextField("", myScript.customMachineName);
        }
        myScript.fileName = EditorGUILayout.TextField("File Name", myScript.fileName);
        myScript.slotId = EditorGUILayout.TextField("Slot ID", myScript.slotId);
        myScript.isMultipleMesh = EditorGUILayout.Toggle("Is Multiple Mesh", myScript.isMultipleMesh);
        if (GUILayout.Button("Generate Profile"))
        {
            myScript._path = EditorUtility.OpenFolderPanel("Select File Location", "Assets/Petrokimia/", "");
            Debug.Log(myScript._path);
            myScript.GenerateButton();
        }
    }
}