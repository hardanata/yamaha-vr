﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(UserInterfaceAssigner)), CanEditMultipleObjects]
public class Assigner : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        UserInterfaceAssigner myScript = (UserInterfaceAssigner)target;
        if (GUILayout.Button("Assign UI"))
        {
            myScript.Assign();
        }
    }
}