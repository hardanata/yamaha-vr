﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;
using Tictech.Oculus.Scenario;
using Tictech.Utilities.Audio;

public class lockMovement : MonoBehaviour
{
    public int initialThreshold = 0;
    public bool lockRotation = false, lockPosition = false;
    public UnityEvent OnChangedThresholdToZero;
    /*public Vector3 minPosition, maxPosition;
    public Quaternion minRotation, maxRotation;*/
    private Quaternion rotate;
    private Vector3 pos;
    [Header("Audio Routine")]
    public AudioRoutine movingAudio;
    public float distance;
    // Start is called before the first frame update
    void Start()
    {
        newPosition();
        newRotation();
    }
    
    public void changeThreshold(int threshold)
    {
        initialThreshold += threshold;
        if(initialThreshold == 0) OnChangedThresholdToZero?.Invoke();
    }

    // Update is called once per frame
    void Update()
    {
        if (initialThreshold != 0)
        {
            if (transform.rotation != rotate)
                transform.rotation = rotate;
            if (transform.position != pos)
                transform.position = pos;
            return;
        }
        if (lockRotation)
        {
            if (transform.rotation != rotate)
            {
                transform.rotation = rotate;
            }
        }
        
        if (lockPosition)
        {
            if (transform.position != pos)
            {
                transform.position = pos;
            }
        }

        if (movingAudio)
        {
            if (Vector3.Distance(pos, transform.position) > distance)
            {
                if (!movingAudio._playHandle.IsRunning)
                {
                    movingAudio.Play();
                    newPosition();
                    newRotation();
                }
            }
            else movingAudio.Stop();
        }
    }


    public void newPosition()
    {
        pos = transform.position;
    }

    public void newRotation()
    {
        rotate = transform.rotation;
    }
}
