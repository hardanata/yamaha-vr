﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnEditorScaler : MonoBehaviour
{
    public Vector3 scale;
    // Start is called before the first frame update
    void Start()
    {
#if UNITY_EDITOR

        transform.localScale = scale;

#endif
    }

}
