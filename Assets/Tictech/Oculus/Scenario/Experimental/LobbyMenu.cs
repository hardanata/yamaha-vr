﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using MEC;
using Tictech.EnterpriseUniversity;
using Tictech.Oculus.Scenario;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LobbyMenu : MonoBehaviour
{
    public Transform startingPoint;
    //public LobbyScenarioData scenarioData;

    [Header("Scenario Detail")]
    public GameObject scenarioDetailPanel;
    public TextMeshProUGUI scenarioTypeText;
    public TextMeshProUGUI scenarioTitleText;
    public TextMeshProUGUI scenarioDetailText;
    public Image scenarioPreviewImage;


    [Header("UI Content")]
    public GameObject topicPrefab;
    public GameObject tagPrefab;
    public GameObject scenarioPrefab;
    public Transform topicContainer;
    public Transform tagContainer;
    public Transform scenarioContainer;

    private List<GameObject> topics;
    private Dictionary<string, GameObject> tags;
    private List<GameObject> scenarios;

#if UNITY_EDITOR
    [Header("Debug")] 
    public bool runDebugSequence;
    public int debugTopicIndex;
    public string debugTopicId;
    public int debugScenarioIndex;

    IEnumerator<float> DebugSequence()
    {
        Debug.Log("Started lobby debug sequence");
        yield return Timing.WaitForSeconds(2f);
        SelectTopic(topics[debugTopicIndex]);
        yield return Timing.WaitForSeconds(2f);
        LoadTags(ScenarioLoader.GetTopic(debugTopicId));
        yield return Timing.WaitForSeconds(2f);
        LoadScenario(ScenarioLoader.GetSelectedTopic().scenarios[debugScenarioIndex]);
        //FindObjectOfType<InScenePrefabToggle>().StartCoroutine("ZeroIndex");
        yield return Timing.WaitForSeconds(2f);
        LoadScenarioScene();
    }
#endif

    private void Start()
    {
        //QualitySettings.SetQualityLevel(0, true);
        topics = new List<GameObject>();
        tags = new Dictionary<string, GameObject>();
        scenarios = new List<GameObject>();

        GameObject.FindWithTag("Player").transform.position = startingPoint.position;
        
        LoadTopics();

#if UNITY_EDITOR
        if(runDebugSequence)
            Timing.RunCoroutine(DebugSequence());
#endif
    }

    public void LoadTopics()
    {
        foreach(var t in topics)
        {
            Destroy(t);
        }
        topics.Clear();

        foreach(var e in ScenarioLoader.GetAll().Values)
        {
            var item = Instantiate(topicPrefab, topicContainer);
            item.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = e.title;
            item.transform.GetChild(0).GetComponent<Image>().sprite = e.preview;
            //item.transform.GetChild(3).GetComponent<TextMeshProUGUI>().text = e.summary;
            item.GetComponentInChildren<Button>().onClick.AddListener(delegate
            {
                SelectTopic(item);
                LoadTags(e);
            });
            topics.Add(item);
        }
        SelectTopic(topics[0]);
        LoadTags(ScenarioLoader.GetAll().Values.First());
    }

    private void SelectTopic(GameObject item)
    {
        foreach (var e in topics)
        {
            e.GetComponentInChildren<Image>().color = e == item ? Color.gray : Color.black;
        }
    }

    private void LoadTags(ScenarioTopic data)
    {
        ScenarioLoader.SelectedTopic = data.title;
        foreach(var t in tags.Values)
        {
            Destroy(t);
        }
        tags.Clear();

        foreach(var s in data.scenarios)
        {
            foreach(var t in s.tags)
            {
                var key = t.ToUpper();
                if(tags.ContainsKey(key))
                    continue;
                
                tags.Add(key, Instantiate(tagPrefab, tagContainer));
                tags[key].GetComponentInChildren<TextMeshProUGUI>().text = key;
                tags[key].GetComponentInChildren<Button>().onClick.AddListener(delegate
                {
                    LoadScenarios(key);
                });
            }
        }
        LoadScenarios(data.scenarios[0].tags[0].ToUpper());
    }

    private void LoadScenarios(string scenarioTag)
    {
        scenarioDetailPanel.SetActive(false);
        foreach(var e in scenarios)
        {
            Destroy(e);
        }
        scenarios.Clear();

        foreach(var e in ScenarioLoader.GetSelectedTopic().scenarios)
        {
            if(e.exclude)
                continue;
            
            if(e.tags.Any(t => t.ToUpper() == scenarioTag))
            {
                var item = Instantiate(scenarioPrefab, scenarioContainer);
                item.GetComponentInChildren<TextMeshProUGUI>().text = e.title;
                item.GetComponentInChildren<Button>().onClick.AddListener(delegate
                {
                    LoadScenario(e);
                });
                scenarios.Add(item);
            }
        }
    }

    private void LoadScenario(ScenarioData data)
    {
        ScenarioLoader.SelectedScenario = data.title;

        scenarioTypeText.text = data.type.ToUpper();
        scenarioTitleText.text = data.title;
        scenarioDetailText.text = data.detail;
        scenarioPreviewImage.sprite = data.preview;
        scenarioDetailPanel.SetActive(true);
    }
    
    private void LoadScene(string sname)
    {
        SceneManager.LoadSceneAsync(sname, LoadSceneMode.Single);
    }

    private void LoadScenarioScene()
    {
        var sceneName = ScenarioLoader.GetSelectedScenario().sceneName;
        if (!string.IsNullOrEmpty(sceneName))
            LoadScene(sceneName);
        else
        {
            var path = "Scenarios/" + ScenarioLoader.SelectedScenario + "/" + ScenarioLoader.GetSelectedScenario().prefabName;
            //Debug.Log("Loading prefab at: " + path + "\r\nResource: " + Resources.Load(path));
            FindObjectOfType<InScenePrefabToggle>().TogglePrefabFromResource(path);
        }
    }
}