﻿using MEC;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace Tictech.Oculus.Scenario
{
    public class ScenarioToolTrigger : MonoBehaviour
    {
        public string toolId;
        public float health;
        public bool hideOnStart;
        public UnityEvent onTriggerEnter;

        private bool _triggered = false;
        private bool _completed = false;
        private Collider _collider;
        private Renderer _renderer;

        private void Start()
        {
            _collider = GetComponent<Collider>();
            _renderer = GetComponent<Renderer>();

            var parent = transform.parent;
            var item = parent.GetComponent<ScenarioItem>();
            item.onStateChanged += Toggle;

            if (!hideOnStart || !parent.GetComponent<OVRGrabbable>())
                return;

            _collider.enabled = false;

            item.onPlaced += delegate
            {
                Toggle(true);
            };
        }

        private void Toggle(bool value)
        {
            if (_renderer != null)
                _renderer.enabled = value;
            _collider.enabled = value;
        }

        private void Update()
        {
            if (!_triggered)
                return;

            health -= Time.deltaTime;
            if (!(health < 0f)) return;
            _triggered = false;
            Trigger();
        }

        public void CheckTrigger(bool value)
        {
            if (_completed || health < 0f)
                return;

            _triggered = value;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (_completed || health < 0f)
                return;

            if (other.CompareTag("Scenario Tool") && other.GetComponent<ScenarioTool>().profile.toolName == toolId)
            {
                _triggered = true;
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (_completed || health < 0f)
                return;

            if (other.CompareTag("Scenario Tool") && other.GetComponent<ScenarioTool>().profile.toolName == toolId)
            {
                _triggered = false;
            }
        }

        [Button("Trigger")]
        public void Trigger()
        {
            if (_completed)
                return;

            var item = GetComponentInParent<ScenarioItem>();
            if (!item.IsActive)
                return;

            _completed = true;

            var point = item.GetToolPoint(toolId);

            if (point == null) return;

            Timing.RunCoroutine(item.CompleteTool(point, 0f));
            onTriggerEnter?.Invoke();
        }
    }
}
