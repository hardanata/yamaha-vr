﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace Tictech.Oculus.Scenario
{
    [CreateAssetMenu(fileName = "New Slot Profile", menuName = "Tictech/VR/Scenario/Slot Profile", order = 1)]
    public class ScenarioSlotProfile : ScriptableObject
    {
        public string slotId;
        public Mesh mesh;
        [ReadOnly]
        public float meshScale = 1;
        [ReadOnly]
        public Vector3 meshScaleV3 = new Vector3(1, 1, 1);

        [Space, ReadOnly]
        public bool isMultipleMesh;
        [ReadOnly]
        public Mesh[] meshList;
        [ReadOnly]
        public Vector3[] meshScaleList;
        [ReadOnly]
        public Vector3[] meshPosList;
        [ReadOnly]
        public Quaternion[] meshRotList;
        [ReadOnly]
        public Vector3 meshParentScale;
        [HideInInspector]
        public bool parentHasMesh;

    }
}