﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Tictech.Oculus.Scenario
{
    public abstract class ScenarioHologramGroupBase : MonoBehaviour
    {

        public abstract void AnimateHologram();
    }
}