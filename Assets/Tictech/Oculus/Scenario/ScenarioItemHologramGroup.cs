﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tictech.Oculus.Scenario
{
    public class ScenarioItemHologramGroup : ScenarioHologramGroupBase
    {
        public bool IsDisableAttacherOnStart;
        public bool IsDisableGrabbableOnStart;
        public bool IsDisableGrabbableOnStartAdamVersion;
        
        private ScenarioItem[] items { get; set; }
        private ScenarioItem currentAnimated;
        private GameObject currentToolHologram;
        private ItemToolInfo currentTool;
        private bool firstRoutine;

        private bool isInit = false;
        //public bool isGrabbableSequence;

        void Start()
        {
            GetChild();

            if (IsDisableAttacherOnStart)
                DisableToolAttacher();
        }

        void DisableToolAttacher()
        {
            foreach (var item in items)
            {
                item.GetComponentInChildren<ScenarioToolRotary>().GetComponent<Collider>().enabled = false;
            }
        }

        public void DisableGrabbable()
        {
            foreach (var item in items)
            {
                if(item.OwnCollider != null)
                    item.OwnCollider.enabled = false;
            }
        }

        public override void AnimateHologram()
        {
            if(!firstRoutine && IsDisableGrabbableOnStartAdamVersion)
                foreach (var item in items)
                {
                    Debug.Log("AdeamVersion");
                    item.GetComponent<Collider>().enabled = false;
                    firstRoutine = true;
                }

            if (IsDisableGrabbableOnStart)
                DisableGrabbable();

            if (items != null && items.Length > 0)
                foreach (var item in items)
                {
                    if (item._slot)
                    {
                        foreach (var tool in item.toolStack)
                        {
                            if (tool.hologramPrefab && !tool.completed)
                            {
                                return;
                            }
                        }
                    }

                    if ((item.currentState == ScenarioItem.State.Unlocked || item.currentState == ScenarioItem.State.Unchanged) && !item._slot)
                    {
                        item.GetComponent<Collider>().enabled = true;
                        if (item.GetComponent<Rigidbody>().isKinematic)
                            item.GetComponent<Rigidbody>().isKinematic = true;

                        if (currentAnimated != null && currentAnimated != item)
                            currentAnimated.SetHologramAnimation(false);

                        currentAnimated = item;
                        item.SetHologramAnimation(true);

                        if (item.currentState == ScenarioItem.State.Unchanged && item._holoList[0].enabled == false)
                        //item.SetItemHologram(true);
                        {
                            foreach (var holo in item._holoList)
                            {
                                holo.enabled = true;
                            }
                            //item._hologram.enabled = true;
                            item.onCompleted += delegate { item.SetItemHologram(false); };

                            if (item.Grabbable)
                            {
                                item.Grabbable.onGrabBegin.AddListener(delegate
                                {
                                    item.SetHologramAnimation(false);
                                    if (item._holoList.Count > 0)
                                        foreach (var holo in item._holoList)
                                        {
                                            holo.enabled = false;
                                        }
                                    //item._hologram.enabled = false;
                                });

                                item.Grabbable.onGrabRelease.AddListener(delegate
                                {
                                    if (item._holoList.Count > 0)
                                        foreach (var holo in item._holoList)
                                        {
                                            holo.enabled = true;
                                        }
                                });
                            }
                        }

                        //if (isGrabbableSequence)
                        //    item.GetComponent<Collider>().enabled = false;
                        break;
                    }
                    else if (item.currentState == ScenarioItem.State.Locked)
                    {
                        if (item.GetComponentInChildren<ScenarioToolRotary>())
                            item.GetComponentInChildren<ScenarioToolRotary>().GetComponent<Collider>().enabled = true;

                        foreach (var tool in item.toolStack)
                        {
                            if (tool.hologramPrefab && !tool.completed)
                            {
                                if (currentTool != tool)
                                {
                                    currentTool = tool;

                                    if (!tool.hologramPoint)
                                        tool.hologramPoint = tool.attachPoint;

                                    currentToolHologram = Instantiate(tool.hologramPrefab);
                                    currentToolHologram.transform.parent = tool.hologramPoint;
                                    currentToolHologram.transform.localPosition = Vector3.zero;
                                    currentToolHologram.transform.localRotation = Quaternion.identity;
                                    tool.hologramObject = currentToolHologram;
                                    tool.onCompleted.AddListener(delegate
                                    {
                                        if (tool.hologramObject)
                                            Destroy(tool.hologramObject);
                                    });
                                    return;
                                }
                                return;
                            }
                        }
                    }
                }
        }

        void GetChild()
        {
            items = GetComponentsInChildren<ScenarioItem>();
            foreach (var item in items)
            {
                item.onCompleted += AnimateHologram;

                item.onSetHologram += AnimateHologram;

                //if (isGrabbableSequence)
                //    item.GetComponent<Collider>().enabled = false;
            }

            var HoloGroup = GetComponentsInChildren<ScenarioItemHologramGroup>();
            var count = 0;
            foreach (var item in HoloGroup)
            {
                count++;
                if (count == 1)
                    continue;
                Debug.LogError("HATI HATI! Terdapat Scenario Item Hologram Group pada " + item.gameObject.name + ". Kemungkinan ngebug, tapi kalau tidak berarti gapapa.");
            }
        }
    }

}
