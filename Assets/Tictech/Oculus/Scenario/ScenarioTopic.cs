﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tictech.Oculus.Scenario
{
    [CreateAssetMenu(fileName = "New Scenario Topic Data", menuName = "Tictech/VR/Scenario/Scenario Topic", order = 0)]
    public class ScenarioTopic : ScriptableObject
    {
        public string title;
        public string sceneName;
        public Sprite preview;
        [TextArea]
        public string summary;
        [TextArea]
        public string detail;
        public List<ScenarioData> scenarios;

        void OnStart()
        {
            if (summary == "")
            {
                summary = "No Detail Added";
            }
        }
    }
}