﻿using UnityEngine;
using Sirenix.OdinInspector;
using System.Collections.Generic;
using System.Linq;

namespace Tictech.Oculus.Scenario
{
    public class ScenarioLoader : MonoBehaviour
    {
        public static ScenarioLoader instance; 

        [SerializeField]
        public ScenarioDataScriptable scenarioData;
        public TagListScriptable tagData;
        [SerializeField]
        private string _selectedTopic;
        public static string SelectedTopic
        {
            get => instance._selectedTopic;
            set => instance._selectedTopic = value;
        }
        [SerializeField]
        private string _selectedScenario;
        public static string SelectedScenario
        {
            get => instance._selectedScenario;
            set => instance._selectedScenario = value;
        }

        [SerializeField]
        private int _selectedId;
        public static int SelectedId
        {
            get => instance._selectedId;
            set => instance._selectedId = value;
        }

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
            /*if(ScenarioManager.Instance)
                ScenarioManager.Instance.generateNextScenarioButton();*/
        }

        public static Dictionary<string, ScenarioTopic> GetAll()
        {
            return instance.scenarioData.data;
        }

        public static ScenarioTopic GetSelectedTopic()
        {
            return GetTopic(SelectedTopic);
        }

        public static ScenarioTopic GetTopic(string id)
        {
            if (!instance.scenarioData.data.ContainsKey(id))
            {
                Debug.LogWarning("No scenario with title " + id);
                return null;
            }
            return instance.scenarioData.data[id];
        }

        public static ScenarioData GetSelectedScenario()
        {
            return GetScenario(SelectedTopic, SelectedScenario);
        }

        public static ScenarioData GetScenario(string topic, string scenario)
        {
            if (!instance.scenarioData.data.ContainsKey(topic))
            {
                Debug.LogWarning("No scenario with title " + topic);
                return null;
            }

            var topicData = instance.scenarioData.data[topic];
            Debug.Log("Selected topic: " + topicData.title);
            for (int i = 0; i < topicData.scenarios.Count; i++)
            {
                Debug.Log("Scenario #" + i + ": " + topicData.scenarios[i].prefabName);
            }
            var data = topicData.scenarios.FirstOrDefault(s => s.prefabName == scenario);
            if(data == null || data.prefabName == "")
            {
                Debug.LogWarning("No scenario with title " + scenario);
            }
            return data;
        }
    }
}
