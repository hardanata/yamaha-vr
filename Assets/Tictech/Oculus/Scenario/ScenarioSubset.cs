﻿using System.Collections;
using System.Collections.Generic;
using Tictech.Oculus.Scenario;
using UnityEngine;

namespace Tictech.Oculus.Scenario
{
    public class ScenarioSubset : MonoBehaviour
    {
        public ScenarioStepInfo[] Sections { get; private set; }
        
        public void Prepare()
        {
            Sections = GetComponentsInChildren<ScenarioStepInfo>();
        }
    }
}
