﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformLerp : MonoBehaviour
{
    public Transform[] segments;
    public Vector3 Rotate = new Vector3(90,0,0);
    
    private Vector3 start;
    private Vector3 end;
    private Transform endTransform;
    
    private void Start()
    {
        if (segments.Length < 2)
        {
            Debug.LogWarning("Need at least 2 segments.");
            enabled = false;
            return;
        }

        endTransform = segments[segments.Length - 1];
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        segments[0].LookAt(segments[1].position);
        segments[0].Rotate(Rotate.x,Rotate.y,Rotate.z);
        start = segments[0].localPosition;
        end = endTransform.localPosition;
        
        Transform objParent = transform.parent;
        for (int i = 1; i < segments.Length - 1; i++)
        {
            if (endTransform.parent != objParent)
            {
                GameObject GO = new GameObject();
                GO.transform.position = endTransform.position;
                GO.transform.parent = objParent;
                end = new Vector3(GO.transform.localPosition.x,GO.transform.localPosition.y,GO.transform.localPosition.z);
                Destroy(GO);
            }
            if (segments[i+1].transform.parent == objParent)
            {
                segments[i].localPosition = Vector3.Lerp(start, end, Mathf.Pow((float)i / segments.Length, 1.2f));
            }
            else
            {
                Vector3 newStart = segments[0].position;
                Vector3 newEnd = endTransform.position;
                segments[i].position = Vector3.Lerp(newStart, newEnd, Mathf.Pow((float)(i) / segments.Length, 1.2f));
            }
            segments[i].LookAt(segments[i + 1].position);
            segments[i].Rotate(Rotate.x,Rotate.y,Rotate.z);
        }
    }
}
