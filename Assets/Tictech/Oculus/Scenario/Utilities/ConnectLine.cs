﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class ConnectLine : MonoBehaviour
{
    private LineRenderer lineRender;
    public Vector3 lineStart, lineEnd;
    public Material lineMaterial;
    public float width = 0.15f;
    public float updateTime = 0.3f;
    public bool offWithDistance;
    public float distanceParameter = 10f;

    void Awake()
    {
        StartCoroutine("drawLine");
    }
    
    IEnumerator drawLine()
    {
        if (lineRender == null)
        {
            lineRender = new GameObject("Line").AddComponent<LineRenderer>();
            lineRender.material = lineMaterial;
            lineRender.positionCount = 2;
            lineRender.startWidth = width;
            lineRender.endWidth = width;
            lineRender.useWorldSpace = false;
            lineRender.numCapVertices = 2;
        }
        else
        {
            if (lineStart != null && lineEnd != null)
            {
                lineRender.SetPosition(0, lineStart);
                lineRender.SetPosition(1, lineEnd);
                if (offWithDistance == true)
                {
                    if (Vector3.Distance(lineStart, lineEnd) < distanceParameter)
                    {
                        lineRender.startWidth = 0;
                        lineRender.endWidth = 0;
                    }
                    else
                    {
                        lineRender.startWidth = width;
                        lineRender.endWidth = width;
                    }
                }
            }
        }
        yield return new WaitForSeconds(updateTime);
        StartCoroutine("drawLine");
    }

    public void setStart(GameObject gameObject)
    {
        lineStart = gameObject.transform.position;
    }

    public void setStart(string tag)
    {
        if (GameObject.FindWithTag((tag)))
        {
            lineStart = GameObject.FindWithTag(tag).transform.position;
        }
    }
    
    public void setEnd(GameObject gameObject)
    {
        lineEnd = gameObject.transform.position;
    }
    
    public void setEnd(string tag)
    {
        if (GameObject.FindWithTag((tag)))
        {
            lineEnd = GameObject.FindWithTag(tag).transform.position;
        }
    }
}
