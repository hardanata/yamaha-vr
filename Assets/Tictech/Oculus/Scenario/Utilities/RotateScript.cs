﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateScript : MonoBehaviour
{
    [SerializeField] Vector3 m_RotateSpeed;
    private Transform ownTransform;

    private void Start()
    {
        ownTransform = transform;
    }

    void Update()
    {
        ownTransform.Rotate(m_RotateSpeed * Time.deltaTime);
    }
}
