﻿using System.Collections;
using System.Collections.Generic;
using MEC;
using Sirenix.OdinInspector;
using UnityEngine;

public class OffsetAnimationGroup : MonoBehaviour
{
    public float startDelay;
    public float iterationRate;

    [Button]
    public void Animate()
    {
        Timing.RunCoroutine(Animating());
    }

    private IEnumerator<float> Animating()
    {
        yield return Timing.WaitForSeconds(startDelay);
        Debug.Log("Animating...");
        foreach (var e in GetComponentsInChildren<OffsetAnimation>())
        {
            e.Animate();
            yield return Timing.WaitForSeconds(iterationRate);
        }
    }
}
