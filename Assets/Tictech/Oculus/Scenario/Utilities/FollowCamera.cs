﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour
{
    public Quaternion rotationMultiplier = new Quaternion(1,1,1,1);
    public float distMoveParameter = 1f;
    private Quaternion targetRotation;
    public float distanceFromCamera = 0.8f;
    private Vector3 targetPosition;
    private bool foundTarget;

    // Start is called before the first frame update
    void Awake()
    {
        foundTarget = false;
        StartCoroutine("findTarget");
    }

    // Update is called once per frame
    void Update()
    {
        if (foundTarget)
        {
            if (Quaternion.Angle(targetRotation,transform.rotation) > 5f || 
                Vector3.Distance(transform.position,targetPosition) > distMoveParameter)
            {
                transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, 0.1f);
                //Debug.Log("rotating");
                transform.position = Vector3.Slerp(transform.position,targetPosition, 0.1f);
                //Debug.Log("moving");
            }
        }
    }

    IEnumerator findTarget()
    {
        GameObject q = new GameObject("canvasIndicator");
        while (FindObjectOfType<OVRScreenFade>())
        {
            yield return new WaitForSeconds(1f);
            Vector3 pos = FindObjectOfType<OVRScreenFade>().transform.position + FindObjectOfType<OVRScreenFade>().transform.forward * distanceFromCamera;
            q.transform.position = new Vector3(pos.x, Camera.main.transform.position.y, pos.z);
            foundTarget = true;
            //position
            targetPosition = q.transform.position;
            //rotation
            q.transform.LookAt(Camera.main.transform);
            targetRotation = q.transform.rotation * rotationMultiplier;
        }
    }
}
