﻿using System.Collections;
using System.Collections.Generic;
using MEC;
using Tictech.Oculus.Scenario;
using UnityEngine;

public class InScenePrefabToggle : MonoBehaviour
{
    public string[] prefabs;
    public int defaultId;

    private GameObject activePrefab;
    private OVRScreenFade _fade;
    private CoroutineHandle _swapping;
    
    private IEnumerator Start()
    {
        _fade = FindObjectOfType<OVRScreenFade>();
        yield return Timing.WaitForSeconds(1f);
        TogglePrefab(defaultId);
    }

    public void TogglePrefab(int id)
    {
        if(!_swapping.IsRunning)
            _swapping = Timing.RunCoroutine(SwappingFromResource(prefabs[id]));
    }

    public void TogglePrefab(GameObject prefab)
    {
        if(!_swapping.IsRunning)
        _swapping = Timing.RunCoroutine(Swapping(prefab));
    }

    public void TogglePrefabFromResource(string path)
    {
        if (!_swapping.IsRunning)
            _swapping = Timing.RunCoroutine(SwappingFromResource(path));
    }

    public IEnumerator ZeroIndex()
    {
        Debug.Log("Reseting ISTP");
        yield return new WaitForSeconds(10f);
        ScenarioManager.Instance.ToggleInGamePrefab(0);
    }

    private IEnumerator<float> SwappingFromResource(string path)
    {
        _fade.FadeOut();
        yield return Timing.WaitForSeconds(2f);
        if(activePrefab)
            Destroy(activePrefab);
        yield return Timing.WaitForSeconds(1f);
        activePrefab = Instantiate(Resources.Load<GameObject>(path));
        yield return Timing.WaitForSeconds(1f);
        Resources.UnloadUnusedAssets();
        _fade.FadeIn();
    }

    private IEnumerator<float> Swapping(GameObject prefab)
    {
        _fade.FadeOut();
        yield return Timing.WaitForSeconds(2f);
        if(activePrefab)
            Destroy(activePrefab);
        yield return Timing.WaitForSeconds(1f);
        activePrefab = Instantiate(prefab);
        yield return Timing.WaitForSeconds(1f);
        Resources.UnloadUnusedAssets();
        _fade.FadeIn();
    }
}
