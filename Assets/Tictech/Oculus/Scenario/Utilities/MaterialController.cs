﻿using System.Collections;
using Sirenix.OdinInspector;
using UnityEngine;

public class MaterialController : MonoBehaviour
{
    public bool offset;
    [ShowIf(nameof(offset))]
    public Vector2 offsetRate;

    private Material material;
    private Vector2 currentOffset;

    private void Start()
    {
        material = GetComponentInChildren<Renderer>().material;
        currentOffset = material.GetVector("Offset");
    }

    private void Update()
    {
        if(offset)
        {
            currentOffset += (offsetRate * Time.deltaTime);
            material.SetVector("Offset", currentOffset);
        }
    }
}
