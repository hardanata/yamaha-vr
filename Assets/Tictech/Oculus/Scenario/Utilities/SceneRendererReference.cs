﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneRendererReference : MonoBehaviour
{
    public static SceneRendererReference Instance;
    
    public Dictionary<string, MeshRenderer> renderers;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
        {
            Destroy(this);
        }
    }

    private void Start()
    {
        renderers = new Dictionary<string, MeshRenderer>();
        var rdr = FindObjectsOfType<MeshRenderer>();
        foreach (var e in rdr)
        {
            if(renderers.ContainsKey(e.name))
                continue;
            renderers.Add(e.name, e);
        }
    }
}
