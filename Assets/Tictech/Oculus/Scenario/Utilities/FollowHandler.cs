﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FollowHandler : MonoBehaviour
{
    public Transform target;
    Rigidbody rb;
    [SerializeField] bool limitMin, limitMax;
    [SerializeField] UnityEvent onLimitReach;
    HingeJoint hingeJoint;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        if (transform.parent.GetComponent<HingeJoint>())
            hingeJoint = transform.parent.GetComponent<HingeJoint>();
    }

    // Update is called once per frame
    void Update()
    {
        rb.position = Vector3.Lerp(rb.position, target.transform.position, 1000f);
        //rb.position = target.position;
    }

    private void OnTriggerStay(Collider other)
    {
        if (hingeJoint == null)
            return;

        Debug.Log("Adam");
        if (other.CompareTag("HingeLimit"))
        {
            if (limitMin && hingeJoint.angle <= hingeJoint.limits.min)
                onLimitReach.Invoke();

            if (limitMax && hingeJoint.angle >= hingeJoint.limits.max)
                onLimitReach.Invoke();
        }
    }

}
