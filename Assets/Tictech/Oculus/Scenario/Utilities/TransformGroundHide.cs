﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class TransformGroundHide : MonoBehaviour
{
    public float resetDuration = 5f;
    public Vector3 hideOffset = new Vector3(0f, -20f, 0f);
    private Vector3 _startPos;
    
    // Start is called before the first frame update
    void Start()
    {
        _startPos = transform.position;
        transform.position = _startPos + hideOffset;
    }

    public void Reset()
    {
        transform.DOMove(_startPos, 5f);
    }
}
