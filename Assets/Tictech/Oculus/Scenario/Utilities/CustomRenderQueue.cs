﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;


public class CustomRenderQueue : MonoBehaviour
{

    public UnityEngine.Rendering.CompareFunction compareRate = UnityEngine.Rendering.CompareFunction.Always;
    
    private void OnEnable()
    {
        foreach (var child in GetComponentsInChildren<Transform>())
        {
            if (child.GetComponent<Image>() != null || child.GetComponent<RawImage>() != null)
            {
                Graphic image = child.GetComponent<Graphic>();
                Material existingGlobalMat = image.materialForRendering;
                Material updatedMaterial = new Material(existingGlobalMat);
                updatedMaterial.SetInt("unity_GUIZTestMode", (int) compareRate);
                image.material = updatedMaterial;
            }
            else if (child.GetComponent<LineRenderer>() != null)
            {
                LineRenderer rawImage = child.GetComponent<LineRenderer>();
                Material existingGlobalMat = rawImage.material;
                Material updatedMaterial = new Material(existingGlobalMat);
                updatedMaterial.SetInt("unity_GUIZTestMode", (int) compareRate + 1);
                rawImage.material = updatedMaterial;
            }
            else if (child.GetComponent<TextMeshProUGUI>() != null)
            {
                child.GetComponent<TextMeshProUGUI>().isOverlay = true;
            }
        }
    }
}