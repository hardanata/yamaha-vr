﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class OffsetAnimation : MonoBehaviour
{
    public Vector3 offset;
    public float duration;
    public bool defaultState;

    private Vector3 startPos;
    private Renderer[] _renderers;

    private void Start()
    {
        startPos = transform.localPosition;
        _renderers = GetComponentsInChildren<Renderer>();
        ToggleRender(defaultState);
    }

    public void Animate()
    {
        transform.localPosition = startPos + offset;
        ToggleRender(true);

        transform.DOLocalMove(startPos, duration);
    }

    public void ToggleRender(bool value)
    {
        if (_renderers == null)
            return;
        
        foreach (var e in _renderers)
        {
            e.enabled = value;
        }
    }
}
