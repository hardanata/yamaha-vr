﻿using System.Collections;
using System.Collections.Generic;
using Tictech.Oculus.Scenario;
using UnityEngine;

public class HandleGrabber : ScenarioGrabbable
{

    public Transform handler;

    public override void GrabEnd(Vector3 linearVelocity, Vector3 angularVelocity)
    {
        base.GrabEnd(Vector3.zero, Vector3.zero);

        transform.position = handler.position;
        transform.rotation = handler.rotation;

        Rigidbody rbhandler = handler.GetComponent<Rigidbody>();
        rbhandler.velocity = Vector3.one;
        rbhandler.angularVelocity = Vector3.one;
    }

    private void Update()
    {
        if (Vector3.Distance(handler.position, transform.position) > 1f)
        {
            grabbedBy.ForceRelease(this);
        }
    }
}
