﻿/*using System.Collections;
using System.Collections.Generic;
using MEC;
using UnityEngine;
using UnityEngine.Rendering.HighDefinition;

[RequireComponent(typeof(DecalProjector))]
public class DecalFade : MonoBehaviour
{
    public float fadeDelay;
    public float fadeTime;
    
    private float currentFade;
    private DecalProjector decal;
    
    private static int DecalCount;

    private void Start()
    {
        DecalCount++;
        decal = GetComponent<DecalProjector>();
        float size = Random.Range(.2f, .5f);
        decal.size = new Vector3(size, size, .5f);
        Timing.RunCoroutine(Fade());
        Debug.Log("Decal count: " + DecalCount);
    }

    IEnumerator<float> Fade()
    {
        currentFade = 0f;
        while (currentFade < 1f)
        {
            currentFade += Time.deltaTime;
            decal.fadeFactor = currentFade;
            yield return Timing.WaitForOneFrame;
        }
        yield return Timing.WaitForSeconds(fadeDelay);
        currentFade = fadeTime;
        while (currentFade > 0f)
        {
            currentFade -= Time.deltaTime;
            decal.fadeFactor = currentFade / fadeTime;
            yield return Timing.WaitForOneFrame;
        }

        if (currentFade < 0)
        {
            DecalCount--;
            Destroy(gameObject);    
        }
    }
}
*/