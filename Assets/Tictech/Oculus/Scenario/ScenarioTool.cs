﻿using System.Collections.Generic;
using DG.Tweening;
using MEC;
using UnityEngine;

namespace Tictech.Oculus.Scenario
{
    public class ScenarioTool : MonoBehaviour
    {
        public ScenarioToolProfile profile;
        // public Transform attachPoint;
        // public AxisAlign alignAxis;
        public bool attachOnValid;

        private Collider[] _colliders;
        private OVRGrabbable _grabbable;
        private ScenarioItem _item;
        public bool IsPaused { get; set; }

        private void Start()
        {            
            gameObject.layer = LayerMask.NameToLayer("Interactable");
            gameObject.tag = "Scenario Tool";
            _grabbable = GetComponent<OVRGrabbable>();
            _colliders = GetComponentsInChildren<Collider>();
        }
/*
        private void OnTriggerEnter(Collider other)
        {
            if (!attachOnValid || IsPaused)
                return;

            if (!other.CompareTag("Scenario Item")) return;
            var item = other.GetComponent<ScenarioItem>();
            var point = item.GetToolPoint(profile.toolName);
            if (point == null) return;

            if (_item)
                _item.ToggleHighlight(false);

            _item = item;
            _item.ToggleHighlight(true);
            
            _grabbable.grabbedBy.GrabEnd();
            other.GetComponent<Animator>().SetTrigger(profile.toolName);
            Timing.RunCoroutine(Sleep(item, point));
        }

        private void OnTriggerExit(Collider other)
        {
            if (!other.CompareTag("Scenario Item") || IsPaused)
                return;

            var nItem = other.GetComponent<ScenarioItem>();
            if (nItem != _item) return;

            _item.ToggleHighlight(false);
            _item = null;
        }
*/
        public void PlayAudio(string key)
        {
            GetComponent<AudioSource>().PlayOneShot(ScenarioManager.Instance.audioReference.GetClip(key));
        }

        public void PlayAudioArray(string key)
        {
            GetComponent<AudioSource>().PlayOneShot(ScenarioManager.Instance.audioReference.GetClipArray(key));
        }

        public void SetPause(bool value)
        {
            if(!value)
                transform.SetParent(null);
            foreach (var e in _colliders)
            {
                e.enabled = !value;
            }
            IsPaused = value;
        }
        
        void OnDrawGizmos()
        {
            // Draws the Light bulb icon at position of the object.
            // Because we draw it inside OnDrawGizmos the icon is also pickable
            // in the scene view.

            Gizmos.DrawIcon(transform.position, "scenario tool gizmo.png", true);
        }
    }
}
