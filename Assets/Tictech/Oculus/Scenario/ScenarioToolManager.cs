﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Tictech.Oculus.Scenario
{
    public class ScenarioToolManager : SerializedMonoBehaviour
    {
        public static ScenarioToolManager instance;

        public Dictionary<string, GameObject> toolHints;

        private void Awake()
        {
            instance = this;
        }
    }
}