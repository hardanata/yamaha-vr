﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tictech.Oculus.Scenario
{
    public class ScenarioSlotHologramGroup : ScenarioHologramGroupBase
    {
        private ScenarioSlot[] slots { get; set; }
        void Start()
        {
            slots = GetComponentsInChildren<ScenarioSlot>();
            foreach (var slot in slots)
            {
                slot.onSlotCompleted.AddListener(delegate { AnimateHologram(); });
            }

        }

        public override void AnimateHologram()
        {
            foreach (var slot in slots)
            {
                if (!slot.completed)
                {
                    if (!slot._hologram)
                        slot.SetHologram(true);

                    slot.SetHologramAnimation(true);
                    slot.EnableHolo();
                    break;
                }
            }
        }
    }
}