﻿    using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using Sirenix.Utilities;
using Tictech.Oculus.Scenario;
using UnityEngine;
    using UnityEngine.Events;

    public class ScenarioItemRoller : ScenarioItem
{
    public Vector3 velocityRate = Vector3.down;
    public double VelocityRate;
    public double AngularVelocity;
    public List<Transform> IndicatorPoints;
    public List<GameObject> marker;
    public UnityEvent onComplete;
    // Start is called before the first frame update
    void Start()
    {
        marker.Clear();
        if (IndicatorPoints.IsNullOrEmpty())
            IndicatorPoints.Add(this.transform);
        foreach (var v in IndicatorPoints)
        {
            marker.Add(Instantiate(ScenarioManager.Instance.slotMarkerPrefab, v ? v.position : transform.position, Quaternion.identity));
        }
        StartCoroutine("RollingCheck");
    }

    IEnumerator RollingCheck()
    {
        while (true)
        {
            yield return new WaitForSeconds(3f);
            if (GetComponentInChildren<Rigidbody>())
            {
                Rigidbody rBody = GetComponentInChildren<Rigidbody>();
                Debug.Log(rBody);
                VelocityRate = (Math.Sqrt((double) (Pow(velocityRate.x) + Pow(velocityRate.y) + Pow(velocityRate.z))));
                AngularVelocity = (Math.Sqrt((double) (Pow(rBody.angularVelocity.x) + Pow(rBody.angularVelocity.y) + Pow(rBody.angularVelocity.z))));
                if (AngularVelocity >= VelocityRate)
                {
                    foreach (var v in marker)
                    {
                        Destroy(v);
                    }
                    marker.Clear();
                    onComplete?.Invoke();
                    ForceComplete();
                }
            }
        }
    }

    double Pow(float i)
    {
        double result = Math.Pow(i, 2);
        return result;
    }
}
