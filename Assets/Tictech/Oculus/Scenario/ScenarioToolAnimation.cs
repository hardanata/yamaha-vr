﻿using System.Collections.Generic;
using DG.Tweening;
using MEC;
using UnityEngine;
using UnityEngine.Events;

namespace Tictech.Oculus.Scenario
{
    public class ScenarioToolAnimation : MonoBehaviour
    {
        public string toolId;
        public Transform attachPoint;
        public Vector3 offset;
        public float animationLength;
        public bool isASequence;
        public bool detachOnComplete;
        //public bool enableOnInit = false;

        private Vector3 _toolStartPos;
        private Vector3 _toolStartRot;
        private UnityAction _onCompleted;
        private bool _completed;

        private CoroutineHandle _animationHandle;
        private Transform _tool;
        private ScenarioItem _item;

 /*       public string grabAudio;
        private AudioSource audioSource;*/

        private void Start()
        {
            _item = GetComponentInParent<ScenarioItem>();
            if(!_item)
                Destroy(gameObject);
            /*if (!string.IsNullOrEmpty(grabAudio))
            {
                audioSource = gameObject.AddComponent<AudioSource>();
                audioSource.playOnAwake = false;
                audioSource.clip = ScenarioManager.Instance.audioReference.clips[grabAudio];
            }*/
        }
/*
        public void playAudio(bool Loop)
        {
            if (!audioSource) return;
            audioSource.loop = Loop;
            if(audioSource.isPlaying == false) audioSource.Play();
        }

        public void stopAudio()
        {
            if (!audioSource) return;
            audioSource.Stop();
        }
*/
        // Update is called once per frame
        private IEnumerator<float> HandleAnimation()
        {
            var item = GetComponentInParent<ScenarioItem>();
            var toolPoint = item.GetToolPoint(toolId);
            if(toolPoint == null)
                yield break;
            
            Timing.RunCoroutine(item.CompleteTool(toolPoint, animationLength));
/*            if (enableOnInit)
            {
<<<<<<< HEAD
                var collider = transform.parent.gameObject;
                var grabb = collider.AddComponent<ScenarioGrabbable>();
                grabb.enabled = true;
                grabb.memasukidengansepenuhhati();
                Debug.Log("item : " + item + "point :" + toolPoint);
=======
                //var collider = transform.parent.gameObject;
                //var grabb = collider.AddComponent<ScenarioGrabbable>();
                //grabb.enabled = true;
                //grabb.memasukidengansepenuhhati();
               // Debug.Log("item : " + item + "point :" + toolPoint);
>>>>>>> 096b0f8d904bc513ea7188cd707c915af5d0e53f
                Timing.RunCoroutine(item.CompleteTool(toolPoint, animationLength));
            }
            else
            {
            }*/
            
            yield return Timing.WaitForOneFrame;
            _toolStartPos = _tool.position;
            _toolStartRot = _tool.rotation.eulerAngles;
            _tool.parent = attachPoint;
            _tool.DOLocalMove(Vector3.zero + offset, .4f);
            _tool.DOLocalRotate(Vector3.zero, .4f);

            yield return Timing.WaitForOneFrame;

            Destroy(GetComponent<Collider>());
            GetComponentInParent<Animator>().SetTrigger(toolId);
            if (animationLength > 0f)
                yield return Timing.WaitForSeconds(animationLength);
            else
                yield return Timing.WaitForSeconds(.5f);

            _completed = true;

            if (!isASequence || detachOnComplete)
                Timing.RunCoroutine(Detaching());
        }
        
        private IEnumerator<float> CompleteAnimation()
        {
            GetComponentInParent<Animator>().SetTrigger(toolId + "Completed");
            yield return Timing.WaitForSeconds(animationLength);
            Timing.RunCoroutine(Detaching());
        }

        public void Detach()
        {
            Timing.RunCoroutine(Detaching());
        }

        private IEnumerator<float> Detaching()
        {
            Timing.KillCoroutines(_animationHandle);
            if(_tool)
            _tool.parent = transform.root;
            _tool.DOMove(_toolStartPos, .4f);
            _tool.DORotate(_toolStartRot, .4f);
            yield return Timing.WaitForSeconds(.4f);
            if(_tool)
            _tool.GetComponentInChildren<ScenarioTool>().SetPause(false);
            Destroy(gameObject);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (_completed || _animationHandle.IsRunning || _item)
                if (_item) { if (_item.Grabbable && _item.currentState == ScenarioItem.State.Unchanged) return; }
                else return;
            
            if (other.GetComponent<ScenarioTool>() == null) return; 
            if (!other.CompareTag("Scenario Tool") || other.GetComponent<ScenarioTool>().profile.toolName != toolId)
                return;
            
            var item = GetComponentInParent<ScenarioItem>();
            var point = item.GetToolPoint(toolId);

            _tool = other.transform;
            
            if (point.hologramObject != null && _tool.GetComponent<OVRGrabbable>().isGrabbed)
                Destroy(point.hologramObject);
            
            _tool.GetComponent<ScenarioGrabbable>().grabbedBy.GrabEnd();
            _tool.GetComponentInChildren<ScenarioTool>().SetPause(true);
            _animationHandle = Timing.RunCoroutine(HandleAnimation());

            
        }
    }
}