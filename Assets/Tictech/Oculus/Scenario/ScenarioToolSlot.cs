﻿using MEC;
using Sirenix.OdinInspector;
//using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.Events;

namespace Tictech.Oculus.Scenario
{
    public class ScenarioToolSlot : MonoBehaviour
    {
        public string toolId;
        public bool hideOnStart;
        public UnityEvent onTriggerEnter;
        public Vector3 lookRotation = Vector3.up;

        private bool _triggered = false;
        private bool _completed = false;
        private bool attach = false;
        private Collider _collider;
        private GameObject triggerObj;

        private void Start()
        {
            _collider = GetComponent<Collider>();
            
            var parent = transform.parent;
            var item = parent.GetComponent<ScenarioItem>();
            item.onStateChanged += Toggle;
            
            if (!hideOnStart || !parent.GetComponent<OVRGrabbable>())
                return;

            _collider.enabled = false;
            
            item.onPlaced += delegate
            {
                Toggle(true);
            };
        }

        private void Toggle(bool value)
        {
            _collider.enabled = value;
        }

        private void Update()
        {
            if (attach == true)
            {
                triggerObj.transform.localRotation = Quaternion.LookRotation(lookRotation);
            }
            if (!_triggered)
                return;
            
            _triggered = false;
            Trigger();
        }

        public void CheckTrigger(bool value)
        {
            if (_completed)
                return;

            _triggered = value;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (_completed || other.GetComponent<ScenarioTool>() != null || other.GetComponentInParent<ScenarioTool>() == null)
                return;
            //GameObject _tool = other.GetComponentInParent<ScenarioTool>().gameObject;
            if (other.GetComponentInParent<ScenarioTool>().profile.toolName == toolId && other.gameObject.tag == "Scenario Tool")
            {
                //_triggered = true;
                triggerObj = other.gameObject;
                attach = true;
                other.transform.position = transform.position;
                other.transform.localRotation = Quaternion.LookRotation(lookRotation);
                if (other.GetComponent<BoxCollider>())
                {
                    //other.GetComponent<ScenarioGrabbable>();
                    if (other.GetComponent<ScenarioGrabbable>().isGrabbed)
                    {
                        other.GetComponent<ScenarioGrabbable>().grabbedBy.GrabEnd();
                    }
                    other.GetComponent<BoxCollider>().enabled = false;
                    other.transform.parent = transform;
                }

                var item = GetComponentInParent<ScenarioItem>();
                var point = item.GetToolPoint(toolId);
                if (point.hologramObject != null)
                    Destroy(point.hologramObject);

                onTriggerEnter?.Invoke();
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (_completed)
                return;
            
            /*if (other.GetComponentInParent<ScenarioTool>().profile.toolName == toolId && !other.GetComponent<ScenarioTool>())
            {
                _triggered = false;
            }*/
        }

        [Button("Trigger")]
        public void Trigger()
        {
            if (_completed)
                return;
            
            var item = GetComponentInParent<ScenarioItem>();
            if (!item.IsActive)
                return;
            
            _completed = true;

            var point = item.GetToolPoint(toolId);
            
            if (point == null) return;
            
            Timing.RunCoroutine(item.CompleteTool(point, 0f));
            attach = false;
            onTriggerEnter?.Invoke();
        }
    }
}
