﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tictech.Oculus.Scenario
{
    [CreateAssetMenu(fileName = "New Scenario Data", menuName = "Tictech/VR/Scenario/Scenario Data", order = 1)]
    public class ScenarioData : ScriptableObject
    {
        public int trainingId;
        public string title;
        public string type;
        [TextArea]
        public string detail;
        public Sprite preview;
        public Sprite preview2;
        public string sceneName;
        public string prefabName;
        public List<string> tags;
        public bool exclude;
    }
}