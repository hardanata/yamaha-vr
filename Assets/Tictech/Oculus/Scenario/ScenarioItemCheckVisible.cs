﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Sirenix.Utilities;
using Tictech.Oculus.Scenario;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.PlayerLoop;

public class ScenarioItemCheckVisible : ScenarioItem
{
    public float checkTime = 3f;
    public float checkDistance = 1f;
    public bool touchToClear = false;
    private bool touched = false;
    private GameObject Player;
    public List<Transform> IndicatorPoints;
    public List<GameObject> marker;
    public UnityEvent onCompleted; 
    
    // Start is called before the first frame update
    void Start()
    {
        Player = GameObject.FindWithTag("Player");
        marker.Clear();
        if (IndicatorPoints.IsNullOrEmpty())
            IndicatorPoints.Add(this.transform);
        foreach (var v in IndicatorPoints)
        {
            marker.Add(Instantiate(ScenarioManager.Instance.slotMarkerPrefab, v ? v.position : transform.position, Quaternion.identity));
        }
        if (!GetComponent<Collider>())
        {
            touchToClear = false;
        }
        else
        {
            if (touchToClear == true) gameObject.layer = LayerMask.NameToLayer("Water");
        }
        touched = false;
        StartCoroutine("RollingCheck");
    }

    void Update()
    {
        if (Vector3.Distance(Camera.main.transform.position,transform.position) < checkDistance)
        {
            if(checkTime>0)
                if (GetComponent<Renderer>().IsVisibleFrom(Camera.main))
                {
                    if ((touchToClear && touched) || !touchToClear)
                    {
                        checkTime -= Time.deltaTime;
                    }
                }
        }
    }

    IEnumerator RollingCheck()
    {
        while (true)
        {
            yield return new WaitForSeconds(1f);
            if (checkTime <= 0)
            {
                foreach (var v in marker)
                {
                    Destroy(v);
                }

                marker.Clear();
                onCompleted.Invoke();
                ForceComplete();
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (Player.GetComponentsInChildren<Collider>().Contains(other))
        {
            touched = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (Player.GetComponentsInChildren<Collider>().Contains(other))
        {
            touched = false;
        }
    }
}
