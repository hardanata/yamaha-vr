﻿using UnityEngine;

namespace Tictech.Oculus.Scenario
{
    public class ScenarioToolSequenceGenerator : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {
            var sequence = GetComponentsInChildren<ScenarioToolTrigger>();
            var item = GetComponent<ScenarioItem>();
            item.toolStack = new ItemToolInfo[sequence.Length];
            for (int i = 0; i < sequence.Length; i++)
            {
                int index = i;
                item.toolStack[index] = new ItemToolInfo()
                {
                    toolId = sequence[index].toolId,
                    attachPoint = sequence[index].transform
                };
                sequence[index].onTriggerEnter.AddListener(delegate
                {
                    if(index + 1 < sequence.Length)
                        sequence[index + 1].gameObject.SetActive(true);
                    sequence[index].gameObject.SetActive(false);
                });
                sequence[index].gameObject.SetActive(i == 0);
            }

            Destroy(this);
        }
    }
}
