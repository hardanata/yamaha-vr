﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Sirenix.Utilities;
using Tictech.Oculus.Scenario;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.PlayerLoop;
using MEC;

public class ScenarioItemCheckVisible1 : ScenarioItem
{
    public float checkTime = 3f;
    public float checkDistance = 1f;
    public bool touchToClear = false;
    private bool touched = false;
    public bool isAllowCheck = false;
    private GameObject Player;
    public List<Transform> IndicatorPoints;
    public List<GameObject> marker;
    public UnityEvent onCompleted;

    // Start is called before the first frame update
    private void Start()
    {
        base.Start();

        Player = GameObject.FindWithTag("Player");
        marker.Clear();
        if (IndicatorPoints.IsNullOrEmpty())
            IndicatorPoints.Add(this.transform);
        foreach (var v in IndicatorPoints)
        {
            marker.Add(Instantiate(ScenarioManager.Instance.slotMarkerPrefab, v ? v.position : transform.position, Quaternion.identity));
        }
        if (!GetComponent<Collider>())
        {
            touchToClear = false;
        }
        else
        {
            if (touchToClear == true) gameObject.layer = LayerMask.NameToLayer("Water");
        }
        touched = false;
    }

    void Update()
    {
        if (isAllowCheck)
        {
            Debug.Log("distance: " + Vector3.Distance(Camera.main.transform.position, transform.position));
            if (Vector3.Distance(Camera.main.transform.position, transform.position) < checkDistance)
            {
                if (checkTime > 0)
                    if (GetComponent<Renderer>().IsVisibleFrom(Camera.main))
                    {
                        if ((touchToClear && touched) || !touchToClear)
                        {
                            checkTime -= Time.deltaTime;
                        }
                    }
            }

            if (checkTime <= 0)
            {
                foreach (var v in marker)
                {
                    Destroy(v);
                }

                isAllowCheck = false;
                marker.Clear();
                if(GetToolPoint("CheckVisible") != null)
                    Timing.RunCoroutine(CompleteTool(GetToolPoint("CheckVisible"), 0f));
            }
        }
    }

    public void SetisAllowCheck(bool value)
    {
        isAllowCheck = value;
    }


    void OnTriggerEnter(Collider other)
    {
        base.OnTriggerEnter(other);

        if (Player.GetComponentsInChildren<Collider>().Contains(other))
        {
            touched = true;
        }
    }

    void OnTriggerExit(Collider other)
    {

        base.OnTriggerExit(other);

        if (Player.GetComponentsInChildren<Collider>().Contains(other))
        {
            touched = false;
        }
    }
}
