﻿using Sirenix.OdinInspector;
using Tictech.Oculus.Scenario;
using Tictech.Utilities.Audio;
using UnityEngine;

public class ToolTriggerSound : MonoBehaviour
{
    public bool useHaptic;
    [ShowIf(nameof(useHaptic))] public float normalFrequency;
    [ShowIf(nameof(useHaptic))] public float actionFrequency;
    [ShowIf(nameof(useHaptic))] public float normalAmplitude;
    [ShowIf(nameof(useHaptic))] public float actionAmplitude;
    
    [Header("Sound")] 
    public AudioRoutine normalAudio;
    public AudioRoutine actionAudio;

    private bool _switchAction;
    private bool _isAction;
    private bool _isActive;

    private ScenarioTool _tool;
    private ScenarioToolTrigger _trigger;
    private OVRGrabbable _grabbable;
    private OVRInput.Controller _lastController;

    private void Start()
    {
        _tool = GetComponent<ScenarioTool>();

        _grabbable = GetComponent<OVRGrabbable>();
        _grabbable.onTriggerBegin += delegate { Toggle(true); };
        _grabbable.onTriggerRelease += delegate { Toggle(false); };
        _grabbable.onGrabRelease.AddListener(delegate { Toggle(false); });
        
        Toggle(false);
    }

    private void Toggle(bool value)
    {
        _isActive = value;

        if (_isActive)
        {
            _lastController = _grabbable.grabbedBy.controller;
            InvokeRepeating(nameof(Haptic), 0f, 1.9f);
            normalAudio.Play();
            return;
        }

        CancelInvoke();
        OVRInput.SetControllerVibration(0f, 0f, _lastController);
        normalAudio.Stop();
        actionAudio.Stop();
        _trigger = null;
        _isAction = false;
    }

    private void Haptic()
    {
        OVRInput.SetControllerVibration(.5f, 1f, _lastController);
    }

    private void OnTriggerEnter(Collider other)
    {
        var trigger = other.GetComponent<ScenarioToolTrigger>();
        if (!_isActive || !trigger || trigger.toolId != _tool.profile.toolName)
            return;

        _trigger = trigger;
        _trigger.CheckTrigger(true);
        _switchAction = true;
    }

    private void OnTriggerExit(Collider other)
    {
        var trigger = other.GetComponent<ScenarioToolTrigger>();
        if (!_isActive || !trigger || trigger != _trigger)
            return;
        
        _trigger.CheckTrigger(false);
        _trigger = null;
        _switchAction = true;
    }

    private void LateUpdate()
    {
        if (!_isActive)
            return;
        
        if (_switchAction && !_isAction)
        {
            _switchAction = false;
            _isAction = true;
            normalAudio.Stop();
            actionAudio.Play();
        }
        else if (_switchAction && _isAction)
        {
            _switchAction = false;
            _isAction = false;
            normalAudio.Play();
            actionAudio.Stop();
        }
    }
}
