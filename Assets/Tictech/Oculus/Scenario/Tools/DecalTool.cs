﻿/*using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using MEC;
using Sirenix.OdinInspector;
using Tictech.Oculus.Scenario;
using Tictech.Utilities.Audio;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Rendering.HighDefinition;

public class DecalTool : MonoBehaviour
{
    public LayerMask layer;
    public float range;
    public DecalProjector decal;
    public float distanceLimit = 1f;
    public bool triggerTool = false;

    [Header("Sound")] 
    public AudioRoutine normalAudio;
    public AudioRoutine actionAudio;

    private LineRenderer _line;
    private bool _isAction;
    private bool _isActive;
    private Vector3 _endPoint;

    private float _intensityChange;
    private ScenarioTool _tool;
    
    private OVRGrabbable _grabbable;
    private OVRInput.Controller _lastController;
    private bool decalCalled;
    private float vibrateTime;

    private void Start()
    {
        decalCalled = false;
        _tool = GetComponent<ScenarioTool>();
        _line = GetComponentInChildren<LineRenderer>();
        _endPoint = decal.gameObject.GetComponentInParent<DecalTool>().transform.localPosition;
        _grabbable = GetComponent<OVRGrabbable>();
        if (triggerTool == true)
        {
            _grabbable.onTriggerBegin += delegate { Toggle(true); };
            _grabbable.onTriggerRelease += delegate { Toggle(false); };
        }
        
        _grabbable.onGrabRelease.AddListener(delegate { 
            Toggle(false); 
            normalAudio.Stop(); 
            actionAudio.Stop(); 
        });
        decal.gameObject.GetComponentInParent<Collider>().tag = "Scenario Tool";
        Toggle(false);
        //joint
            trigger = false;
            prevState = state;
            setBasic();
            StartCoroutine("StateMachine");
        
    }

    public void ForceToggle()
    {
        if(state == jointState.active)
        Toggle(true);
        new WaitForSeconds(1f);
        Toggle(false);
    }

    private void Toggle(bool value)
    {
        Debug.Log("DecalTool state: " + value);

        _isActive = value;
        if (triggerTool == true)
        {
            if (_isActive)
            {
                _lastController = _grabbable.grabbedBy.controller;
                if (vibrateTime > 0f)
                {
                    InvokeRepeating(nameof(Haptic), 0f, 1.9f);
                    vibrateTime -= Time.deltaTime;
                }
                else
                {
                    OVRInput.SetControllerVibration(0f, 0f, _lastController);
                }
                normalAudio.Play();
                return;
            }

            CancelInvoke();
            OVRInput.SetControllerVibration(0f, 0f, _lastController);
            normalAudio.Stop();
            actionAudio.Stop(); 
        }
        _isAction = false;
        checkDecal();
    }

    private void Haptic()
    {
        OVRInput.SetControllerVibration(.8f, .7f, _lastController);
    }

    

    private void checkDecal()
    {
        if (!_isActive)
        {
            decalCalled = false;
            vibrateTime = 1f;
            if (GetComponentInParent<Collider>() == null) return;
            
            if (Vector3.Distance(transform.position, decal.GetComponentInParent<Collider>().transform.position) > distanceLimit)
            {
                Debug.Log("Detaching ");
                decal.GetComponentInParent<Collider>().transform.parent = transform;
                decal.GetComponentInParent<Collider>().transform.localPosition = Vector3.zero;
                decal.GetComponentInParent<Collider>().enabled = true;
            }
            return;
        }
        
        if (decal != null && decalCalled == false)
        {
            GameObject DecalObj = Instantiate(decal.gameObject, decal.transform.position, decal.transform.rotation);
            DecalObj.SetActive(true);
            decalCalled = true;
            if (decal.GetComponentInParent<Collider>().enabled == false)
            {
                var trigger = decal.GetComponentInParent<ScenarioToolSlot>();
                if(trigger){}
                trigger.Trigger();
                state = jointState.nonActive;
            }
            if (decal.gameObject)
            {
                decal.gameObject.GetComponentInParent<Collider>().enabled = true;
                decal.gameObject.GetComponentInParent<Collider>().transform.parent = transform;
            }
        }
        
        if (!_isAction)
        {
            normalAudio.Stop();
            actionAudio.Play();
            _isAction = true;
        }
        else if (_isAction)
        {
            _isAction = false;
            normalAudio.Play();
            actionAudio.Stop();
        }
    }
    
    public enum jointState
    {
        basic = 0,
        active = 1,
        nonActive = 2
    }

    [Header("Joint")]
    
    public float triggerAngles = 15f;
    public jointState state;
    public List<OVRGrabbable> listGrabbable;
    public List<Collider> listCollider;
    public List<FollowHandler> listHandlers;
    public List<Joint> listJoint;
    public List<Transform> listJointPosition; 
    public UnityEvent OnPumped;

    private jointState prevState;
    private bool trigger;

    // Start is called before the first frame update
    
    void setBasic()
    {
        listGrabbable = GetComponentsInChildren<OVRGrabbable>(true).ToList();
        listCollider = GetComponentsInChildren<Collider>(true).ToList();
        listHandlers = GetComponentsInChildren<FollowHandler>(true).ToList();
        listJoint = GetComponentsInChildren<Joint>(true).ToList();
        foreach (var vPos in listJoint)
        {
            GameObject gObject = new GameObject(null);
            gObject.transform.localPosition = vPos.transform.localPosition;
            gObject.transform.localRotation = vPos.transform.localRotation;
            listJointPosition.Add(gObject.transform);
        }
    }

    void FixedUpdate()
    {
        if (state == jointState.active)
        {
            int indexJoint = 0, indexTrigger = 0;
            foreach (var vJoint in listJoint)
            {
                if (vJoint.GetComponent<HingeJoint>() != null)
                {
                    indexJoint++;
                    HingeJoint hj = vJoint.GetComponent<HingeJoint>();
                    if (hj.motor.targetVelocity > hj.limits.min)
                    {
                        if (Mathf.Abs(hj.angle - hj.limits.min) < triggerAngles)
                        {
                            indexTrigger++;
                        }
                    }
                    else
                    {
                        if (Mathf.Abs(hj.angle - hj.limits.max) < triggerAngles)
                        {
                            indexTrigger++;
                        }
                    }
                }
            }
            if (indexTrigger >= indexJoint && trigger == false)
            {
                trigger = true;
                ForceToggle();
                OnPumped.Invoke();
                Debug.Log("Trigger Joint");
                //state = jointState.nonActive;
            }
            else if (indexTrigger < indexJoint && trigger == true)
            {
                trigger = false;
            }
        }
        //joint
        
            if (triggerTool == true)
            {
                checkDecal();
            }

            if (!decal.transform.IsChildOf(transform))
            {
                state = jointState.active;
            }
        
    }

    IEnumerator StateMachine()
    {
        while (true)
        {
            //Debug.Log("prevstate - " + prevState + " | currentstate - " + state);
            if (prevState!=state)
            {
                switch (state)
                {
                    case jointState.basic :
                        setBasic();
                        break;
                    case jointState.active :
                        active();
                        break;
                    case jointState.nonActive :
                        nonActive();
                        break;
                }
                prevState = state;
                //Debug.Log("Current Joint State : " + state);
            }
            yield return new WaitForSeconds(1f);
        }
    }

    void active()
    {
        if(GetComponent<OVRGrabbable>().isGrabbed)
        GetComponent<OVRGrabbable>().grabbedBy.GrabEnd();
        foreach (var vGrabbable in listGrabbable)
        {
            if (vGrabbable.gameObject.GetComponent<HandleGrabber>() != null)
            {
                vGrabbable.enabled = true;
            }
            else
            {
                vGrabbable.enabled = false;
            }
        }
        foreach (var vJoint in listJoint)
        {
            if (vJoint.GetComponent<HingeJoint>())
            {
                Rigidbody rb = vJoint.GetComponent<Rigidbody>();
                rb.isKinematic = false;
                rb.velocity = Vector3.zero;
                rb.transform.localRotation = listJointPosition[listJoint.IndexOf(vJoint)].localRotation;
                rb.transform.localPosition = listJointPosition[listJoint.IndexOf(vJoint)].localPosition;
            }
        }
    }

    void nonActive()
    {
        foreach (var vGrabbable in listGrabbable)
        {
            if (vGrabbable.gameObject.GetComponent<HandleGrabber>() != null)
            {
                normalAudio.Stop();
                actionAudio.Stop();
                if(vGrabbable.isGrabbed)
                    vGrabbable.grabbedBy.GrabEnd();
                vGrabbable.enabled = false;
            }
            else
            {
                vGrabbable.enabled = true;
            }
        }
        foreach (var vJoint in listJoint)
        {
            if (vJoint.GetComponent<HingeJoint>())
            vJoint.gameObject.GetComponent<Rigidbody>().isKinematic = true;
            Rigidbody rb = vJoint.GetComponent<Rigidbody>();
            rb.velocity = Vector3.zero;
            rb.transform.localRotation = listJointPosition[listJoint.IndexOf(vJoint)].localRotation;
            rb.transform.localPosition = listJointPosition[listJoint.IndexOf(vJoint)].localPosition;
        }
    }

    public void ChangeState(int newState)
    {
        state = (jointState)newState;
    }
}*/