﻿using System.Collections;
using System.Collections.Generic;
using MEC;
using Tictech.Oculus.Scenario;
using Tictech.Utilities.Audio;
using UnityEngine;
//using UnityEngine.Rendering.HighDefinition;

public class ToolWelding : MonoBehaviour
{
    public LayerMask layer;
    public float range;
    public float minLight, maxLight;
    public float modifySpeed;

    [Header("Sound")] 
    public AudioRoutine normalAudio;
    public AudioRoutine actionAudio;

    private LineRenderer _line;
    private ParticleSystem _particle;
    //private HDAdditionalLightData _light;
    private bool _isAction;
    private bool _isActive;
    private float _intensity;
    private float _targetIntensity;
    private RaycastHit _hit;
    private Vector3 _endPoint;

    private float _intensityChange;
    private ScenarioTool _tool;
    private OVRGrabbable _grabbable;
    private OVRInput.Controller _lastController;

    private void Start()
    {
        _tool = GetComponent<ScenarioTool>();
        _line = GetComponentInChildren<LineRenderer>();
        _particle = GetComponentInChildren<ParticleSystem>();
        //_light = GetComponentInChildren<HDAdditionalLightData>();

        _grabbable = GetComponent<OVRGrabbable>();
        _grabbable.onTriggerBegin += delegate { Toggle(true); };
        _grabbable.onTriggerRelease += delegate { Toggle(false); };
        _grabbable.onGrabRelease.AddListener(delegate { Toggle(false); });

        Toggle(false);
    }

    private void Toggle(bool value)
    {
        Debug.Log("WeldingTool state: " + value);

        _isActive = value;

        if (_isActive)
        {
            _lastController = _grabbable.grabbedBy.controller;
            InvokeRepeating(nameof(Haptic), 0f, 1.9f);
            normalAudio.Play();
            return;
        }

        CancelInvoke();
        OVRInput.SetControllerVibration(0f, 0f, _lastController);
        normalAudio.Stop();
        actionAudio.Stop();
        _particle.Stop();
        _intensity = 0f;
        //_light.intensity = _intensity;

        _isAction = false;
        _line.SetPosition(1, Vector3.zero);
    }

    private void Haptic()
    {
        OVRInput.SetControllerVibration(.8f, .7f, _lastController);
    }

    private void FixedUpdate()
    {
        if (!_isActive)
            return;
        
        if (Physics.Raycast(_line.transform.position, _line.transform.forward, out _hit, range, layer))
        {
            if (!_isAction)
            {
                normalAudio.Stop();
                actionAudio.Play();
                _isAction = true;
            }
            
            _endPoint = transform.InverseTransformPoint(_hit.point);
            _line.SetPosition(1, _line.transform.InverseTransformPoint(_hit.point));
            _particle.transform.localPosition = _endPoint;
            
            if (!_particle.isPlaying)
            {
                _particle.Play();
                _intensity = minLight;
                //_light.intensity = _intensity;
            }

            _intensityChange += Time.deltaTime * Random.Range(1f, 3f);
            if (_intensityChange > 1f)
            {
                _targetIntensity = Random.Range(minLight, maxLight);
                _intensityChange = 0f;
            }

            _intensity = Mathf.MoveTowards(_intensity, _targetIntensity, Time.deltaTime * modifySpeed);
            //_light.intensity = _intensity;

            var trigger = _hit.collider.GetComponent<ScenarioToolTrigger>();
            if(trigger)
                trigger.Trigger();
        }
        else if (_isAction)
        {
            _isAction = false;
            normalAudio.Play();
            actionAudio.Stop();
            _particle.Stop();
        }
        else
        {
            //_light.intensity = minLight;
            _particle.transform.localPosition = transform.InverseTransformPoint(_line.transform.position + _line.transform.forward * range);
            _endPoint = transform.InverseTransformPoint(transform.position + transform.forward * range);
            _line.SetPosition(1, _endPoint);
        }
    }
}
