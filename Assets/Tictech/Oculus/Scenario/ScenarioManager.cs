﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using DG.Tweening;
using MEC;
using Sirenix.OdinInspector;
using Tictech.EnterpriseUniversity;
using Tictech.Utilities.Audio;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Tictech.Oculus.Scenario
{
    public class ScenarioManager : MonoBehaviour
    {
        public static ScenarioManager Instance;

        [Header("General")]
        public GameObject slotMarkerPrefab;
        public GameObject toolMarkerPrefab;
        public Material highlightMaterial;
        public AudioReference audioReference;

        [Header("Info Panel Content")]
        public Sprite scenarioImage;
        public string scenarioName;
        [TextArea]
        public string scenarioDetail;
        public Sprite subScenarioImage;
        public string subScenarioName;
        [TextArea]
        public string subScenarioDetail;

        [Header("Stage")]
        public float desiredEta;
        public Transform startingPoint;
        public TeleportPoint[] defaultTeleports;
        [ReadOnly]
        public ScenarioStepInfo[] stepsInfo;

        [Header("User Interface")]
        public TeleportScaleCanvas menuCanvas;
        public TeleportScaleCanvas completionCanvas;
        public TextMeshProUGUI stepTitleText;
        public TextMeshProUGUI stepDetailText;
        public RatioVideoController videoController;
        public Button nextButton;
        public bool summonMenuOnStart;

        [Header("Finish Interface")]
        public TextMeshProUGUI timeText;
        public TextMeshProUGUI scoreText;
        public TeleportScaleCanvas finishCanvas;
        [Tooltip("Add this key to audio reference to play score audio.")]
        [SerializeField, ReadOnly]
        private string finishScoreAudioRef = "Finish Score";

        [Header("User Data Integration")]
        public string scorePostUrl;
        public int trainingId;
        public string userParameter;

#if UNITY_EDITOR
        [Header("Debug")]
        public int jumpToStep;
        public string debugAnimTrigger;
        public string debugMaterial;
#endif

        private static readonly int MatColor = Shader.PropertyToID("_BaseColor");

        public static int Mistake;
        public static int StepCount;
        public static int CompletedStep;
        public static int currentStep { get; set; }

        private const OVRInput.Controller MenuController = OVRInput.Controller.LTouch;
        private TeleportPoint[] _teleportPoints;
        private float _sessionTime;
        private int _currentStep;
        private bool _stepComplete;
        private CoroutineHandle _sessionHandle;
        private Rigidbody _playerRigidbody;

        [Header("Info Panel Object")]
        public GameObject objectivePanel;
        public Image scenarioImage1;
        public Image scenarioImage2;
        public TextMeshProUGUI scenarioTitle1, scenarioTitle2;
        public TextMeshProUGUI scenarioDetail1, scenarioDetail2;
        public Image subScenarioImage1, subScenarioImage2;
        public GameObject tagPanel1, tagPanel2;
        public TextMeshProUGUI subScenarioTitle1, subScenarioTitle2;
        public TextMeshProUGUI subScenarioDetail1, subScenarioDetail2;
        public TextMeshProUGUI endText;

        private void Awake()
        {
            if (this.enabled == true)
                if (Instance == null)
                    Instance = this;
                else if (Instance != this)
                    Destroy(gameObject);
        }

        private IEnumerator Start()
        {
            stepsInfo = GetComponentsInChildren<ScenarioStepInfo>();
            foreach (var e in defaultTeleports)
                e.gameObject.SetActive(false);
            yield return new WaitForEndOfFrame();
            _teleportPoints = FindObjectsOfType<TeleportPoint>();
            foreach (var e in _teleportPoints)
                e.gameObject.SetActive(false);
            yield return new WaitForEndOfFrame();
            foreach (var e in defaultTeleports)
                e.gameObject.SetActive(true);

            var player = GameObject.FindWithTag("Player");
            _playerRigidbody = player.GetComponent<Rigidbody>();
            player.transform.position = startingPoint.position;
            yield return new WaitForSeconds(1f);
            CompletedStep = 0;
            if (summonMenuOnStart)
                menuCanvas.Summon();

            scenarioImage1.sprite = scenarioImage;
            scenarioImage2.sprite = scenarioImage;

            scenarioTitle1.text = scenarioName;
            scenarioTitle2.text = scenarioName;
            if (string.IsNullOrEmpty(scenarioDetail))
                if (!string.IsNullOrEmpty(ScenarioLoader.instance.scenarioData.data[ScenarioLoader.SelectedTopic].detail))
                    scenarioDetail = ScenarioLoader.instance.scenarioData.data[ScenarioLoader.SelectedTopic].detail;
                else scenarioDetail = ScenarioLoader.instance.scenarioData.data[ScenarioLoader.SelectedTopic].summary;
            scenarioDetail1.text = scenarioDetail;
            scenarioDetail2.text = scenarioDetail;

            subScenarioImage1.sprite = subScenarioImage;
            subScenarioImage2.sprite = subScenarioImage;

            if ((tagPanel1 || tagPanel2) && ScenarioLoader.instance.tagData.tagList.Length > 0)
            {
                foreach (var item in ScenarioLoader.instance.tagData.tagList)
                {
                    if (!ScenarioLoader.GetSelectedScenario().tags.Contains(item.tagName)) continue;
                    GameObject go = GameObject.Instantiate(tagPanel1, tagPanel1.transform.parent);
                    go.GetComponentInChildren<TextMeshProUGUI>().text = item.tagName + " " + ScenarioLoader.GetSelectedScenario().type;
                    go.GetComponentInChildren<Image>().color = item.tagColor;
                    go.SetActive(true);
                    go = GameObject.Instantiate(tagPanel2, tagPanel2.transform.parent);
                    go.GetComponentInChildren<TextMeshProUGUI>().text = item.tagName + " " + ScenarioLoader.GetSelectedScenario().type;
                    go.GetComponentInChildren<Image>().color = item.tagColor;
                    go.SetActive(true);
                }
            }

            subScenarioTitle1.text = subScenarioName;
            subScenarioTitle2.text = subScenarioName;
            if (string.IsNullOrEmpty(subScenarioDetail))
                if (!string.IsNullOrEmpty(ScenarioLoader.instance.scenarioData.data[ScenarioLoader.SelectedTopic].scenarios.Find(t=> t.title == ScenarioLoader.SelectedScenario).detail))
                    subScenarioDetail = ScenarioLoader.instance.scenarioData.data[ScenarioLoader.SelectedTopic].scenarios.Find(t => t.title == ScenarioLoader.SelectedScenario).detail;
            subScenarioDetail1.text = subScenarioDetail;
            subScenarioDetail2.text = subScenarioDetail;
            endText.text = "Selamat! Anda berhasil menyelesaikan subskenario " + subScenarioName + ".";
            objectivePanel.GetComponentsInChildren<TextMeshProUGUI>().ToList().Find(t => t.gameObject.name == "Objective Detail").text = "Objektif\n" +
                "Mempelajari mengenai " + scenarioName +". Pada pelatihan virtual ini, Anda akan mengetahui perawatan mesin dari awal hingga siap dioperasikan.\n\n" +
                "Scoring\n" +
                "Perhitungan score dihitung berdasarkan ketepatan dan kecepatan Anda saat melakukan maintenance pada mesin.\n\n" +
                "Notes\n" +
                "Tidak semua langkah termasuk dalam scoring, ada beberapa langkah yang mengharuskan Anda untuk melihat dan memahami dalam bentuk video saja.";

#if UNITY_EDITOR
            if (jumpToStep > 0)
            {
                objectivePanel.SetActive(false);
                _stepComplete = true;
                _currentStep = jumpToStep - 1;
                for (int i = 0; i <= _currentStep; i++)
                {
                    foreach (var e in stepsInfo[i].itemCompletion)
                    {
                        foreach (var f in e.transform.parent.GetComponentsInChildren<Renderer>())
                        {
                            f.enabled = false;
                        }
                    }

                    foreach (var e in stepsInfo[i].scenarioGroups)
                    {
                        foreach (var f in e.transform.parent.GetComponentsInChildren<Renderer>())
                        {
                            f.enabled = false;
                        }
                    }

                    foreach (var e in stepsInfo[i].stepSlots)
                    {
                        foreach (var f in e.transform.parent.GetComponentsInChildren<Renderer>())
                        {
                            f.enabled = false;
                        }
                    }
                }
                //if(!string.IsNullOrEmpty(debugAnimTrigger))
                //    GetComponent<Animator>().SetTrigger(debugAnimTrigger);
                NextStep();
            }
#endif

            if (!EuRuntimeManager.Instance)
                yield break;
            stepTitleText.text = ScenarioLoader.SelectedScenario;
            stepDetailText.text = "";
        }

        private void Update()
        {
            if (OVRInput.GetDown(OVRInput.RawButton.Y))
                ToggleMenu();
        }

        private void ToggleMenu()
        {
            if (menuCanvas.transform.localScale.x > 0.0001f)
                menuCanvas.Toggle(false);
            else
                menuCanvas.Summon();
        }

        public void LoadSummonStep(int index)
        {
            var step = stepsInfo[index];
            step.Completed = true;
            stepTitleText.text = _currentStep > 0 ? (_currentStep + ". ") : "" + step.title;
            stepDetailText.text = step.detail;
            videoController.Play(step.clip);
            menuCanvas.Summon();

            if (stepsInfo.Any(e => !e.Completed))
                return;

            FinishSession();
        }

        public void LoadScene(string sname)
        {
            SceneManager.LoadScene(sname);
        }

        public void FadeInMaterial(string oName)
        {
            Timing.RunCoroutine(DoFadeMaterial(oName, 1f));
        }

        public void FadeOutMaterial(string oName)
        {
            Timing.RunCoroutine(DoFadeMaterial(oName, 0f));
        }

        public void ToggleInGamePrefab(int id)
        {
            foreach (var e in FindObjectsOfType<ScenarioTool>())
            {
                Destroy(e.gameObject);
            }

            SceneManager.LoadScene(2);
            return;

            var istp = FindObjectOfType<InScenePrefabToggle>();
            if (istp)
                istp.TogglePrefab(id);
            else
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        private IEnumerator<float> DoFadeMaterial(string oName, float alpha)
        {
            var ren = GameObject.Find(oName);
            if (ren)
            {
                var renMat = ren.GetComponent<MeshRenderer>().material;
                var color = renMat.GetColor(MatColor);
                var startAlpha = color.a;
                float a = 0;
                while (a < 1f)
                {
                    a += Time.deltaTime;
                    color.a = Mathf.Lerp(startAlpha, alpha, a);
                    renMat.SetColor(MatColor, color);
                    yield return Timing.WaitForOneFrame;
                }
            }
            else
            {
                Debug.LogWarning("No renderer in path: " + oName);
            }
        }

        IEnumerator<float> SessionRuntime()
        {
            while (true)
            {
                _sessionTime += Time.deltaTime;
                yield return Timing.WaitForOneFrame;
            }
        }

        public void WaitStep(float wait)
        {
            Timing.RunCoroutine(Waiting(wait));
        }

        private IEnumerator<float> Waiting(float wait)
        {
            yield return Timing.WaitForSeconds(wait);
            CompleteStep();
        }

        public void StartSession()
        {
            _sessionHandle = Timing.RunCoroutine(SessionRuntime());
            _stepComplete = true;
            _currentStep = 0;
            NextStep();
            currentStep = _currentStep;
            GenerateNextScenarioButton();
            GenerateResetButton();
        }

        public void CompleteStep()
        {
            _stepComplete = true;
            stepsInfo[_currentStep].Completed = true;
            stepsInfo[_currentStep].onStepEnd?.Invoke();
            _currentStep++;
            currentStep = _currentStep;

            Debug.Log("Current step: " + _currentStep);
            if (_currentStep < stepsInfo.Length)
            {
                nextButton.interactable = true;
                menuCanvas.Toggle(false);
                menuCanvas.SetActive(false);
                completionCanvas.Summon();
                completionCanvas.GetComponentInChildren<Animator>().SetTrigger("Summon");
            }
            else
            {
                FinishSession();
            }
        }

        public void NextStep()
        {
            if (!_stepComplete)
                return;

            Debug.Log("Ending step: " + stepsInfo[_currentStep].title);
            Timing.RunCoroutine(PrepareStep());
        }

        private IEnumerator<float> PrepareStep()
        {
            menuCanvas.SetActive(true);
            _stepComplete = false;
            nextButton.interactable = false;
            _playerRigidbody.isKinematic = true;

            yield return Timing.WaitForSeconds(.1f);
            var step = stepsInfo[_currentStep];
            step.onStepBegin?.Invoke();
            yield return Timing.WaitForSeconds(.1f);
            menuCanvas.Summon();
            completionCanvas.Toggle(false);

            stepTitleText.text = (_currentStep + 1) + ". " + step.title;
            stepDetailText.text = step.detail;
            videoController.Play(step.clip);
            step.Activate();

            yield return Timing.WaitForSeconds(1f);
            _playerRigidbody.isKinematic = false;
        }

        private void FinishSession()
        {
            Timing.KillCoroutines(_sessionHandle);

            var span = TimeSpan.FromSeconds((double)(new decimal(_sessionTime)));

            timeText.text = span.Minutes + ":" + span.Seconds;
            //scoreText.DOText(GetScore().ToString(CultureInfo.InvariantCulture), 1f);
            scoreText.text = GetScore().ToString();
            scoreText.GetComponentInParent<AudioSource>().PlayOneShot(audioReference.GetClip(finishScoreAudioRef));

            nextButton.interactable = false;
            finishCanvas.Summon();

            if (EuRuntimeManager.Instance)
                Timing.RunCoroutine(EuRuntimeManager.Instance.PostScore(ScenarioLoader.GetSelectedScenario().trainingId.ToString(), GetScore()));
        }

        public bool IsStepTransition => _stepComplete;

        public int GetScore()
        {
            return Mathf.CeilToInt((1000 - Mathf.Clamp(_sessionTime - desiredEta, 0f, 500f) - Mathf.Clamp(Mistake * 5f, 0f, 500f)) / 1000f * 100f);
        }

        private void GenerateNextScenarioButton()
        {
            Button nextScnBt = finishCanvas.GetComponentsInChildren<Button>(true)
                .FirstOrDefault(e => string.Equals(e.gameObject.name, "NextScenarioButton"));
            if (string.IsNullOrEmpty(SubScenarioManager.instance.nextSubName()))
                nextScnBt.gameObject.SetActive(false);
            if (nextScnBt)
            {
                if (ScenarioLoader.instance)
                    nextScnBt.transform.GetComponentInChildren<TextMeshProUGUI>(true).text = "Subskenario selanjutnya (" + SubScenarioManager.instance.nextSubName() + ")";
                if (!string.IsNullOrEmpty(ScenarioLoader.SelectedTopic) && !string.IsNullOrEmpty(ScenarioLoader.SelectedScenario))
                    nextScnBt.onClick.AddListener(SubScenarioManager.instance.NextSubScenario);
            }
        
            nextScnBt = finishCanvas.GetComponentsInChildren<Button>(true)
                .FirstOrDefault(e => string.Equals(e.gameObject.name, "NextScenarioButton(Clone)"));            
            if (nextScnBt)
            {
                if (ScenarioLoader.instance)
                    nextScnBt.transform.GetComponentInChildren<TextMeshProUGUI>(true).text = "Subskenario selanjutnya (" + SubScenarioManager.instance.nextSubName() + ")";
                if (!string.IsNullOrEmpty(ScenarioLoader.SelectedTopic) && !string.IsNullOrEmpty(ScenarioLoader.SelectedScenario))
                    nextScnBt.onClick.AddListener(SubScenarioManager.instance.NextSubScenario);
            }
        }

        private void GenerateResetButton()
        {
            Button resetBtn0 = menuCanvas.GetComponentsInChildren<Button>(true)
                .FirstOrDefault(e => string.Equals(e.gameObject.name.ToUpper(), "ResetButton".ToUpper()));
            Button resetBtn1 = finishCanvas.GetComponentsInChildren<Button>(true)
                .FirstOrDefault(e => string.Equals(e.gameObject.name.ToUpper(), "ResetButton".ToUpper()));
            
            Debug.Log("btn0 " + resetBtn0);
            Debug.Log("btn1 " + resetBtn1);

            if (resetBtn0 && resetBtn1)
            {
                resetBtn0.onClick.AddListener(() => {
                    ClearTool();
                    SubScenarioManager.instance.SpawnSubScenario(ScenarioLoader.GetSelectedScenario().prefabName); 
                });
                resetBtn1.onClick.AddListener(() => {
                    ClearTool();
                    SubScenarioManager.instance.SpawnSubScenario(ScenarioLoader.GetSelectedScenario().prefabName); 
                });

                resetBtn0.GetComponentsInChildren<Button>().LastOrDefault().onClick.AddListener(() => {
                    ClearTool();
                    SubScenarioManager.instance.SpawnSubScenario(ScenarioLoader.GetSelectedScenario().prefabName); 
                });
                resetBtn1.GetComponentsInChildren<Button>().LastOrDefault().onClick.AddListener(() => {
                    ClearTool();
                    SubScenarioManager.instance.SpawnSubScenario(ScenarioLoader.GetSelectedScenario().prefabName); 
                });

                /*resetBtn0.onClick.AddListener(SubScenarioManager.instance.SpawnSubScenario);
                resetBtn1.onClick.AddListener(SubScenarioManager.instance.SpawnSubScenario);

                resetBtn0.GetComponentsInChildren<Button>().LastOrDefault().onClick.AddListener(SubScenarioManager.instance.SpawnSubScenario);
                resetBtn1.GetComponentsInChildren<Button>().LastOrDefault().onClick.AddListener(SubScenarioManager.instance.SpawnSubScenario);*/
            }
            else
            {
                resetBtn0.gameObject.SetActive(false);
                resetBtn1.gameObject.SetActive(false);
            }
        }

        public void ClearTool ()
        {
            foreach (var item in GameObject.FindObjectsOfType<ScenarioTool>())
            {
                Destroy(item.gameObject);
            }
        }
    }
}