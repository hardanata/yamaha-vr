﻿using UnityEngine;

namespace Tictech.Oculus.Core
{
    public class GeneralGrabbable : OVRGrabbable
    {
        [SerializeField] protected string grabSound;

        protected override void Start()
        {
            base.Start();
            if (!string.IsNullOrEmpty(grabSound))
            {
                
            }
        }
    }
}
