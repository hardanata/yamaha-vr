﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using MEC;
using Sirenix.OdinInspector;

namespace Arna.AssetManagement
{
    public class AssetBundlePrefabInstantiator : SerializedMonoBehaviour
    {
        public Dictionary<string, string[]> objects;
        public List<string> dependencies;
        public bool setAsParent;
        
        private string _instanceId;
        private readonly List<GameObject> _instancedObject = new List<GameObject>();
        private bool _unloaded = false;
        
        private void Start()
        {
            if (!AssetBundleManager.Instance)
            {
                _unloaded = true;
                Destroy(gameObject);
                return;
            }
            
            _instanceId = gameObject.GetInstanceID().ToString();
            Timing.RunCoroutine(Load());
        }

        private void OnDestroy()
        {
            Unload();
        }

        private IEnumerator<float> Load()
        {
            foreach (var bundle in dependencies)
            {
                yield return Timing.WaitUntilDone(AssetBundleManager.Instance.LoadBundle(bundle, _instanceId));
            }
            
            foreach (var bundle in objects.Keys)
            {
                yield return Timing.WaitUntilDone(AssetBundleManager.Instance.LoadBundle(bundle, _instanceId));
                foreach (var item in objects[bundle])
                {
                    _instancedObject.Add(setAsParent
                        ? Instantiate(AssetBundleManager.Instance.GetAsset<GameObject>(bundle, item), transform)
                        : Instantiate(AssetBundleManager.Instance.GetAsset<GameObject>(bundle, item)));
                }

                yield return Timing.WaitForOneFrame;
            }
            InvokeRepeating(nameof(CheckInstanced), 5f, 5f);
        }

        private void CheckInstanced()
        {
            var active = 0;
            foreach (var io in _instancedObject)
            {
                if (io) active++;
            }

            if (active != 0) return;
            
            Unload();
            Destroy(gameObject);
        }

        private void Unload()
        {
            if (_unloaded) return;
            
            foreach (var bundle in dependencies)
            {
                if (!AssetBundleManager.Instance.LoadedBundles.ContainsKey(bundle)) break;
                AssetBundleManager.Instance.LoadedBundles[bundle].RemoveReference(_instanceId);
            }    
                
            foreach (var bundle in objects.Keys)
            {
                if (!AssetBundleManager.Instance.LoadedBundles.ContainsKey(bundle)) break;
                AssetBundleManager.Instance.LoadedBundles[bundle].RemoveReference(_instanceId);
            }

            _unloaded = true;
        }
    }
}
