﻿using UnityEditor;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Networking;
using System.IO;
using System.Collections;

namespace Arna.AssetManagement
{
    public class EditorConfigurationTool : Editor
    {
        [MenuItem("Tools/AssetBundle Manager/Refresh Android Database")]
        static void RefreshAndroidBundleDatabase()
        {
            RefreshBundle(AssetBundleManager.TargetPlatform.Android);
        }
        [MenuItem("Tools/AssetBundle Manager/Refresh iOS Database")]
        static void RefreshIOSBundleDatabase()
        {
            RefreshBundle(AssetBundleManager.TargetPlatform.iOS);
        }
        [MenuItem("Tools/AssetBundle Manager/Refresh Standalone Database")]
        static void RefreshStandaloneBundleDatabase()
        {
            RefreshBundle(AssetBundleManager.TargetPlatform.Standalone);
        }

        static void RefreshBundle(AssetBundleManager.TargetPlatform platform)
        {
            AssetBundleDatabase database = AssetDatabase.LoadAssetAtPath<AssetBundleDatabase>("Assets/Arna/Asset Management/AssetBundleData/" + platform.ToString() + "/AssetBundle Database.asset");
            
            // Find all assets labelled with 'item data' :
            var n = new Dictionary<string, AssetBundleDataScriptable>();
            string[] guids = AssetDatabase.FindAssets("l:AssetBundleData", new string[] { "Assets/Arna/Asset Management/AssetBundleData/" + platform.ToString() });
            foreach (string g in guids)
            {
                var data = AssetDatabase.LoadAssetAtPath<AssetBundleDataScriptable>(AssetDatabase.GUIDToAssetPath(g));
                n.Add(data.bundleName, data);
                Debug.Log("Added data: " + data.bundleName);
            }

            database.RefreshDatabase("onestepahead", n);
            EditorUtility.SetDirty(database);
        }
    }
}