﻿using System.Collections;
using System.Collections.Generic;
using MEC;
using UnityEngine;
using System;

namespace Arna.AssetManagement
{
    public class AssetBundlePatcher : MonoBehaviour
    {
#if UNITY_EDITOR
        [Header("Debug")] public string debugUploadFile;
#endif

        [HideInInspector]
        public Action onPatchCompleted;
        public Dictionary<string, AssetBundleData> dynamicAssets { get; private set; }

        //private DatabaseReference _patchReference;
        //private StorageReference _assetReference;
        private AssetBundleManager _bundleManager;

        private CoroutineHandle _patchHandle;
        private List<AssetBundleData> _patchQueue;

        private void Start()
        {
            Timing.RunCoroutine(Initialize());
        }

        private IEnumerator<float> Initialize()
        {
#if UNITY_EDITOR
            //GoogleDriveFiles.List().Send().OnDone += list => { };
#endif
            yield return Timing.WaitForOneFrame;
        }
    }
}