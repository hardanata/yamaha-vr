﻿using System;
using MEC;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Arna.Loading;
using Arna.Firebase;
using Object = UnityEngine.Object;

namespace Arna.AssetManagement
{
    public class AssetBundleManager : MonoBehaviour
    {
        public static AssetBundleManager Instance;

        public enum TargetPlatform
        {
            Standalone = 0,
            Android = 1,
            iOS = 2
        }

        public Dictionary<string, AssetBundleData> BundleData { get; private set; }
        public Dictionary<string, LoadedBundle> LoadedBundles { get; internal set; }

        private FirebasePatcher _firebasePatcher;

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else if (Instance != this)
            {
                Destroy(gameObject);
            }
        }

        private void Start()
        {
            if (ES3.KeyExists("CachedBundleData")) BundleData = ES3.Load<Dictionary<string, AssetBundleData>>("CachedBundleData");
            if (BundleData == null) BundleData = new Dictionary<string, AssetBundleData>();

            LoadedBundles = new Dictionary<string, LoadedBundle>();
            _firebasePatcher = FirebasePatcher.Instance;

            foreach (var key in BundleData.Keys) Debug.Log("Cached bundle: " + key);
        }

        private void OnDestroy()
        {
            ES3.Save<Dictionary<string, AssetBundleData>>("CachedBundleData", BundleData);
        }

        public T GetAsset<T>(string bundle, string asset) where T : Object
        {
            if (!LoadedBundles.ContainsKey(bundle))
            {
                Debug.LogError("AssetBundle " + bundle + "isn't loaded, please make sure to load the corresponding bundle first.");
                return null;
            }

            return LoadedBundles[bundle].GetAsset<T>(asset);
        }

        public IEnumerator<float> LoadBundle(string bundle, string usageKey)
        {
            if (!BundleData.ContainsKey(bundle))
            {
                Debug.LogWarning("There is no bundle with name: " + bundle);
                yield break;
            }

            if (BundleIsLoaded(bundle)) yield break;
            
            var data = BundleData[bundle];
            if (data.type != AssetBundleType.Necessary && !AssetIsValid(_firebasePatcher.dynamicAssets[bundle]))
            {
                yield return Timing.WaitUntilDone(_firebasePatcher.QueuePatch(_firebasePatcher.dynamicAssets[bundle]));
            }

            var task = UnityWebRequestAssetBundle.GetAssetBundle(data.url, Hash128.Parse(data.hash), data.crc);
            var async = task.SendWebRequest();
            while (!async.isDone) yield return Timing.WaitForOneFrame;
            if (task.isNetworkError || task.isHttpError)
            {
                Debug.Log(task.error);
            }

            if (BundleIsLoaded(bundle)) yield break;
    
            LoadedBundles.Add(data.bundleName, new LoadedBundle(DownloadHandlerAssetBundle.GetContent(task), usageKey));
            Debug.Log("Bundle is loaded: " + data.bundleName);
        }

        bool BundleIsLoaded(string bundle)
        {
            if (!LoadedBundles.ContainsKey(bundle)) return false;
            LoadedBundles[bundle].AddReference(bundle);
            Debug.Log("Bundle is already loaded.");
            return true;

        }

        public void UnloadBundle(string key, bool unloadAll)
        {
            LoadedBundles[key].Unload(unloadAll);
            LoadedBundles.Remove(key);
        }

        public static bool AssetIsValid(AssetBundleData data)
        {
            return Instance.BundleData.ContainsKey(data.bundleName) 
                   && Instance.BundleData[data.bundleName].crc.Equals(data.crc) 
                   && Instance.BundleData[data.bundleName].hash.Equals(data.hash)
                   && Instance.BundleData[data.bundleName].bundleName.Equals(data.bundleName)
                   && Instance.BundleData[data.bundleName].description.Equals(data.description);
        }
    }

    public class LoadedBundle
    {
        private readonly List<string> _usageKeys;
        public string Name => _assetBundle.name;
        
        private readonly AssetBundle _assetBundle;

        public LoadedBundle(AssetBundle bundle, string usageKey)
        {
            _assetBundle = bundle;
            _usageKeys = new List<string>();
            _usageKeys.Add(usageKey);
        }

        public T GetAsset<T>(string asset) where T : UnityEngine.Object
        {
            return _assetBundle.LoadAsset<T>(asset);
        }

        public void AddReference(string key)
        {
            if(!_usageKeys.Contains(key)) _usageKeys.Add(key);
        }        

        public void RemoveReference(string key)
        {
            if (_usageKeys.Contains(key)) _usageKeys.Remove(key);
            if (_usageKeys.Count == 0) AssetBundleManager.Instance.UnloadBundle(Name, true);
        }

        public void RemoveReference(string[] keys)
        {
            foreach (var key in keys) if (_usageKeys.Contains(key)) _usageKeys.Remove(key);
            if (_usageKeys.Count == 0) AssetBundleManager.Instance.UnloadBundle(Name, true);
        }

        public void Unload(bool unloadAll)
        {
            Debug.Log("Asset bundle " + Name + " is unloaded.");
            _assetBundle.Unload(unloadAll);
        }
    }

    [System.Serializable]
    public class AssetBundleData
    {
        public string bundleName;
        public string description;
        public AssetBundleType type;
        [HideInInspector] public uint crc;
        [HideInInspector] public string hash;
        [HideInInspector] public string url;
        [HideInInspector] public long size;
     
        public AssetBundleData()
        {

        }

        public AssetBundleData(string bn, string ds, AssetBundleType t)
        {
            bundleName = bn;
            description = ds;
            type = t;
        }
    }

    [System.Serializable]
    public class CachedBundleData
    {
        public Dictionary<string, AssetBundleData> Data;

        public CachedBundleData()
        {
            Data = new Dictionary<string, AssetBundleData>();
        }
    }

    public enum AssetBundleType
    {
        Necessary,
        Downloadable,
        Purchaseable,
        Unavailable
    }
}