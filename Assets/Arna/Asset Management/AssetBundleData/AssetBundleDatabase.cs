﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Arna.AssetManagement
{
    [CreateAssetMenu(fileName = "New AssetBundle Database", menuName = "AssetBundle Manager/Database", order = 0)]
    public class AssetBundleDatabase : SerializedScriptableObject
    {
        [SerializeField]
        public string assetbundlePath;
        public Dictionary<string, int> versions;
        public Dictionary<string, AssetBundleDataScriptable> bundles;

        public void RefreshDatabase(string key, Dictionary<string, AssetBundleDataScriptable> values)
        {
            if(key != "onestepahead")
            {
                Debug.LogWarning("Password missmatch.");
                return;
            }

            bundles = values;
        }
    }
}