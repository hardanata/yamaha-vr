﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Arna.AssetManagement
{
    [CreateAssetMenu(fileName = "New AssetBundle Data", menuName = "AssetBundle Manager/Data", order = 0)]
    public class AssetBundleDataScriptable : SerializedScriptableObject
    { 
        public string bundleName;
        public string description;
        public AssetBundleType type;
        
        public AssetBundleData BundleData()
        {
            return new AssetBundleData(bundleName, description, type);
        }
    }
}