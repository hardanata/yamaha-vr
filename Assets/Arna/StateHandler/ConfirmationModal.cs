﻿using UnityEngine;

public class ConfirmationModal : MonoBehaviour {
    [SerializeField] UnityEngine.UI.Button m_ConfirmButton;
    [SerializeField] TMPro.TextMeshProUGUI m_Message;

    public void Init(string message, UnityEngine.Events.UnityAction e)
    {
        m_Message.text = message;
        m_ConfirmButton.onClick.AddListener(() => e());

        StateHandler.Instance.OnBack += SelfDestruct;
    }

    private void OnDestroy()
    {
        StateHandler.Instance.OnBack -= SelfDestruct;
    }

    public void SelfDestruct()
    {
        Destroy(gameObject);
    }
}
