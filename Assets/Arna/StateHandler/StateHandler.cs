﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using System.Collections;
using UnityEngine.Networking;

public class StateHandler : MonoBehaviour
{
    public static StateHandler Instance;

    public delegate void BackAction();
    public event BackAction OnBack;

    [Header("Modals")]
    [SerializeField] GameObject m_ConfirmationModal;
    [SerializeField] GameObject m_PicturePicker;

    private List<GameObject> m_panelHistory = new List<GameObject>();
    private bool blocked;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }
    
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (blocked)
            {
                return;
            }

            PreviousPanel();
        }
    }

    #region PANEL HANDLER
    public void ToggleBlock(bool value)
    {
        blocked = value;
    }

    public void OpenPanel(GameObject panel)
    {
        for(int i = 0; i < m_panelHistory.Count; i++)
        {
            if (!m_panelHistory[i])
            {
                m_panelHistory.RemoveAt(i);
            }
        }
        
        if(m_panelHistory.Contains(panel))
        {
            m_panelHistory.Remove(panel);
        }

        panel.SetActive(true);
        m_panelHistory.Add(panel);
    }

    public void PreviousPanel()
    {
        if (m_panelHistory.Count > 0)
        {
            if (m_panelHistory[m_panelHistory.Count - 1])
            {
                m_panelHistory[m_panelHistory.Count - 1].SetActive(false);
            }
            m_panelHistory.RemoveAt(m_panelHistory.Count - 1);
            if (m_panelHistory.Count > 0 && m_panelHistory[m_panelHistory.Count - 1])
            {
                m_panelHistory[m_panelHistory.Count - 1].SetActive(true);
            }
            else
            {
                CloseAllPanel();
            }
        }
        else if (OnBack != null)
        {
            OnBack();
            return;
        }
        else
        { 
            CreateConfirmation("Quit app?", QuitApplication);
        }
    }

    public void CloseAllPanel()
    {
        SoundHelper.Instance.PlayOnce(0);
        foreach (GameObject go in m_panelHistory)
        {
            go.SetActive(false);
        }
        m_panelHistory.Clear();
    }

    public void QuitApplication()
    {
        Application.Quit();
    }
    #endregion

    #region SCENE MANAGEMENT
    public void LoadScene(string sname)
    {
        CloseAllPanel();
        OnBack -= ShowBackHome;
        StartCoroutine(LoadSceneAsync(sname));
        SoundHelper.Instance.PlayOnce(0);
    }

    public void LoadSceneAdditive(string sname)
    {
        SceneManager.LoadSceneAsync(sname, LoadSceneMode.Additive);
    }

    public void UnloadScene(string sname)
    {
        SceneManager.UnloadSceneAsync(sname);
    }

    IEnumerator LoadSceneAsync(string sname)
    {
        ShowLoading("Loading " + sname);
        //yield return SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene());
        yield return SceneManager.LoadSceneAsync(sname, LoadSceneMode.Single);
        //UsedBundle.Unload(true);
        //SceneBundle.Unload(true);
        HideLoading();
    }

    void ShowBackHome()
    {
        CreateConfirmation("Back to home?", delegate { LoadScene("Home"); });
    }
    #endregion

    #region LOADING
    [Header("Loading")]
    [SerializeField] GameObject m_FullBlock;
    [SerializeField] GameObject m_SubLoadingPanel;
    [SerializeField] GameObject m_LoadingPanel;
    [SerializeField] TextMeshProUGUI m_LoadingMessage;

    public void ShowLoading(string message)
    {
        Debug.Log(message);
        m_LoadingMessage.text = message;
        m_LoadingPanel.SetActive(true);
    }

    public void HideLoading()
    {
        m_LoadingPanel.SetActive(false);
    }

    public void ShowSubLoad()
    {
        m_SubLoadingPanel.SetActive(true);
    }

    public void HideSubLoad()
    {
        m_SubLoadingPanel.SetActive(false);
    }

    public void SetBlock(bool value)
    {
        m_FullBlock.SetActive(value);
    }
    #endregion

    #region GETTER AND SETTER
    public int PanelCount
    {
        get
        {
            return m_panelHistory.Count;
        }
    }

    public void CreateConfirmation(string message, UnityEngine.Events.UnityAction e)
    {
        Instantiate(m_ConfirmationModal, transform).GetComponent<ConfirmationModal>().Init(message, e);
    }

    public void CreatePicturePicker(string title, UnityEngine.Events.UnityAction gallery, UnityEngine.Events.UnityAction camera)
    {
        Instantiate(m_PicturePicker, transform).GetComponent<PicturePicker>().Init(title, gallery, camera);
    }
    #endregion
}

[System.Serializable]
public class PanelLayer
{
    public Transform Container;
    public GameObject Panel;
}

public class PanelHistory
{
    public string Name;
    public int Layer;
    public GameObject Panel;

    public PanelHistory(string n, int l, GameObject p)
    {
        Name = n;
        Layer = l;
        Panel = p;
    }
}