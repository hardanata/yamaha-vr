﻿using UnityEngine;

public class SoundHelper : MonoBehaviour
{
    public static SoundHelper Instance;

    [SerializeField] SoundData[] SoundLibrary;
    private AudioSource Source;

    private bool m_Busy;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            Source = gameObject.AddComponent<AudioSource>();
            // Temporary
            m_Busy = true;
        }
    }
    
    public void PlayOnce(int id)
    {
        if (m_Busy)
        {
            return;
        }

        m_Busy = true;
        Invoke("CancelBusy", .1f);

        Source.PlayOneShot(SoundLibrary[id].Clip);
    }

    void CancelBusy()
    {
        m_Busy = false;
    }
}

[System.Serializable]
public struct SoundData
{
    [SerializeField] string m_Name;
    public AudioClip Clip;
}