﻿using UnityEngine;
using UnityEngine.UI;

public class PicturePicker : MonoBehaviour {
    [SerializeField] TMPro.TextMeshProUGUI m_Title;
    [SerializeField] Button m_PickGallery;
    [SerializeField] Button m_PickCamera;

    public void Init(string title, UnityEngine.Events.UnityAction gallery, UnityEngine.Events.UnityAction camera)
    {
        m_Title.text = title;
        m_PickGallery.onClick.AddListener(() => gallery());
        m_PickCamera.onClick.AddListener(() => camera());
        StateHandler.Instance.OnBack += SelfDestruct;
    }

    private void OnDestroy()
    {
        StateHandler.Instance.OnBack -= SelfDestruct;
    }

    public void SelfDestruct()
    {
        Destroy(gameObject);
    }
}
