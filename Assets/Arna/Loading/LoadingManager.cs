﻿using System;
using MEC;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
using Arna.AssetManagement;
using Sirenix.OdinInspector;

namespace Arna.Loading
{
    [RequireComponent(typeof(CanvasGroup))]
    public class LoadingManager : MonoBehaviour
    {
        public static LoadingManager Instance;

        public GameObject loadingIndicator;
        public GameObject progressPanel;
        public TextMeshProUGUI loadingMessage;
        public TextMeshProUGUI loadingProgressText;
        public Image loadingProgressBar;
        public Image background;

#if UNITY_EDITOR
        [SerializeField, ReadOnly] private string[] activeLoading;
#endif
     
        private CanvasGroup _canvasGroup;
        private readonly Dictionary<string, CoroutineHandle> _loadings = new Dictionary<string, CoroutineHandle>();
        private string _sceneKey;
        private List<string> _bundleUsages = new List<string>();

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else if (Instance != this)
            {
                Destroy(gameObject);
            }
        }

        private void Start()
        {
            _canvasGroup = GetComponent<CanvasGroup>();
            _canvasGroup.alpha = 0f;
            _canvasGroup.blocksRaycasts = false;
            _canvasGroup.interactable = false;
            
            #if UNITY_EDITOR
            InvokeRepeating(nameof(CountLoading), 3f, 3f);
            #endif
        }

#if UNITY_EDITOR
        void CountLoading()
        {
            activeLoading = _loadings.Keys.ToArray();
        }
#endif

        public static CoroutineHandle GenericLoadingMessage(string message, CoroutineHandle loading)
        {
            if (Instance._loadings.ContainsKey("message: " + message)) return new CoroutineHandle();
            var handle = Timing.RunCoroutine(Instance.ShowMessage(message, loading));
            Instance.AddLoading("message: " + message, handle);
            return handle;
        }

        public static CoroutineHandle ShowProgress(CoroutineHandle progress)
        {
            if (Instance._loadings.ContainsKey("Show Progress")) return new CoroutineHandle();
            var handle = Timing.RunCoroutine(Instance.LoadingWithProgress(progress));
            Instance.AddLoading("Show Progress", handle);
            return handle;
        }
        
        public static CoroutineHandle LoadProgress(long size, AsyncOperation async)
        {
            string key = "loadProgress:" + async.GetHashCode();
            var handle = Timing.RunCoroutine(Instance.LoadingProgress(key, size, async));
            Instance.AddLoading(key, handle);
            return handle;
        }
        
        public static CoroutineHandle LoadScene(string scene, LoadSceneMode mode)
        {
            string key = "loadScene:" + scene;
            var handle = Timing.RunCoroutine(Instance.LoadingScene(key, scene, mode));
            Instance.AddLoading(key, handle);
            return handle;
        }
        
        public static CoroutineHandle LoadScene(string sceneBundle, string sceneName, string[] bundleDependencies, LoadSceneMode mode)
        {
            string key = "loadScene:" + sceneName;
            var handle = Timing.RunCoroutine(Instance.LoadingScene(key, sceneBundle, sceneName, bundleDependencies, mode));
            Instance.AddLoading(key, handle);
            return handle;
        }
        
        public static CoroutineHandle ReloadCurrentScene()
        {
            string key = "loadScene:" + LastLoadedScene.SceneName;
            var handle = Timing.RunCoroutine(Instance.LoadingScene(key, LastLoadedScene.SceneBundle, LastLoadedScene.SceneName, LastLoadedScene.BundleDependencies, LastLoadedScene.Mode));
            Instance.AddLoading(key, handle);
            return handle;
        }
        
        public void AddLoading(string key, CoroutineHandle handle)
        {
            _loadings.Add(key, handle);
            _canvasGroup.blocksRaycasts = true;
            _canvasGroup.alpha = 1;
        }

        IEnumerator<float> ShowMessage(string message, CoroutineHandle handle)
        {
            Debug.Log(message);
            loadingMessage.text = message;
            loadingMessage.gameObject.SetActive(true);
            loadingIndicator.SetActive(true);
            while (handle.IsRunning) yield return Timing.WaitForOneFrame;
            yield return Timing.WaitForSeconds(.2f);
            loadingIndicator.SetActive(false);
            loadingMessage.gameObject.SetActive(false);
            Instance.EndLoading("message: " + message); 
        }

        IEnumerator<float> LoadingWithProgress(CoroutineHandle handle)
        { 
            progressPanel.SetActive(true);
            
            yield return Timing.WaitForSeconds(.2f);
            while (handle.IsRunning) yield return Timing.WaitForOneFrame;

            progressPanel.SetActive(false);
            Instance.EndLoading("Show Progress");
        }

        IEnumerator<float> LoadingProgress(string key, long size, AsyncOperation async)
        {
            while (!async.isDone)
            {
                yield return Timing.WaitForOneFrame;
                loadingProgressText.text = "Loading... (" + async.progress.ToString("P") + ")";
                loadingProgressBar.fillAmount = async.progress;
                Debug.Log("Loading... (" + async.progress.ToString("P") + ")");
            }
            EndLoading(key);
        }

        IEnumerator<float> LoadingScene(string key, string scene, LoadSceneMode mode)
        {
            _canvasGroup.alpha = 1;
            _canvasGroup.blocksRaycasts = true;
            if (mode == LoadSceneMode.Single) UnloadSceneBundles();
            yield return Timing.WaitForOneFrame;
            _sceneKey = key;

            var task = SceneManager.LoadSceneAsync(scene, mode);
            while (!task.isDone)
            {
                loadingProgressBar.fillAmount = task.progress;
                yield return Timing.WaitForSeconds(.1f);
            }

            EndLoading(key);
            LastLoadedScene = new SceneMetadata(key, "core-scene", scene, new string[0], LoadSceneMode.Single );
        }

        private IEnumerator<float> LoadingScene(string key, string sceneBundle, string sceneName, string[] bundleDependencies, LoadSceneMode mode)
        {
            _canvasGroup.alpha = 1;
            _canvasGroup.blocksRaycasts = true;
            if (mode == LoadSceneMode.Single) UnloadSceneBundles();
            yield return Timing.WaitForOneFrame;
            _sceneKey = key;
            
            foreach (var bundle in bundleDependencies)
            {
                yield return Timing.WaitUntilDone(AssetBundleManager.Instance.LoadBundle(bundle, key));
                _bundleUsages.Add(bundle);
            }
            yield return Timing.WaitUntilDone(AssetBundleManager.Instance.LoadBundle(sceneBundle, key));
            _bundleUsages.Add(sceneBundle);

            var task = SceneManager.LoadSceneAsync(sceneName, mode);
            while (!task.isDone)
            {
                loadingProgressBar.fillAmount = task.progress;
                yield return Timing.WaitForSeconds(.1f);
            }
            EndLoading(key);
            LastLoadedScene = new SceneMetadata(key, sceneBundle, sceneName, bundleDependencies, mode);
        }

        void UnloadSceneBundles()
        {
            foreach (var bundle in _bundleUsages)
            {
                AssetBundleManager.Instance.LoadedBundles[bundle].RemoveReference(_sceneKey);
            }    
            _bundleUsages.Clear();
        }
        
        bool EndLoading(string key)
        {
            if (!_loadings.ContainsKey(key))
            {
                Debug.LogWarning("Failed to end loading: '" + key + "'\r\nKey doesn't exist.");
                return false;
            }
            
            Debug.Log("Loading ended: '" + key + "'");
            _loadings.Remove(key);

            if (_loadings.Count != 0) return false;
            
            _canvasGroup.blocksRaycasts = false;
            _canvasGroup.alpha = 0;
            return true;
        }

        public static SceneMetadata LastLoadedScene;
        
        public struct SceneMetadata
        {
            public string Key;
            public string SceneBundle;
            public string SceneName;
            public string[] BundleDependencies;
            public LoadSceneMode Mode;

            public SceneMetadata(string k, string sb, string sn, string[] bd, LoadSceneMode m)
            {
                Key = k;
                SceneBundle = sb;
                SceneName = sn;
                BundleDependencies = bd;
                Mode = m;
            }
        };
    }
}