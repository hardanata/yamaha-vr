﻿using System.Collections;
using System.Collections.Generic;
using Arna.Loading;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Arna.Loading
{
    public class LoadingAccess : MonoBehaviour
    {
        public void LoadScene(string sceneName)
        {
            LoadingManager.LoadScene(sceneName, LoadSceneMode.Single);
        }

        public void ReloadScene()
        {
            LoadingManager.ReloadCurrentScene();
        }
    }
}