﻿//using Firebase;
//using Firebase.Unity.Editor;
using System.Collections.Generic;
using UnityEngine;
using MEC;
using Sirenix.OdinInspector;

namespace Arna.Firebase
{
    public class FirebaseManager : MonoBehaviour
    {
        [BoxGroup("Realtime Database"), SerializeField]
        bool useDatabase;
        [ShowIf("useDatabase", true), BoxGroup("Realtime Database"), SerializeField]
        string databaseURL;

        private void Start()
        {
            //if (useDatabase) FirebaseApp.DefaultInstance.SetEditorDatabaseUrl(databaseURL);
        }
    }
}