﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using UnityEngine;
/*
using Firebase;
using Firebase.Auth;
using Firebase.Database;
using Firebase.Storage;
using Firebase.Unity.Editor;
*/
using Sirenix.OdinInspector;
using Arna.AssetManagement;
using Arna.Loading;
using UnityEngine.Networking;
using MEC;
using UnityEditor;
using UnityEngine.SceneManagement;

namespace Arna.Firebase
{
    public class FirebasePatcher : MonoBehaviour
    {
        public static FirebasePatcher Instance;

        [BoxGroup("Deployment"), SerializeField]
        public AssetBundleManager.TargetPlatform targetPlatform;
        [BoxGroup("Deployment"), SerializeField]
        private string channel;
        [BoxGroup("Deployment"), SerializeField]
        private string coreSceneBundle;
        [BoxGroup("Deployment"), SerializeField]
        private string proceedingScene;
        [BoxGroup("Deployment"), SerializeField, ReadOnly]
        private int patchVersion;

        [BoxGroup("Database Reference"), SerializeField]
        string databaseURL;
        [BoxGroup("Database Reference"), SerializeField]
        string patchPath;

        [BoxGroup("Storage Reference"), SerializeField]
        string storageURL;
        [BoxGroup("Storage Reference"), SerializeField]
        string assetPath;

#if UNITY_EDITOR
        [BoxGroup("Patching"), SerializeField] private string assetBundlePath;
        [BoxGroup("Patching"), SerializeField] private AssetBundleDatabase patchDatabase;
        [BoxGroup("Patching"), SerializeField] private string user;
        [BoxGroup("Patching"), SerializeField] private string password;
        [BoxGroup("Patching"), SerializeField] private bool pushPatch;
#endif

        [HideInInspector]
        public Action onPatchCompleted;
        public Dictionary<string, AssetBundleData> dynamicAssets { get; private set; }

        //private DatabaseReference _patchReference;
        //private StorageReference _assetReference;
        private AssetBundleManager _bundleManager;

        private CoroutineHandle _patchHandle;
        private List<AssetBundleData> _patchQueue;

        private bool _isReady = false;

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else if (Instance != this)
            {
                Destroy(gameObject);
            }
        }

        private void Start()
        {
            _patchQueue = new List<AssetBundleData>();
            _bundleManager = AssetBundleManager.Instance;
            Timing.RunCoroutine(Initiate());
        }

        IEnumerator<float> Initiate()
        {
            /*
            FirebaseApp.DefaultInstance.SetEditorDatabaseUrl(databaseURL);

            _patchReference = FirebaseDatabase.DefaultInstance.RootReference.Child(patchPath).Child(targetPlatform.ToString()).Child(channel).Child("data");
            _assetReference = FirebaseStorage.DefaultInstance.RootReference.Child(assetPath).Child(targetPlatform.ToString());*/
            dynamicAssets = new Dictionary<string, AssetBundleData>();

#if UNITY_EDITOR
            /*var login = FirebaseAuth.DefaultInstance.SignInWithEmailAndPasswordAsync(user, password);
            while (!login.IsCompleted) yield return Timing.WaitForOneFrame;

            if (login.IsCanceled)
            {
                Debug.LogError("CreateUserWithEmailAndPasswordAsync was canceled.");
                yield break;
            }
            if (login.IsFaulted)
            {
                Debug.LogError("CreateUserWithEmailAndPasswordAsync encountered an error: " + login.Exception);
                yield break;
            }

            // Firebase user has been created.
            FirebaseUser newUser = login.Result;
            Debug.LogFormat("Firebase user created successfully: {0} ({1})",
                newUser.DisplayName, newUser.UserId);


            if (pushPatch)
            {
                string message = "Pushing patch version: " + patchDatabase.versions[channel] + ", to channel: " + channel;
                yield return Timing.WaitUntilDone(LoadingManager.GenericLoadingMessage(message, Timing.RunCoroutine(Push())));
            }*/
#endif

            /*
            var fetchVersion = _patchReference.Parent.Child("patchVersion").GetValueAsync();
            while (!fetchVersion.IsCompleted) yield return Timing.WaitForOneFrame;
            patchVersion = int.Parse(fetchVersion.Result.GetRawJsonValue());

            while (!Caching.ready) yield return Timing.WaitForOneFrame;

            _patchReference.ChildAdded += HandleAssetAdded;
            _patchReference.ChildChanged += HandleAssetChanged;
            _patchReference.ChildRemoved += HandleAssetRemoved;*/
            
            while (!_isReady) yield return Timing.WaitForSeconds(1f);
            yield return Timing.WaitUntilDone(_bundleManager.LoadBundle(coreSceneBundle, "corescenes"));
            LoadingManager.LoadScene(proceedingScene, LoadSceneMode.Single);
        }

        /*
        void HandleAssetAdded(object sender, ChildChangedEventArgs args)
        {
            if (args.DatabaseError != null)
            {
                Debug.LogError(args.DatabaseError.Message);
                return;
            }

            var json = args.Snapshot.GetRawJsonValue();
            if (!json.Contains("bundleName")) return;
            var data = JsonUtility.FromJson<AssetBundleData>(json);
            if(data.type == AssetBundleType.Necessary)
            {
                QueuePatch(data);
            }
            else
            {
                dynamicAssets.Add(data.bundleName, data);
            }
            Debug.Log("Added: " + json);
        }

        void HandleAssetChanged(object sender, ChildChangedEventArgs args)
        {
            if (args.DatabaseError != null)
            {
                Debug.LogError(args.DatabaseError.Message);
                return;
            }

            Debug.Log("Changed: " + args.Snapshot.GetRawJsonValue());
            var data = JsonUtility.FromJson<AssetBundleData>(args.Snapshot.GetRawJsonValue());
            if (data.type == AssetBundleType.Necessary)
            {

            }
            else
            {
                dynamicAssets[data.bundleName] = data;
            }
        }

        void HandleAssetRemoved(object sender, ChildChangedEventArgs args)
        {
            if (args.DatabaseError != null)
            {
                Debug.LogError(args.DatabaseError.Message);
                return;
            }

            var json = args.Snapshot.GetRawJsonValue();
            if (!json.Contains("bundleName")) return;
            Debug.Log("Removed: " + json);

            var data = JsonUtility.FromJson<AssetBundleData>(json);
            if (data.type == AssetBundleType.Necessary)
            {

            }
            else
            {
                dynamicAssets.Remove(data.bundleName);
            }
        }*/
        
        public CoroutineHandle QueuePatch(AssetBundleData data)
        {
            _patchQueue.Add(data);
            if (!_patchHandle.IsRunning)
            {
                _patchHandle = Timing.RunCoroutine(Patching());
                LoadingManager.ShowProgress(_patchHandle);
            }
            return _patchHandle;
        }

        IEnumerator<float> Patching()
        {
            _isReady = false;
            while (_patchQueue.Count > 0)
            {
                var data = _patchQueue[0];
                _patchQueue.RemoveAt(0);
                
                //Debug.Log("Asset path: " + _assetReference + "/" + patchVersion.ToString() + "/" + data.bundleName);
                if (!AssetBundleManager.AssetIsValid(data))
                {
                    Debug.Log("Updating bundle: " + data.bundleName + ".");
                    var bundleRequest = UnityWebRequestAssetBundle.GetAssetBundle(data.url, Hash128.Parse(data.hash), data.crc);
                    yield return Timing.WaitUntilDone(LoadingManager.LoadProgress(data.size, bundleRequest.SendWebRequest()));
                    if (bundleRequest.isNetworkError || bundleRequest.isHttpError)
                    {
                        Debug.Log(bundleRequest.error);
                    }
                    else
                    {
                        Caching.ClearOtherCachedVersions(data.bundleName, Hash128.Parse(data.hash));
                        Debug.Log(data.bundleName + " is updated.");
                    }
                    bundleRequest.Dispose();
                }
                else
                {
                    Debug.Log(data.bundleName + " is up to date.");
                }
                if (_bundleManager.BundleData.ContainsKey(data.bundleName)) _bundleManager.BundleData[data.bundleName] = data;
                else _bundleManager.BundleData.Add(data.bundleName, data);
                
                yield return Timing.WaitForSeconds(.1f);
            }
            onPatchCompleted?.Invoke();
            _isReady = true;
        }

            /*
        private IEnumerator<float> GetURL(AssetBundleData data)
        {
            var getURL = _assetReference.Child(patchVersion.ToString()).Child(data.bundleName).GetDownloadUrlAsync();
            while (!getURL.IsCompleted) yield return Timing.WaitForOneFrame;
            if (!getURL.IsFaulted && !getURL.IsCanceled)
            {
                Debug.Log(data.bundleName + " Uri: " + getURL.Result);
                data.url = getURL.Result.ToString();
            }
            else
            {
                Debug.LogError(getURL.Exception);
            }
            getURL.Dispose();
        }

#if UNITY_EDITOR
        private IEnumerator<float> Push()
        {
            var filePath = Path.Combine(Application.dataPath, assetBundlePath, targetPlatform.ToString(),
                patchVersion.ToString());
            var manifestAssetBundle = AssetBundle.LoadFromFile(Path.Combine(filePath, patchVersion.ToString()));
            var assetBundleManifest = manifestAssetBundle.LoadAsset<AssetBundleManifest>("AssetBundleManifest");

            // Hash128 hash = assetBundleManifest.GetAssetBundleHash(.name);
            
            _patchReference.Parent.Child("patchVersion").SetValueAsync(patchDatabase.versions[channel]);
            foreach (string key in patchDatabase.bundles.Keys)
            {
                var data = patchDatabase.bundles[key].BundleData();
                
                data.hash = assetBundleManifest.GetAssetBundleHash(data.bundleName).ToString();
                BuildPipeline.GetCRCForAssetBundle(Path.Combine(filePath, data.bundleName), out data.crc);
                Debug.Log(data.bundleName + " Hash: " + data.hash);
                Debug.Log(data.bundleName + " CRC: " + data.crc);
                
                if (AssetBundleManager.AssetIsValid(data))
                {
                    Debug.Log(data.bundleName + " is valid, no need to patch.");
                    continue;
                }
                
                var upload = _assetReference.Child(patchVersion.ToString()).Child(data.bundleName).PutFileAsync(Path.Combine(filePath, data.bundleName), null,
                        new StorageProgress<UploadState>(state => {
                            // called periodically during the upload
                            Debug.Log(string.Format("Progress: {0} of {1} bytes transferred.",
                                state.BytesTransferred, state.TotalByteCount));
                        }), CancellationToken.None, null);
                
                while (!upload.IsCompleted)
                {
                    yield return Timing.WaitForOneFrame;
                }
                if (upload.IsFaulted || upload.IsCanceled)
                {
                    Debug.Log(upload.Exception.ToString());
                    // Uh-oh, an error occurred!
                }
                else
                {
                    // Metadata contains file metadata such as size, content-type, and download URL.
                    StorageMetadata metadata = upload.Result;
                    data.size = metadata.SizeBytes;
                    yield return Timing.WaitUntilDone(GetURL(data));
                    Debug.Log("Finished uploading " + key);
                }
            
                var push = _patchReference.Child(key)
                    .SetRawJsonValueAsync(JsonUtility.ToJson(data));
                while (!push.IsCompleted) yield return Timing.WaitForOneFrame;
            }
            
            Debug.Log("Patch push completed.");
            Debug.Log("Current patch version: " + patchDatabase.versions[channel]);
        }
#endif*/
    }
}